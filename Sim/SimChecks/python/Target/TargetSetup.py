#######################################################################################
## Option file to define geometry reader and geometry conditions                     ##
##                                                                                   ##
##  @authors : L. Pescatore, K. Zarebski                                             ##
##  @date    : last modified 2018-07-16                                              ##
#######################################################################################
from Gaudi.Configuration import *
from Gauss.Configuration import *

particles = {'Piminus': -211, 'Piplus': 211, 'Kminus': -321, 'Kplus': 321, 'p': 2212, 'pbar': -2212}
Zplane = {'Al': {1: 200, 5: 400, 10: 600}, 'Be': {1: 800, 5: 1000, 10: 1200}, 'Si': {1: 1400, 5: 1600, 10: 1800}}
Zorig = {'Al': {1: 100, 5: 300, 10: 500}, 'Be': {1: 700, 5: 900, 10: 1100}, 'Si': {1: 1300, 5: 1500, 10: 1700}}

import logging
logging.basicConfig()

_logger = logging.getLogger("TARGETSETUP")

def targetGaussGeo():
    from Configurables import GaussGeo
    hadronTestGeo = GaussGeo()
    hadronTestGeo.OutputLevel = WARNING
    #hadronTestGeo.GeoItemsNames = ["/dd/Structure/TargetDet/"+target]  #Adds only the target you are currently looking at
    hadronTestGeo.GeoItemsNames = ["/dd/Structure/TargetDet"]

    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/SimulationRICHesOff.xml"
    SimulationSvc().OutputLevel = WARNING

def targetGiGaGeo():
    from Configurables import GiGaInputStream
    hadronTestGeo = GiGaInputStream('Geo')
    hadronTestGeo.OutputLevel = WARNING
   #hadronTestGeo.StreamItems = ["/dd/Structure/TargetDet/"+target]  #Adds only the target you are currently looking at
    hadronTestGeo.StreamItems = ["/dd/Structure/TargetDet"]          # Adds all targets at once

    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/SimulationRICHesOff.xml"
    SimulationSvc().OutputLevel = WARNING


def addMyTool():
    from Configurables import GiGa, GiGaTrackActionSequence
    hadronTestGiGa = GiGa()
    hadronTestGiGa.OutputLevel = WARNING
    hadronTestGiGa.addTool(GiGaTrackActionSequence("TrackSeq"), name="TrackSeq")
    hadronTestGiGa.TrackSeq.Members.append("GaussTargetMultiplicity")


def setup_Target_GaussJob(physList, targetThick, targetMat, projEng, projID, nEvts=10000):

    from Configurables import LHCbApp
    from Configurables import DDDBConf
    from Configurables import CondDB
    from Configurables import Gauss

    target = 'Target_' + str(targetThick) + 'mm' + targetMat

    hadronTestGauss = Gauss()
   
    DDDBConf().DbRoot = "conddb:/TargetsDet.xml"
    if 'v45' not in os.environ["GAUSSROOT"]:
        CondDB().LoadCALIBDB = "HLT1"
    CondDB().Upgrade = True
    LHCbApp().DDDBtag = "dddb-20140120"
    LHCbApp().CondDBtag = "sim-20131108-vc-md100"
    LHCbApp().Simulation = True
    LHCbApp().OutputLevel = WARNING
    # For testing only
    #DDDBConf(DbRoot = "/afs/cern.ch/user/s/seaso/public/Simulation/
    #upgrade/Gauss_Target/DB/myDDDB-Upgrade-TargetGeom-January2014/TargetsDet.xml")

    hadronTestGauss.DetectorGeo = {"Detectors": []}
    hadronTestGauss.DetectorSim = {"Detectors": []}
    hadronTestGauss.DetectorMoni = {"Detectors": []}

    hadronTestGauss.DataType = "Upgrade"
    hadronTestGauss.PhysicsList = {"Em": 'NoCuts', "Hadron": physList, "GeneralPhys": True, "LHCbPhys": True}

    # --- activate special targets hadronTestGeometry

    if 'UseGaussGeo' in dir(hadronTestGauss):
        # As GaussGeo is not set to any value by default, check if set to 'False'
        # else take default 'member' state to be 'True'
        try:
           assert hadronTestGauss.UseGaussGeo == False
           _logger.info("Using 'GiGaGeo' for Geometry Input.")
           appendPostConfigAction(targetGiGaGeo)
        except AssertionError:
           _logger.info("Using 'GaussGeo' for Geometry Input.")
           appendPostConfigAction(targetGaussGeo) 
        except AttributeError:
           # Check if no value set and warn user which reader is being used
           _logger.warning("'UseGaussGeo' not set using 'GaussGeo' for Geometry Input.")
           appendPostConfigAction(targetGaussGeo) 

    else:
        try:
           from Configurables import GaussGeo
           _logger.warning("'UseGaussGeo' option not found in current Gauss version, using Default reader 'GaussGeo' instead.")
           appendPostConfigAction(targetGaussGeo)
        except ImportError:
           _logger.warning("'UseGaussGeo' option not found in current Gauss version, using Default reader 'GiGaGeo' instead.")
           appendPostConfigAction(targetGiGaGeo)


    # --- Switch off delta rays
    hadronTestGauss.DeltaRays = False

    # --- activate GaussTargetMultiplicity tool
    appendPostConfigAction(addMyTool)

    # --- Configure the tool
    from Configurables import GiGa, GiGaTrackActionSequence, GaussTargetMultiplicity
    hadronTestGiGa = GiGa()
    hadronTestGiGa.OutputLevel = WARNING
    hadronTestGiGa.addTool(GiGaTrackActionSequence("TrackSeq"), name="TrackSeq")
    hadronTestGiGa.TrackSeq.addTool(GaussTargetMultiplicity)
    hadronTestGiGa.TrackSeq.GaussTargetMultiplicity.InteractionVolumeName = ["/dd/Structure/TargetDet/{}#pv{}".format(target,target.replace('Target', 'Targ'))]
    hadronTestGiGa.TrackSeq.GaussTargetMultiplicity.InteractionVolumeString = [target]
    hadronTestGiGa.TrackSeq.GaussTargetMultiplicity.TargetMaterial = [targetMat]
    hadronTestGiGa.TrackSeq.GaussTargetMultiplicity.TargetThickness = [targetThick]
    hadronTestGiGa.TrackSeq.GaussTargetMultiplicity.PhysicsList = [physList]
    hadronTestGiGa.TrackSeq.GaussTargetMultiplicity.ProjectileEnergy = [projEng]
    hadronTestGiGa.TrackSeq.GaussTargetMultiplicity.ProjectilePdgID = [particles[projID]]
    #hadronTestGiGa.TrackSeq.GaussTargetMultiplicity.OutputLevel = DEBUG

    from Configurables import CondDB, LHCbApp, DDDBConf, CondDBAccessSvc
    from Configurables import Gauss

    hadronTestGauss.Production = 'PGUN'

    #--Generator phase, set random numbers
    GaussGen = GenInit("GaussGen")
    GaussGen.OutputLevel = WARNING
    GaussGen.FirstEventNumber = 1
    GaussGen.RunNumber = 1082

    #--Number of events
    LHCbApp().EvtMax = nEvts

    hadronTestGauss.Production = 'PGUN'

    hadronTestGauss.OutputType = 'NONE'
    hadronTestGauss.Histograms = 'NONE'

    #--- Save ntuple with hadronic cross section information
    ApplicationMgr().ExtSvc += ["NTupleSvc"]
    NTupleSvc().Output = ["FILE1 DATAFILE='Multi_{particle}_in{material}.root' TYP='ROOT' OPT='NEW'".format(particle=projID, material=targetMat)]


def setup_Targets_pguns(projID, projEng, targMat, targThick):

    from Configurables import ParticleGun
    from GaudiKernel.SystemOfUnits import mm, GeV, rad

    # Set up ParticleGun
    ParticleGun = ParticleGun("ParticleGun")
    ParticleGun.OutputLevel = WARNING

    # Control of the ParticleGun
    # Event type is set as described in LHCb-2005-034 : G=5! (Def = 0)
    ParticleGun.EventType = 53210205

    # Pick the type of particle gun generator
    from Configurables import MaterialEval
    ParticleGun.addTool(MaterialEval, name="MaterialEval")
    ParticleGun.ParticleGunTool = "MaterialEval"

    # Set fixed number of particles to produce in an event (default = 10) and
    # their PDG code (default = 2221111)
    from Configurables import FlatNParticles
    ParticleGun.addTool(FlatNParticles, name="FlatNParticles")
    ParticleGun.NumberOfParticlesTool = "FlatNParticles"
    ParticleGun.FlatNParticles.MinNParticles = 1
    ParticleGun.FlatNParticles.MaxNParticles = 1
    ParticleGun.MaterialEval.PdgCode = particles[projID]

    # The vertex of each particle gun is produced in a single point of given
    # coordinates. Default is 0.0*mm for all.
    # distribution within the given limits, when min=max the vertex is in the
    # given point.
    #ParticleGun.MaterialEval.Xorig = 0*mm
    #ParticleGun.MaterialEval.Yorig = 0*mm
    ParticleGun.MaterialEval.Zorig = Zorig[targMat][targThick] * mm

    # The abs(P) of the particle if fixed at the given value (default = 500 GeV)
    ParticleGun.MaterialEval.ModP = projEng * GeV

    # The particle can be generated uniformly (randomly or in a grid) in eta-phi
    # rather than x-y (default is false, i.e. x-y)
    ParticleGun.MaterialEval.EtaPhi = True

    # The boundary limits of the x-y or eta-phi planes have to given.
    #
    # The following options are for the x-y plane, to specify the position
    # and size of the "target rectangle". The default plane is at z = 10*m,
    # with -3.2*m < x < 3.2*m and -2.6*m < y < -2.6*m, i.e. a little more
    # than the LHCb acceptance
    ParticleGun.MaterialEval.ZPlane = Zplane[targMat][targThick] * mm
    #ParticleGun.MaterialEval.Xmin   = -15*m
    #ParticleGun.MaterialEval.Ymin   = -15*m
    #ParticleGun.MaterialEval.Xmax   =  15*m
    #ParticleGun.MaterialEval.Ymax   =  15*m

    # The follwing control the parameters for the generation in eta-phi.
    # The defaults are 2.1 < eta < 4.9 and phi over 2pi, i.e. a little more
    # than scanning the LHCb acceptance
    ParticleGun.MaterialEval.MinEta = 10.
    ParticleGun.MaterialEval.MaxEta = 10.
    ParticleGun.MaterialEval.MinPhi = 0.0 * rad
    ParticleGun.MaterialEval.MaxPhi = 0.0 * rad

    # A regular grid of given steps can be used instead of a randomly uniform
    # distribution (default = false)
    ParticleGun.MaterialEval.UseGrid = False
