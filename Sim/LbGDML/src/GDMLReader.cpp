// Gaudi
// Geant4
#include "Geant4/G4VPhysicalVolume.hh"
#include "Geant4/G4LogicalVolume.hh"
#include "Geant4/G4LogicalVolumeStore.hh"
#include "Geant4/G4PVPlacement.hh"
#include "Geant4/G4AssemblyVolume.hh"
#include "Geant4/G4GDMLParser.hh"
#include "Geant4/G4VisAttributes.hh"
#include "GiGa/IGiGaSensDet.h"
#include "Geant4/G4SDManager.hh"
// Local
#include "GDMLReader.h"

/** @file GDMLReader.cpp
 *
 *  Implementation of class : GDMLReader
 *
 */

DECLARE_COMPONENT( GDMLReader )

//=============================================================================
/// Constructor
//=============================================================================
GDMLReader::GDMLReader(const std::string& type,
                       const std::string& name,
                       const IInterface* parent) : 
    GaudiTool(type, name, parent) {

  declareInterface<IGDMLReader>(this);

  declareProperty("FileName", m_gdmlfile);
  declareProperty("TranslationX", m_tx = 0.);
  declareProperty("TranslationY", m_ty = 0.);
  declareProperty("TranslationZ", m_tz = 0.);
  declareProperty("RotationX", m_rx = 0.);
  declareProperty("RotationY", m_ry = 0.);
  declareProperty("RotationZ", m_rz = 0.);
  declareProperty("SensitiveDetectors", m_sensDetName);

}

//=============================================================================
/// Import geometry from GDML file and attach to world volume
//=============================================================================
StatusCode GDMLReader::import(G4VPhysicalVolume* world) {

  info() << "Loading GDML geometry description from file "
         << m_gdmlfile << "." << endmsg;
  /// Read the GDML file.
  G4GDMLParser g4parser;
  g4parser.Read(m_gdmlfile, false);
  /// Get the world volume.
  G4VPhysicalVolume* gdmlWorldPV = g4parser.GetWorldVolume();
  if (!gdmlWorldPV) {
    error() << "Could not retrieve world volume from file " 
            << m_gdmlfile << "." << endmsg;
    return StatusCode::FAILURE; 
  }
  G4LogicalVolume* gdmlWorldLV = gdmlWorldPV->GetLogicalVolume();

  /// Make an assembly volume to hold the daughter volumes of the GDML world.
  G4AssemblyVolume* gdmlAssembly = new G4AssemblyVolume();
  // Loop over the daughter volumes.
  const int nDaughters = gdmlWorldLV->GetNoDaughters();
  info() << "GDML world volume " << gdmlWorldLV->GetName()
         << " has "<< nDaughters << " daughter(s)" << endmsg;
  for (int i = nDaughters; i--;) {
    G4VPhysicalVolume* gdmlDaughterPV = gdmlWorldLV->GetDaughter(i);
    std::string nameDaughter = gdmlDaughterPV->GetName();
    info() << "Picking up volume " << nameDaughter
           << " from " << m_gdmlfile << endmsg;
    G4ThreeVector transDaughter = gdmlDaughterPV->GetTranslation();
    G4RotationMatrix* rotDaughter = gdmlDaughterPV->GetRotation();
    G4LogicalVolume* gdmlDaughterLV = gdmlDaughterPV->GetLogicalVolume();
    /// Give the GDML volume a different colour (to better distinguish it
    /// in the G4 visualization)
    G4VisAttributes* gdmlDaughterVisAtt = new G4VisAttributes(G4Colour(0, 1, 1));
    gdmlDaughterLV->SetVisAttributes(gdmlDaughterVisAtt);

    /// Add the volume to the assembly
    gdmlAssembly->AddPlacedVolume(gdmlDaughterLV,
                                  transDaughter, rotDaughter);
  }
  // Now instrument the given volumes with the sensitive detectors as needed
  // Use a map to store the pointers to sensitive detector instances
  // in case multiple volume-sensDet pairs have the same sensitive detector
  std::map<std::string, IGiGaSensDet*>  m_sensDets;
  G4SDManager* sd_manager = G4SDManager::GetSDMpointer();
  for (auto& volSensDetPair : m_sensDetName) {
    if (m_sensDets.find(volSensDetPair.second) == std::end(m_sensDets)) {
      m_sensDets[volSensDetPair.second] =
          tool<IGiGaSensDet>(volSensDetPair.second, this);
      sd_manager->AddNewDetector(m_sensDets[volSensDetPair.second]);
    }
    auto sensDet = m_sensDets[volSensDetPair.second];
    if (!sensDet) {
      return Error("Failed to locate Sensitive Detector '" +
                   volSensDetPair.second + "'");
    }
    auto& volName = volSensDetPair.first;
    // Get the volume name via the volume store.
    // TODO: if necessary, could allow for wildcard volume names and use regex
    // auto vol = G4LogicalVolumeStore::GetInstance()->GetVolume(volName);
    unsigned int augmented_volumes = 0;
    for (auto& vol : *G4LogicalVolumeStore::GetInstance()) {
      if (vol->GetName() != G4String(volName)) {
        continue;
      }
      augmented_volumes++;
      vol->SetSensitiveDetector(sensDet);
      info() << "Attached sensitive detector " << sensDet->GetName()
             << " to logical volume " << vol->GetName() << " #"
             << augmented_volumes << endmsg;
    }
    if (augmented_volumes == 0) {
      return Error("Failed to any locate logical volumes '" + volName + "'");
    }
  }

  /// Set the position of the GDML assembly in the LHCb world.
  G4ThreeVector transAssembly(m_tx, m_ty, m_tz);
  G4RotationMatrix* rotAssembly = new G4RotationMatrix();
  rotAssembly->rotateX(m_rx * CLHEP::deg);
  rotAssembly->rotateY(m_ry * CLHEP::deg);
  rotAssembly->rotateZ(m_rz * CLHEP::deg);
  /// Place the assembly volume in the LHCb world.
  gdmlAssembly->MakeImprint(world->GetLogicalVolume(),
                            transAssembly, rotAssembly);
  return StatusCode::SUCCESS;

}
