"""
High level configuration tools for Gauss
Beampipe configuration added.
"""
__version__ = "$Id: Configuration.py,v 1.30 2010/05/09 18:14:28 gcorti Exp $"
__author__  = "Gloria Corti <Gloria.Corti@cern.ch>"

from Gaudi.Configuration import *
import GaudiKernel.ProcessJobOptions
from GaudiKernel import SystemOfUnits
from Configurables import LHCbConfigurableUser, LHCbApp, SimConf


# CRJ - Its un-neccessary to import everythting by default. Better to
#       import as and when you need it ...
from Configurables import ( CondDBCnvSvc, EventClockSvc, FakeEventTime,
                            CondDBEntityResolver )
from Configurables import ( GenInit, Generation, MinimumBias, Inclusive,
#from Configurables import ( GenInit, Generation,
# Double imports
                            SignalPlain, SignalRepeatedHadronization,
                            SignalForcedFragmentation, StandAloneDecayTool,
                            Special,
                            PythiaProduction, HijingProduction,
                            CRMCProduction,
# Not used
                            CollidingBeams, FixedTarget,
                            BeamSpotSmearVertex, FlatZSmearVertex,
                            EvtGenDecay )
from Configurables import ( SimInit, GaussGeo, GiGaGeo, GiGaInputStream, GiGa,
                            GiGaDataStoreAlgorithm,
                            GiGaPhysListModular, GiGaRunActionSequence,
                            TrCutsRunAction, GiGaRunActionCommand,
                            GiGaEventActionSequence, GiGaMagFieldGlobal,
                            GiGaTrackActionSequence, GaussPostTrackAction,
                            GiGaStepActionSequence, SimulationSvc,
                            GiGaFieldMgr, GiGaRunManager, GiGaSetSimAttributes,
                            GiGaPhysConstructorOp, GiGaPhysConstructorHpd,
                            SpdPrsSensDet, EcalSensDet, HcalSensDet,
                            GaussSensPlaneDet )
from Configurables import ( GenerationToSimulation, GiGaFlushAlgorithm,
                            GiGaCheckEventStatus, SimulationToMCTruth,
                            GiGaGetEventAlg, GiGaGetHitsAlg,
                            GetTrackerHitsAlg, GetCaloHitsAlg,
                            GetMCRichHitsAlg, GetMCRichOpticalPhotonsAlg,
                            GetMCRichSegmentsAlg, #GetMCRichTracksAlg,
                            Rich__MC__MCPartToMCRichTrackAlg,
                            Rich__MC__MCRichHitToMCRichOpPhotAlg)
from Configurables import ( GenMonitorAlg, MuonHitChecker, MCTruthMonitor,
                            VeloGaussMoni, MCHitMonitor, MCCaloMonitor,
                            DumpHepMC )
from Configurables import ( PackMCParticle, PackMCVertex,
                            UnpackMCParticle, UnpackMCVertex,
                            CompareMCParticle, CompareMCVertex )

# Various options for the RICH
from Configurables import (GaussRICHConf, GaussCherenkovConf)

# All GaussRedecay includes
from Configurables import ( GaussRedecay, GaussRedecayCopyToService,
                            GaussRedecayRetrieveFromService,
                            GaussRedecayPrintMCParticles,
                            GaussRedecayCtrFilter,
                            GaussRedecaySorter,
                            GaussHepMCSplitter,
                            GaussRedecayMergeAndClean)

from DetCond.Configuration import CondDB

## @class Gauss
#  Configurable for Gauss application
#  @author Gloria Corti <Gloria.Corti@cern.ch>
#  @date   2009-07-13

class Gauss(LHCbConfigurableUser):

    __knownDetectors__ = [
        'velo', 'puveto', 'vp',
        'tt' , 'ut',
        'it' , 'sl',
        'ot' , 'ft', 'ft-noshield',
        'rich',  'rich1', 'rich2', 'torch' ,
        'calo',  'spd', 'prs', 'ecal', 'hcal' ,
        'muon' ,
        'magnet',
        'rich1pmt', 'rich2pmt',
        'hc'
#        'bcm', 'bls'
        ]

    ## Possible used Configurables
    __used_configurables__ = [ LHCbApp, SimConf,
                               (GaussRICHConf,None),
                               (GaussCherenkovConf,None)
                               ]

    ## Map to contain PDG ids for beam particles
    __ion_pdg_id__ = { 'Pb': 1000822080 , 'Ar': 1000180400 , 'p': 2212 , 'Ne': 1000100200 , 'He': 1000020040 , 'Kr': 1000360840 ,
                       'Xe': 1000541320 }

    ## Steering options
    __slots__ = {
        "Histograms"        : "DEFAULT"
        ,"DatasetName"       : "Gauss"
        ,"DataType"          : ""
        # Simple lists of sub detectors
        ,"DetectorGeo"       : {"Detectors": ['PuVeto', 'Velo', 'TT', 'IT', 'OT', 'Rich1', 'Rich2', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet'] }
        ,"DetectorSim"       : {"Detectors": ['PuVeto', 'Velo', 'TT', 'IT', 'OT', 'Rich1', 'Rich2', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet'] }
        ,"DetectorMoni"      : {"Detectors": ['PuVeto', 'Velo', 'TT', 'IT', 'OT', 'Rich1', 'Rich2', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon'] }
        ,"SpilloverPaths"    : []
        ,"PhysicsList"       : {"Em":'NoCuts', "Hadron":'FTFP_BERT', "GeneralPhys":True, "LHCbPhys":True, "Other": '' }
        ,"DeltaRays"         : True
        ,"RICHRandomHits"    : False
        ,"Phases"            : ["Generator","Simulation"] # The Gauss phases to include in the SIM file
        ,"BeamMomentum"      : 3.5*SystemOfUnits.TeV
        ,"BeamHCrossingAngle" : -0.520*SystemOfUnits.mrad
        ,"BeamVCrossingAngle" : 0.0
        ,"BeamEmittance"     : 0.0037*SystemOfUnits.mm
        ,"BeamBetaStar"      : 3.1*SystemOfUnits.m
        ,"BeamLineAngles"    : [ -0.075*SystemOfUnits.mrad,
                                  0.035*SystemOfUnits.mrad ]
        ,"InteractionPosition" : [  0.459*SystemOfUnits.mm ,
                                   -0.015*SystemOfUnits.mm ,
                                    0.5*SystemOfUnits.mm ]
        ,"BunchRMS"          : 82.03*SystemOfUnits.mm
        ,"Luminosity"        : 0.247*(10**30)/(SystemOfUnits.cm2*SystemOfUnits.s)
        ,"TotalCrossSection" : 91.1*SystemOfUnits.millibarn
        ,"OutputType"        : 'SIM'
        ,"Production"        : 'PHYS'
        ,"EnablePack"        : True
        ,"DataPackingChecks" : True
        ,"WriteFSR"          : True
        ,"MergeGenFSR"       : False
        ,"Debug"             : False
        ,"BeamPipe" : "BeamPipeOn" # _beamPipeSwitch = 1
        ,"ReplaceWithGDML"   : [ { "volsToReplace" : [], "gdmlFile" : "", 'volsToInstrument' : {} } ]
        #,"BeamPipe" : "BeamPipeOff"  # _beamPipeSwitch = 0
        #,"BeamPipe" : "BeamPipeInDet"  # _beamPipeSwitch = -1
        ,"RandomGenerator"   : 'Ranlux'
        ## Add properties for fixed target and heavy ion simulation
        ## energy of the beam 2 (beam 1 is taken from the BeamMomentum property
        ## if this energy is 0, or if BeamMomentum is 0, it is fixed target simulation
        , "B2Momentum" : 3.5*SystemOfUnits.TeV
        ## type of particle in the beam or in the fixed target
        , "B1Particle" : 'p'
        , "B2Particle" : 'p'
        , "UseGaussGeo" : True
        , "Redecay" : {"N": 100, 'active': False, 'rd_mode': 1}
        , "CurrentRICHSimRunOption" : 'GTB'
        , "UpgradeRICHSimRunOption"  : 'GTB'
        , "SplitSim" : False
        , "PostSimFilters"  : []
      }

    _detectorsDefaults = {"Detectors": ['PuVeto', 'Velo', 'TT', 'IT', 'OT', 'Rich1', 'Rich2', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet'] }
    _propertyDocDct = {
        'Histograms'     : """ Type of histograms: ['NONE','DEFAULT'] """
       ,'DatasetName'    : """ String used to build output file names """
       ,"DataType"       : """ Must specify 'Upgrade' for upgrade simulations, otherwise not used """
       ,"DetectorGeo"    : """ Dictionary specifying the detectors to take into account in Geometry """
       ,"DetectorSim"    : """ Dictionary specifying the detectors to simulated (should be in geometry): """
       ,"DetectorMoni"   : """ Dictionary specifying the detectors to monitor (should be simulated) :"""
       ,'SpilloverPaths' : """ Spillover paths to fill: [] means no spillover, otherwise put ['Next', 'Prev', 'PrevPrev'] """
       ,'PhysicsList'    : """ Name of physics modules to be passed 'Em':['Std','Opt1,'Opt2','Opt3','NoCuts','LHCb', 'LHCbNoCuts', 'LHCbOldForE', 'LHCbNoCutsOldForE', 'LHCbTest', 'LHCbTestNoCut' ], 'GeneralPhys':[True,False], 'Hadron':['QGSP_BERT','QGSP_BERT_HP','QGSP_FTFP_BERT','FTFP_BERT','FTFP_BERT_HP'], 'LHCbPhys': [True,False], 'Other': [''] """
       ,"DeltaRays"      : """ Simulation of delta rays enabled (default True) """
       ,'Phases'         : """ List of phases to run (Generator, Simulation, GenToMCTree) """
       ,'OutputType'     : """ Output: [ 'NONE', 'GEN', 'XGEN', 'RGEN', 'SIM', 'XSIM' ] (default 'SIM') """
       ,'Production'     : """ Generation type : ['PHYS', 'PGUN', 'MIB' (default 'PHYS')"""
       ,'EnablePack'     : """ Flag to turn on or off the packing of the SIM data """
       ,'DataPackingChecks' : """ Flag to turn on or off the running of some test algorithms to check the quality of the data packing """
       ,"WriteFSR"       : """Add file summary record, default True"""
       ,"MergeGenFSR"    : """Flags whether to merge the generator level FSRs"""
       ,"BeamPipe"       : """Switch for beampipe definition; BeamPipeOn: On everywhere, BeamPipeOff: Off everywhere, BeamPipeInDet: Only in named detectors """
       ,"ReplaceWithGDML": """Replace a list of specified volumes with GDML description from file provided """
       ,"RandomGenerator": """Name of randon number generator engine: Ranlux or MTwist"""
       ,"UseGaussGeo"    : """Use GaussGeo (True: default) or GiGaGeo (False) for geometry conversion"""
       ,"Redecay"        : """ Dict with redecay settings, default: {'N': 100, 'active': False, 'rd_mode': 1}."""
       ,"CurrentRICHSimRunOption" : """ GaussRICH run options: ['Formula1', 'GTB', 'SUV','HGV', 'clunker', 'FareFiasco'] (default 'GTB') """
       ,"UpgradeRICHSimRunOption" : """ GaussCherenkov run options: ['Formula1', 'GTB', 'SUV','HGV', 'clunker' , 'FareFiasco'] (default 'GTB') """
       , "SplitSim"      : """ Split the simulation phase for gamma-conversion. (default: False)"""
       ,"PostSimFilters" : """ List of filters that are executed after Geant has run (default: none)"""
       }
    KnownHistOptions     = ['NONE','DEFAULT']
    TrackingSystem       = ['VELO','TT','IT','OT']
    PIDSystem            = ['RICH','CALO','MUON']

    Run1DataTypes = [ "2009", "2010", "2011", "2012", "2013" ]
    Run2DataTypes = [ "2015", "2016", "2017", "2018" ]

    _beamPipeStates = ['beampipeon', 'beampipeoff', 'beampipeindet']

    KnownPostSimFilters    = [ "ConversionFilter" ]

    _incompatibleDetectors = {
        "Velo"       : [ "Velo", "VP" ],
        "VeloPuVeto" : [ "PuVeto", "VP" ],
        "TT"         : [ "TT", "UT" ],
        "Muon"       : [ "Muon", "MuonNoM1" ],
        "MuonTorch"  : [ "Muon", "Torch" ]
        }

    _beamPipeElements = {
        #"upstreamregion" : [
        #"/dd/Structure/LHCb/UpstreamRegion/PipeUpstream" ,
        #"/dd/Structure/LHCb/UpstreamRegion/MBXWHUp" ],
        #"beforemagnetregion" : [
        #    "/dd/Structure/LHCb/BeforeMagnetRegion/PipeJunctionBeforeVelo",
        #    "/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/PipeBeforeVelo",
        #    "/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/PipeSupportBeforeVelo" ],
                         "velo" : [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownStreamWakeFieldCone",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpStreamWakeFieldCone",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownstreamPipeSections",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VacTank" ],
                         "rich1" : [
            "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1BeforeSubM",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1SubMaster",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1AfterSubM",
            "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1BeamPipe" ],
                         "tt" : [
            "/dd/Structure/LHCb/BeforeMagnetRegion/TT/PipeInTT" ],
                         "ut" : [
            "/dd/Structure/LHCb/BeforeMagnetRegion/UT/PipeInUT" ],
                         "magnet" : [
            "/dd/Structure/LHCb/MagnetRegion/PipeInMagnet",
            "/dd/Structure/LHCb/MagnetRegion/PipeSupportsInMagnet" ],
                         "AfterMagnetRegion" : [
            "/dd/Structure/LHCb/AfterMagnetRegion/PipeAfterT",
            "/dd/Structure/LHCb/AfterMagnetRegion/PipeSupportsAfterMagnet" ],
                         "t" : [
            "/dd/Structure/LHCb/AfterMagnetRegion/T/PipeInT" ],
                         "rich2" : [
            "/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2BeamPipe" ],
                         "downstreamregion" : [
            "/dd/Structure/LHCb/DownstreamRegion/PipeDownstream",
            "/dd/Structure/LHCb/DownstreamRegion/PipeSupportsDownstream",
            "/dd/Structure/LHCb/DownstreamRegion/PipeBakeoutDownstream" ]
                         #"aftermuon" : [
                         #"/dd/Structure/LHCb/DownstreamRegion/AfterMuon/PipeAfterMuon",
            #"/dd/Structure/LHCb/DownstreamRegion/AfterMuon/MBXWSDown" ]
        }

    # List of geometry objects which will be converted, it's content is used in
    # GaussGeo or in GiGaInputStream if GiGaGeo is used for conversion
    _listOfGeoObjects_ = []

#"""
#Helper
#HELPER
#
#  ><<     ><<             ><<
#  ><<     ><<             ><<
#  ><<     ><<    ><<      ><< >< ><<      ><<     >< ><<<
#  ><<<<<< ><<  ><   ><<   ><< ><  ><<   ><   ><<   ><<
#  ><<     ><< ><<<<< ><<  ><< ><   ><< ><<<<< ><<  ><<
#  ><<     ><< ><          ><< ><< ><<  ><          ><<
#  ><<     ><<   ><<<<     ><< ><<        ><<<<     ><<<
#                              ><<
#
#"""
    ##
    ##
    def slotName(self,slot) :
        name = slot
        if slot == '' : name = "Main"
        return name

    ##
    ## Helper functions for spill-over
    def slot_( self, slot ):
        if slot != '':
            return slot + '/'
        return slot

    ##
    def setTrackersHitsProperties( self, alg , det , slot , dd ):
        alg.MCHitsLocation = '/Event/' + self.slot_(slot) + 'MC/' + det + '/Hits'
        if det == 'PuVeto':
            det = 'VeloPu'
        alg.CollectionName = det + 'SDet/Hits'
        alg.Detectors = ['/dd/Structure/LHCb/'+dd]

    ##
    def evtMax(self):
        return LHCbApp().evtMax()

    ##
    def eventType(self):
        from Configurables import Generation
        evtType = ''
        if Generation("Generation").isPropertySet("EventType"):
            evtType = str( Generation("Generation").EventType )
        return evtType

    def setLHCbAppDetectors(self):
        from Configurables import LHCbApp
        # If detectors set in LHCbApp then use those
        if hasattr(LHCbApp(),"Detectors"):
            if not LHCbApp().Detectors:
                LHCbApp().Detectors = self.getProp("DetectorGeo")["Detectors"]
            else:
                log.warning("Value of 'LHCbApp().Detectors' already set, using that value: %s" %(LHCbApp().Detectors))
        return

#"""
##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################
#"""



# ><< ><<
# ><    ><<                                                ><
# ><     ><<    ><<        ><<     ><<< ><< ><<  >< ><<       >< ><<      ><<
# ><<< ><     ><   ><<   ><<  ><<   ><<  ><  ><< ><  ><<  ><< ><  ><<   ><   ><<
# ><     ><< ><<<<< ><< ><<   ><<   ><<  ><  ><< ><   ><< ><< ><   ><< ><<<<< ><<
# ><      >< ><         ><<   ><<   ><<  ><  ><< ><< ><<  ><< ><< ><<  ><
# ><<<< ><<    ><<<<      ><< ><<< ><<<  ><  ><< ><<      ><< ><<        ><<<<
#                                                ><<          ><<

    def validateBeamPipeSwitch ( self, bpString ):
        import string
        bpLower = self.getProp("BeamPipe").lower()
        if bpLower not in self._beamPipeStates:
            raise RuntimeError("ERROR: BeamPipe configuration '%s' not recognised!" %bpString)

    def removeBeamPipeElements( self, det ):
        det = det.lower()
        # Remove beampipe elements in <det> - will be included automatically
        if det in self._beamPipeElements.keys():
            for element in self._beamPipeElements[det]:
                # remove all instances of the element
                while element in self._listOfGeoObjects_:
                    self._listOfGeoObjects_.remove(element)

    def removeAllBeamPipeElements( self ):
        # Remove all beampipe elements
        for det in self._beamPipeElements.keys():
            for element in self._beamPipeElements[det]:
                # remove all instances of the element
                while element in self._listOfGeoObjects_:
                    self._listOfGeoObjects_.remove(element)

    def defineBeamPipeGeo ( self, basePieces, detPieces ):
        # Add all BeamPipe Elements in the BeamPipeElements dictionary

        # Here commences a hack to deal with daft DDDB structure
        ignoreList = ['ut', 'tt']
        # decide if TT or UT in dets to simulate
        ttDetectorList = [det for det in ['UT', 'TT'] if det in self.getProp('DetectorGeo')['Detectors']]
        # lower strings
        if ttDetectorList:
            # lower everything in ttDetectorList
            ttDetectorList = [det.lower() for det in ttDetectorList]
            # get the elements to ignore
            ignoreList = [det for det in ignoreList if det not in ttDetectorList]

        for region in self._beamPipeElements.keys():
            if region in ignoreList:
                continue
            for element in self._beamPipeElements[region]:
                self._listOfGeoObjects_.append(element)

        # Finally add in the TT or UT beampipe if we're not defining the detectors but want the BP anyway depending on DataType
        # Nasty and unclean - change the DDDB s.t. it makes sense please!
        if (
            ("UT" not in self.getProp("DetectorGeo")["Detectors"])
            and
            ("TT" not in self.getProp("DetectorGeo")["Detectors"])
            ):
            if self.getProp("DataType") not in ["Upgrade"]:
                for element in self._beamPipeElements["tt"]:
                    self._listOfGeoObjects_.append(element)
            else:
                for element in self._beamPipeElements["ut"]:
                    self._listOfGeoObjects_.append(element)


    def defineGDMLGeo ( self, geoCnvSvc, gdmlDict ):

        # Define the GDML reader tool and add it to the sequence
        from Configurables import GDMLReader
        import os
        gdmlFile = gdmlDict["gdmlFile"]
        instrumentation = gdmlDict['volsToInstrument'] if 'volsToInstrument' in gdmlDict else {}
        parsed_instr = {}

        if gdmlFile:
            gdmlToolName = os.path.splitext(os.path.basename(gdmlFile))[0]
            gdmlTool = GDMLReader( gdmlToolName,
                                   FileName = gdmlFile )
            reader = geoCnvSvc.addTool(gdmlTool, gdmlToolName)
            import Configurables
            for vol, instr in instrumentation.items():
                tool, name, kwargs = instr
                reader.addTool( getattr(Configurables, tool)(name, **kwargs), name=name )
                parsed_instr[vol] = tool + "/" + name
            reader.SensitiveDetectors = parsed_instr
            geoCnvSvc.GdmlReaders.append(gdmlToolName)

            # Remove the corresponding geometry from the Geo.InputStreams
            for item in [g for g in gdmlDict["volsToReplace"] if g != '']:
                if item in self._listOfGeoObjects_:
                    self._listOfGeoObjects_.remove(item)
                else:
                    raise RuntimeError("ERROR: Volume not in list of existing volumes, '%s'" %item)
        else:
           raise RuntimeError("ERROR: Invalid GDML file provided, '%s'" %gdmlFile)



#"""
#><<         ><<             ><<               ><<<<<     ><<<<<     ><<<<<     ><< ><<
# ><<       ><<              ><<               ><<   ><<  ><<   ><<  ><<   ><<  ><    ><<
#  ><<     ><<      ><<      ><<    ><<        ><<    ><< ><<    ><< ><<    ><< ><     ><<
#   ><<   ><<     ><   ><<   ><<  ><<  ><<     ><<    ><< ><<    ><< ><<    ><< ><<< ><
#    ><< ><<     ><<<<< ><<  ><< ><<    ><<    ><<    ><< ><<    ><< ><<    ><< ><     ><<
#     ><<<<      ><          ><<  ><<  ><<     ><<   ><<  ><<   ><<  ><<   ><<  ><      ><
#      ><<         ><<<<    ><<<    ><<        ><<<<<     ><<<<<     ><<<<<     ><<<< ><<
#"""

    def checkVeloDDDB( self ):
        """
        Check if the Velo geometry is compatible with the chosen tags
        """
        # set validity limits for  Velo geometry
        # first postMC09 Velo geometry
        GTagLimit1 = "head-20091120"
        GTagLimit1 = GTagLimit1.split('-')[1].strip()
        VeloLTagLimit1 = "velo-20091116"
        VeloLTagLimit1 = VeloLTagLimit1.split('-')[1].strip()
        # Thomas L. Velo geometry
        GTagLimit2 = "head-20100119"
        GTagLimit2 = GTagLimit2.split('-')[1].strip()
        VeloLTagLimit2 = "velo-20100114"
        VeloLTagLimit2 = VeloLTagLimit2.split('-')[1].strip()

        # DDDB global tag used
        DDDBDate = LHCbApp().DDDBtag
        DDDBDate = DDDBDate.split('-')

        # Check if DDDB tag has a regular format (instead of e.g. a user git branch)
        if len(DDDBDate) > 1 :
          DDDBDate = DDDBDate[1].strip()
          if DDDBDate.isdigit() :

            # check if/which local tag is used for Velo
            cdb = CondDB()
            cdbVeloDate = 0
            for p in cdb.LocalTags:
                if p == "DDDB":
                    taglist = list(cdb.LocalTags[p])
                    for ltag in taglist:
                        if ltag.find("velo")!=-1 :
                            cdbVeloDate = ltag.split('-')[1].strip()

            # Put this here rather than as an argument
            VeloPostMC09 = 0
            # check if the selected tags require one of the postMC09 Velo geometries
            if (DDDBDate >= GTagLimit1) or (cdbVeloDate >= VeloLTagLimit1):
                VeloPostMC09 = 1
            if (DDDBDate >= GTagLimit2) or (cdbVeloDate >= VeloLTagLimit2):
                VeloPostMC09 = 2

            return VeloPostMC09

        log.warning("DDDB tag not parsable as date. Using post-MC09 velo geometry : %s" %(DDDBDate))
        return 2



    ##
    ##


#"""
# ><<         ><< ><<<<<<<< ><<           ><<<<
#  ><<       ><<  ><<       ><<         ><<    ><<
#   ><<     ><<   ><<       ><<       ><<        ><<
#    ><<   ><<    ><<<<<<   ><<       ><<        ><<
#     ><< ><<     ><<       ><<       ><<        ><<
#      ><<<<      ><<       ><<         ><<     ><<
#       ><<       ><<<<<<<< ><<<<<<<<     ><<<<
#"""


    ##
    ##
    def veloMisAlignGeometry( self, VeloPostMC09 ):

        print "veloMisAlignGeometry: %s" %(VeloPostMC09)

        """
        File containing the list of detector element to explicitely set
        to have misalignement in the VELO.
        """
        # remove Automatically included detector elements
        self.removeBeamPipeElements( "velo" )


        if "/dd/Structure/LHCb/BeforeMagnetRegion/Velo" in self._listOfGeoObjects_:
            self._listOfGeoObjects_.remove("/dd/Structure/LHCb/BeforeMagnetRegion/Velo")

        self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU00")
        self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU02")
        self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU01")
        self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU03")

        txt = "/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModuleXX"
        import math
        for i in range(42):
            nr = str(i)
            if len(nr) == 1 : nr = '0'+str(i)
            temp1 = txt.replace('XX',nr)
            if math.modf(float(nr)/2.)[0] > 0.1 :  temp1 = temp1.replace('Left','Right')
            self._listOfGeoObjects_.append(temp1)

        self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownStreamWakeFieldCone")
        self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpStreamWakeFieldCone")
        if (VeloPostMC09==1):
            # description postMC09 of Velo (head-20091120), problem with Velo Tank simulation
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VacTank")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownstreamPipeSections")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpstreamPipeSections")
        elif (VeloPostMC09==2):
            # Thomas L. newer description postMC09 of Velo
            # --- Velo Right
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/RFBoxRight")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/DetSupportRight")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ConstSysRight")
            # --- Velo Left
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/RFBoxLeft")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/DetSupportLeft")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ConstSysLeft")
            # --- Velo
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownstreamPipeSections")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpstreamPipeSections")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VacTank")
        else:
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/UpStreamVacTank")
            self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/DownStreamVacTank")

        self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/RFFoilRight")
        self._listOfGeoObjects_.append("/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/RFFoilLeft")
    ##
    ##


    def defineVeloGeo( self , basePieces , detPieces ):
        # Alter in place BasePieces
        # check if the new velo geometry is required with the chosen DDDB tags

        VeloP = self.checkVeloDDDB()
        if (VeloP==1 or VeloP==2):
            basePieces['BeforeMagnetRegion']=[]

        # Also sort out mis-alignment
        VeloP = self.checkVeloDDDB()
        # No need to check, this is the case if this is called.
        # No need to misalign if only PuVeto exits - check me PSZ.
        if "Velo" in self.getProp('DetectorGeo')['Detectors']:
            self.veloMisAlignGeometry(VeloP) # To misalign VELO





    def configureVeloSim( self, slot, detHits ):
        region = "BeforeMagnetRegion"
        det = "Velo"
        moni = GetTrackerHitsAlg(
            'Get' + det + 'Hits' + slot,
            MCHitsLocation = 'MC/' + det  + '/Hits',
            CollectionName = det + 'SDet/Hits',
            Detectors = [ '/dd/Structure/LHCb/' + region + '/' + det ]
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')




    def configureVeloMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.VeloHits = 'MC/Velo/Hits'

        ## Set the VeloMonitor
        detMoniSeq.Members += [ VeloGaussMoni( "VeloGaussMoni" + slot ) ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :
            from Configurables import DataPacking__Unpack_LHCb__MCVeloHitPacker_
            upVelo = DataPacking__Unpack_LHCb__MCVeloHitPacker_("UnpackVeloHits"+slot,
                                                                OutputName = "MC/Velo/HitsTest" )
            packCheckSeq.Members += [upVelo]

            from Configurables import DataPacking__Check_LHCb__MCVeloHitPacker_
            cVelo = DataPacking__Check_LHCb__MCVeloHitPacker_("CheckVeloHits"+slot)
            packCheckSeq.Members += [cVelo]




#"""
#><<         ><< ><<<<<<<< ><<           ><<<<      ><<<<<<<
# ><<       ><<  ><<       ><<         ><<    ><<   ><<    ><<  ><
#  ><<     ><<   ><<       ><<       ><<        ><< ><<    ><<     ><<   ><<
#   ><<   ><<    ><<<<<<   ><<       ><<        ><< ><<<<<<<   ><<   >< ><<
#    ><< ><<     ><<       ><<       ><<        ><< ><<        ><<    ><
#     ><<<<      ><<       ><<         ><<     ><<  ><<        ><<  ><  ><<
#      ><<       ><<<<<<<< ><<<<<<<<     ><<<<      ><<        ><< ><<   ><<
#"""


    def defineVPGeo( self, detPieces ):
        self.removeBeamPipeElements( "velo" )
        if detPieces.has_key('BeforeMagnetRegion'):
            detPieces['BeforeMagnetRegion'] += ['VP']




    def configureVPSim( self, slot, detHits ):
        region = "BeforeMagnetRegion"
        det = "VP"
        moni = GetTrackerHitsAlg(
            'Get' + det + 'Hits' + slot,
            MCHitsLocation = 'MC/' + det  + '/Hits',
            CollectionName = det + 'SDet/Hits',
            Detectors = [ '/dd/Structure/LHCb/' + region + '/' + det ]
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')






    def configureVPMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        ## in case of a non default detector, need to be overwritten
        #detMoniSeq = GaudiSequencer( "DetectorsMonitor" + slot )
        from Configurables import VPGaussMoni
        detMoniSeq.Members += [ VPGaussMoni( "VPGaussMoni" + slot ) ]

        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :
            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCVPHitPacker_
            checkHits.VeloHits =  'MC/VP/Hits'
            # This is not done in the PuVeto Moni config
            #checkHits.PuVetoHits = ''
            upVP = DataPacking__Unpack_LHCb__MCVPHitPacker_("UnpackVPHits"+slot,
                                                                      OutputName = "MC/VP/HitsTest" )
            packCheckSeq.Members += [upVP]

            from Configurables import DataPacking__Check_LHCb__MCVPHitPacker_
            cVP = DataPacking__Check_LHCb__MCVPHitPacker_("CheckVPHits"+slot)
            packCheckSeq.Members += [cVP]



#"""
#  ><<<<<<<     ><<     ><<    ><<     ><<
#  ><<    ><<   ><<  ><<   ><< ><<     ><<
#  ><<    ><<   ><< ><<        ><<     ><<
#  >< ><<       ><< ><<        ><<<<<< ><<
#  ><<  ><<     ><< ><<        ><<     ><<
#  ><<    ><<   ><<  ><<   ><< ><<     ><<
#  ><<      ><< ><<    ><<<<   ><<     ><<
#
#"""


    def defineRich1GeoDet( self , detPieces ):
        self.removeBeamPipeElements( "rich1" )
        detPieces['BeforeMagnetRegion']+=['Rich1']

    def defineRich1GeoStream( self ):
        self._listOfGeoObjects_.append("/dd/Geometry/BeforeMagnetRegion/Rich1/Rich1Surfaces")
        self._listOfGeoObjects_.append("/dd/Geometry/BeforeMagnetRegion/Rich1/RichHPDSurfaces")

    def defineRich2GeoDet( self , detPieces ):
        self.removeBeamPipeElements( "rich2" )
        detPieces['AfterMagnetRegion']+=['Rich2']

    def defineRich2GeoStream( self ):
        self._listOfGeoObjects_.append("/dd/Geometry/AfterMagnetRegion/Rich2/Rich2Surfaces")

    def defineRichPhys( self, gmpl ):
        gmpl.PhysicsConstructors.append("GiGaPhysConstructorOp")
        gmpl.PhysicsConstructors.append("GiGaPhysConstructorHpd")

    ##
    ##
    def configureRichSim (self, slot, detHits ):
        from Configurables import (
            GetMCRichHitsAlg,
            GetMCRichOpticalPhotonsAlg,
            GetMCRichSegmentsAlg,
            GetMCRichTracksAlg
            )

        #for slot in SpillOverSlots:
        richHitsSeq = GaudiSequencer( "RichHits" + slot )
        detHits.Members += [ richHitsSeq ]
        richHitsSeq.Members = [ GetMCRichHitsAlg( "GetRichHits"+slot),
                                GetMCRichOpticalPhotonsAlg("GetRichPhotons"+slot),
                                GetMCRichSegmentsAlg("GetRichSegments"+slot),
                                GetMCRichTracksAlg("GetRichTracks"+slot),
                                Rich__MC__MCPartToMCRichTrackAlg("MCPartToMCRichTrack"+slot),
                                Rich__MC__MCRichHitToMCRichOpPhotAlg("MCRichHitToMCRichOpPhot"+slot) ]


    def configureRichMoni ( self, slot, packCheckSeq, detMoniSeq, checkHits, configuredRichMoni ):

        # reinstate checkHits default value
        checkHits.RichHits = 'MC/Rich/Hits'

        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :
            from Configurables import DataPacking__Unpack_LHCb__MCRichHitPacker_
            from Configurables import DataPacking__Unpack_LHCb__MCRichOpticalPhotonPacker_
            from Configurables import DataPacking__Unpack_LHCb__MCRichSegmentPacker_
            from Configurables import DataPacking__Unpack_LHCb__MCRichTrackPacker_

            upRichHit  = DataPacking__Unpack_LHCb__MCRichHitPacker_("UnpackRichHits"+slot,
                                                                    OutputName = "MC/Rich/HitsTest" )
            upRichOpPh = DataPacking__Unpack_LHCb__MCRichOpticalPhotonPacker_("UnpackRichOpPhot"+slot,
                                                                              OutputName = "MC/Rich/OpticalPhotonsTest" )
            upRichSeg  = DataPacking__Unpack_LHCb__MCRichSegmentPacker_("UnpackRichSegments"+slot,
                                                                        OutputName = "MC/Rich/SegmentsTest" )
            upRichTrk  = DataPacking__Unpack_LHCb__MCRichTrackPacker_("UnpackRichTracks"+slot,
                                                                      OutputName = "MC/Rich/TracksTest" )
            packCheckSeq.Members += [upRichHit,upRichOpPh,upRichSeg,upRichTrk]


            from Configurables import DataPacking__Check_LHCb__MCRichHitPacker_
            from Configurables import DataPacking__Check_LHCb__MCRichOpticalPhotonPacker_
            from Configurables import DataPacking__Check_LHCb__MCRichSegmentPacker_
            from Configurables import DataPacking__Check_LHCb__MCRichTrackPacker_
            cRichHit  = DataPacking__Check_LHCb__MCRichHitPacker_("CheckRichHits"+slot )
            cRichOpPh = DataPacking__Check_LHCb__MCRichOpticalPhotonPacker_("CheckRichOpPhot"+slot )
            cRichSeg  = DataPacking__Check_LHCb__MCRichSegmentPacker_("CheckRichSegments"+slot )
            cRichTrk  = DataPacking__Check_LHCb__MCRichTrackPacker_("CheckRichTracks"+slot )
            packCheckSeq.Members += [cRichHit,cRichOpPh,cRichSeg,cRichTrk]
        # Out of loop
        if ( self.getProp("CurrentRICHSimRunOption") == "clunker" ):
            if not configuredRichMoni[1]:
                importOptions("$GAUSSRICHROOT/options/RichAnalysis.opts")
                configuredRichMoni[1] = True
        elif (GaussRICHConf().getProp("MakeRichG4MonitorHistoSet2") ):
            if not configuredRichMoni[1]:
                configuredRichMoni[1] = True

#"""
#  ><<<<<<<     ><<     ><<    ><<     ><<    ><<<<<<<   ><<       ><< ><<< ><<<<<<
#  ><<    ><<   ><<  ><<   ><< ><<     ><<    ><<    ><< >< ><<   ><<<      ><<
#  ><<    ><<   ><< ><<        ><<     ><<    ><<    ><< ><< ><< > ><<      ><<
#  >< ><<       ><< ><<        ><<<<<< ><<    ><<<<<<<   ><<  ><<  ><<      ><<
#  ><<  ><<     ><< ><<        ><<     ><<    ><<        ><<   ><  ><<      ><<
#  ><<    ><<   ><<  ><<   ><< ><<     ><<    ><<        ><<       ><<      ><<
#  ><<      ><< ><<    ><<<<   ><<     ><<    ><<        ><<       ><<      ><<
#
#"""

    def defineRich1MaPmtGeoDet( self , detPieces ):
        self.removeBeamPipeElements( "rich1" )
        detPieces['BeforeMagnetRegion']+=['Rich1']

    def defineRich1MaPmtGeoStream( self, geoCnvSvc ):
        self._listOfGeoObjects_.append("/dd/Geometry/BeforeMagnetRegion/Rich1/Rich1Surfaces")
        self._listOfGeoObjects_.append("/dd/Geometry/BeforeMagnetRegion/Rich1/RichPMTSurfaces")
        geoCnvSvc.UseAlignment = False
        geoCnvSvc.AlignAllDetectors = False


    def defineRich2MaPmtGeoDet( self , detPieces ):
        self.removeBeamPipeElements( "rich2" )
        detPieces['AfterMagnetRegion']+=['Rich2']

    def defineRich2MaPmtGeoStream( self, geoCnvSvc ):
        self._listOfGeoObjects_.append("/dd/Geometry/AfterMagnetRegion/Rich2/Rich2Surfaces")
        geoCnvSvc.UseAlignment = False
        geoCnvSvc.AlignAllDetectors = False


    def defineRichMaPmtPhys( self, gmpl):
        gmpl.PhysicsConstructors.append("GiGaPhysConstructorOpCkv")
        gmpl.PhysicsConstructors.append("GiGaPhysConstructorPhotoDetector")




    def configureRichMaPmtSim (self, slot, detHits ):
        from Configurables import (
            GetMCCkvHitsAlg,
            GetMCCkvOpticalPhotonsAlg,
            GetMCCkvSegmentsAlg,
            GetMCCkvTracksAlg
            )
        richHitsSeq = GaudiSequencer( "RichHits" + slot )
        detHits.Members += [ richHitsSeq ]
        richHitsSeq.Members = [ GetMCCkvHitsAlg( "GetRichHits"+slot),
                                GetMCCkvOpticalPhotonsAlg("GetRichPhotons"+slot),
                                GetMCCkvSegmentsAlg("GetRichSegments"+slot),
                                GetMCCkvTracksAlg("GetRichTracks"+slot),
                                Rich__MC__MCPartToMCRichTrackAlg("MCPartToMCRichTrack"+slot),
                                Rich__MC__MCRichHitToMCRichOpPhotAlg("MCRichHitToMCRichOpPhot"+slot) ]


    def configureRichMaPmtMoni ( self, slot, packCheckSeq, detMoniSeq, checkHits, configuredRichMoni ):
        checkHits.RichHits = 'MC/Rich/Hits'

        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :
            from Configurables import DataPacking__Unpack_LHCb__MCRichHitPacker_
            from Configurables import DataPacking__Unpack_LHCb__MCRichOpticalPhotonPacker_
            from Configurables import DataPacking__Unpack_LHCb__MCRichSegmentPacker_
            from Configurables import DataPacking__Unpack_LHCb__MCRichTrackPacker_

            upRichHit  = DataPacking__Unpack_LHCb__MCRichHitPacker_("UnpackRichHits"+slot,
                                                                    OutputName = "MC/Rich/HitsTest" )
            upRichOpPh = DataPacking__Unpack_LHCb__MCRichOpticalPhotonPacker_("UnpackRichOpPhot"+slot,
                                                                              OutputName = "MC/Rich/OpticalPhotonsTest" )
            upRichSeg  = DataPacking__Unpack_LHCb__MCRichSegmentPacker_("UnpackRichSegments"+slot,
                                                                        OutputName = "MC/Rich/SegmentsTest" )
            upRichTrk  = DataPacking__Unpack_LHCb__MCRichTrackPacker_("UnpackRichTracks"+slot,
                                                                      OutputName = "MC/Rich/TracksTest" )
            packCheckSeq.Members += [upRichHit,upRichOpPh,upRichSeg,upRichTrk]


            from Configurables import DataPacking__Check_LHCb__MCRichHitPacker_
            from Configurables import DataPacking__Check_LHCb__MCRichOpticalPhotonPacker_
            from Configurables import DataPacking__Check_LHCb__MCRichSegmentPacker_
            from Configurables import DataPacking__Check_LHCb__MCRichTrackPacker_
            cRichHit  = DataPacking__Check_LHCb__MCRichHitPacker_("CheckRichHits"+slot )
            cRichOpPh = DataPacking__Check_LHCb__MCRichOpticalPhotonPacker_("CheckRichOpPhot"+slot )
            cRichSeg  = DataPacking__Check_LHCb__MCRichSegmentPacker_("CheckRichSegments"+slot )
            cRichTrk  = DataPacking__Check_LHCb__MCRichTrackPacker_("CheckRichTracks"+slot )
            packCheckSeq.Members += [cRichHit,cRichOpPh,cRichSeg,cRichTrk]
        # Out of indent


        if ( self.getProp("UpgradeRICHSimRunOption") == "clunker" ):
            if not configuredRichMoni[1]:
                importOptions("$GAUSSCHERENKOVROOT/options/GaussCherenkovAnalysis.opts")
                configuredRichMoni[1] = True
        elif (GaussCherenkovConf().getProp("MakeCkvG4MonitorHistoSet2")):
            if not configuredRichMoni[1]:
                configuredRichMoni[1] = True



#"""
#><<< ><<<<<<     ><<<<      ><<<<<<<         ><<    ><<     ><<
#     ><<       ><<    ><<   ><<    ><<    ><<   ><< ><<     ><<
#     ><<     ><<        ><< ><<    ><<   ><<        ><<     ><<
#     ><<     ><<        ><< >< ><<       ><<        ><<<<<< ><<
#     ><<     ><<        ><< ><<  ><<     ><<        ><<     ><<
#     ><<       ><<     ><<  ><<    ><<    ><<   ><< ><<     ><<
#     ><<         ><<<<      ><<      ><<    ><<<<   ><<     ><<
#
#"""
    def defineTorchGeo( self ):
        log.warning("Geo not defined for TORCH")
        pass

    def configureTorchSim( self, slot, detHits ):
        log.warning("Sim not defined for TORCH")
        pass

    def configureTorchMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        #detMoniSeq = GaudiSequencer( "DetectorsMonitor" + slot )
        log.warning ("Moni not defined for TORCH")
        pass




#"""
#><<< ><<<<<< ><<< ><<<<<<
#     ><<          ><<
#     ><<          ><<
#     ><<          ><<
#     ><<          ><<
#     ><<          ><<
#     ><<          ><<
#"""


    def defineTTGeo( self , detPieces ):
        self.removeBeamPipeElements( "tt" )
        if 'TT' not in detPieces['BeforeMagnetRegion']:
            detPieces['BeforeMagnetRegion']+=['TT']


    def configureTTSim( self, slot, detHits ):
        region   = "BeforeMagnetRegion"
        det = "TT"
        moni = GetTrackerHitsAlg(
            'Get' + det + 'Hits' + slot,
            MCHitsLocation = 'MC/' + det  + '/Hits',
            CollectionName = det + 'SDet/Hits',
            Detectors = [ '/dd/Structure/LHCb/' + region + '/' + det ]
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')


    def configureTTMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.TTHits = 'MC/TT/Hits'

        myZStations = [
            2350.0*SystemOfUnits.mm,
            2620.0*SystemOfUnits.mm
            ]
        myZStationXMax = 150.*SystemOfUnits.cm
        myZStationYMax = 150.*SystemOfUnits.cm

        detMoniSeq.Members += [
            MCHitMonitor(
                "TTHitMonitor" + slot ,
                mcPathString = "MC/TT/Hits",
                zStations = myZStations,
                xMax = myZStationXMax,
                yMax = myZStationYMax
                )
            ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )

            from Configurables import DataPacking__Unpack_LHCb__MCTTHitPacker_
            from Configurables import DataPacking__Check_LHCb__MCTTHitPacker_
            upTT   = DataPacking__Unpack_LHCb__MCTTHitPacker_("UnpackTTHits"+slot,
                                                              OutputName = "MC/TT/HitsTest" )
            cTT   = DataPacking__Check_LHCb__MCTTHitPacker_("CheckTTHits"+slot )
            packCheckSeq.Members += [upTT, cTT]



#"""
# ><<     ><< ><<< ><<<<<<
# ><<     ><<      ><<
# ><<     ><<      ><<
# ><<     ><<      ><<
# ><<     ><<      ><<
# ><<     ><<      ><<
#   ><<<<<         ><<
#"""


    def defineUTGeo( self , detPieces ):
        self.removeBeamPipeElements( "ut" )
        if 'UT' not in detPieces['BeforeMagnetRegion']:
            detPieces['BeforeMagnetRegion']+=['UT']


    def configureUTSim( self, slot, detHits ):
        region   = "BeforeMagnetRegion"
        det = "UT"
        moni = GetTrackerHitsAlg(
            'Get' + det + 'Hits' + slot,
            MCHitsLocation = 'MC/' + det  + '/Hits',
            CollectionName = det + 'SDet/Hits',
            Detectors = [ '/dd/Structure/LHCb/' + region + '/' + det ]
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')


    def configureUTMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.UTHits = 'MC/UT/Hits'

        myZStations = [
            2350.0*SystemOfUnits.mm,
            2620.0*SystemOfUnits.mm
            ]
        myZStationXMax = 150.*SystemOfUnits.cm
        myZStationYMax = 150.*SystemOfUnits.cm

        detMoniSeq.Members += [
            MCHitMonitor(
                "UTHitMonitor" + slot ,
                mcPathString = "MC/UT/Hits",
                zStations = myZStations,
                xMax = myZStationXMax,
                yMax = myZStationYMax
                )
            ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )

            from Configurables import DataPacking__Unpack_LHCb__MCUTHitPacker_
            from Configurables import DataPacking__Check_LHCb__MCUTHitPacker_
            upUT   = DataPacking__Unpack_LHCb__MCUTHitPacker_("UnpackUTHits"+slot,
                                                              OutputName = "MC/UT/HitsTest" )
            cUT   = DataPacking__Check_LHCb__MCUTHitPacker_("CheckUTHits"+slot )
            packCheckSeq.Members += [upUT, cUT]




#"""
#  ><<  ><<< ><<<<<<
#  ><<       ><<
#  ><<       ><<
#  ><<       ><<
#  ><<       ><<
#  ><<       ><<
#  ><<       ><<
#
#"""

    def defineITGeo( self , detPieces ):
        self.removeBeamPipeElements( "t" )
        region = "AfterMagnetRegion"
        if 'T' not in detPieces[region]:
            detPieces[region]+=['T/IT']
        if 'T/PipeInT' not in detPieces[region]:
            detPieces[region]+=['T/PipeInT']


    def configureITSim( self, slot, detHits ):
        region   = "AfterMagnetRegion/T"
        det = "IT"
        moni = GetTrackerHitsAlg(
            'Get' + det + 'Hits' + slot,
            MCHitsLocation = 'MC/' + det  + '/Hits',
            CollectionName = det + 'SDet/Hits',
            Detectors = [ '/dd/Structure/LHCb/' + region + '/' + det ]
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')


    def configureITMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.ITHits = 'MC/IT/Hits'

        myZStations = [
            7780.0*SystemOfUnits.mm,
            8460.0*SystemOfUnits.mm,
            9115.0*SystemOfUnits.mm
            ]
        myZStationXMax = 150.*SystemOfUnits.cm
        myZStationYMax = 150.*SystemOfUnits.cm

        # Upgrade
        #if self.getProp("DataType") == "Upgrade" :
        #    myZStations = [
        #        8015.0*SystemOfUnits.mm,
        #        8697.0*SystemOfUnits.mm,
        #        9363.0*SystemOfUnits.mm
        #        ]

        detMoniSeq.Members += [
            MCHitMonitor(
                "ITHitMonitor" + slot ,
                mcPathString = "MC/IT/Hits",
                zStations = myZStations,
                xMax = myZStationXMax,
                yMax = myZStationYMax
                )
            ]

        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCITHitPacker_
            upIT   = DataPacking__Unpack_LHCb__MCITHitPacker_("UnpackITHits"+slot,
                                                              OutputName = "MC/IT/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCITHitPacker_
            cIT   = DataPacking__Check_LHCb__MCITHitPacker_("CheckITHits"+slot )
            packCheckSeq.Members += [upIT, cIT]


#"""
#
#     Si IT
#
#"""

    def defineSLGeo( self , detPieces ):
        self.removeBeamPipeElements( "t" )
        region = "AfterMagnetRegion"
        if 'T' not in detPieces[region]:
            detPieces[region]+=['T/SL']
        # PSZ - line below might need to go depending on SL definition
        if 'T/PipeInT' not in detPieces[region]:
            detPieces[region]+=['T/PipeInT']


    def configureSLSim( self, slot, detHits ):
        region   = "AfterMagnetRegion/T"
        det = "SL"
        moni = GetTrackerHitsAlg(
            'Get' + det + 'Hits' + slot,
            MCHitsLocation = 'MC/' + det  + '/Hits',
            CollectionName = det + 'SDet/Hits',
            Detectors = [ '/dd/Structure/LHCb/' + region + '/' + det ]
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')
        pass

    #def configureSLMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
    #    pass
    def configureSLMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.SLHits = 'MC/SL/Hits'

        myZStations = [
            7780.0*SystemOfUnits.mm,
            #8460.0*SystemOfUnits.mm,
            9115.0*SystemOfUnits.mm
            ]
        myZStationXMax = 150.*SystemOfUnits.cm
        myZStationYMax = 150.*SystemOfUnits.cm

        detMoniSeq.Members += [
            MCHitMonitor(
                "SLHitMonitor" + slot ,
                mcPathString = "MC/SL/Hits",
                zStations = myZStations,
                xMax = myZStationXMax,
                yMax = myZStationYMax
                )
            ]

        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCSLHitPacker_
            upSL   = DataPacking__Unpack_LHCb__MCSLHitPacker_("UnpackSLHits"+slot,
                                                              OutputName = "MC/SL/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCSLHitPacker_
            cSL   = DataPacking__Check_LHCb__MCSLHitPacker_("CheckSLHits"+slot )
            packCheckSeq.Members += [upSL, cSL]



#"""
#   ><<<<<<<< ><<< ><<<<<<
#   ><<            ><<
#   ><<            ><<
#   ><<<<<<        ><<
#   ><<            ><<
#   ><<            ><<
#   ><<            ><<
#
#"""
    def defineFTGeo( self , detType , detPieces ):
        self.removeBeamPipeElements( "t" )
        region = "AfterMagnetRegion"
        if 'T' not in detPieces[region]:
            detPieces[region]+=['T/FT']
        if 'T/PipeInT' not in detPieces[region]:
            detPieces[region]+=['T/PipeInT']
        if detType == "ft":
            region = "DownstreamRegion"
            detPieces[region]+=['NeutronShielding']


    def configureFTSim( self, slot, detHits ):
        region   = "AfterMagnetRegion/T"
        det = "FT"
        moni = GetTrackerHitsAlg(
            'Get' + det + 'Hits' + slot,
            MCHitsLocation = 'MC/' + det  + '/Hits',
            CollectionName = det + 'SDet/Hits',
            Detectors = [ '/dd/Structure/LHCb/' + region + '/' + det ]
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')


    def configureFTMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.FTHits = 'MC/FT/Hits'


        # Upgrade
        myZStations = [
            7938.0*SystemOfUnits.mm,
            8625.0*SystemOfUnits.mm,
            9315.0*SystemOfUnits.mm
            ]
        myZStationXMax = 100.*SystemOfUnits.cm
        myZStationYMax = 100.*SystemOfUnits.cm

        detMoniSeq.Members += [
            MCHitMonitor(
                "FTHitMonitor" + slot ,
                mcPathString = "MC/FT/Hits",
                zStations = myZStations,
                xMax = myZStationXMax,
                yMax = myZStationYMax
                )
            ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :
            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCFTHitPacker_
            upFT = DataPacking__Unpack_LHCb__MCFTHitPacker_("UnpackFTHits"+slot,
                                                              OutputName = "MC/FT/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCFTHitPacker_
            cFT  = DataPacking__Check_LHCb__MCFTHitPacker_("CheckFTHits"+slot )
            packCheckSeq.Members += [upFT, cFT]


#"""
#    ><<<<      ><<< ><<<<<<
#  ><<    ><<        ><<
#><<        ><<      ><<
#><<        ><<      ><<
#><<        ><<      ><<
#  ><<     ><<       ><<
#    ><<<<           ><<
#"""

    def defineOTGeo( self , detPieces ):
        self.removeBeamPipeElements( "t" )
        region = "AfterMagnetRegion"
        if 'T' not in detPieces[region]:
            detPieces[region]+=['T/OT']
        if 'T/PipeInT' not in detPieces[region]:
            detPieces[region]+=['T/PipeInT']



    def configureOTSim( self, slot, detHits ):
        region   = "AfterMagnetRegion/T"
        det = "OT"
        moni = GetTrackerHitsAlg(
            'Get' + det + 'Hits' + slot,
            MCHitsLocation = 'MC/' + det  + '/Hits',
            CollectionName = det + 'SDet/Hits',
            Detectors = [ '/dd/Structure/LHCb/' + region + '/' + det ]
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')

    def configureOTMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.OTHits = 'MC/OT/Hits'

        myZStations = [
            7938.0*SystemOfUnits.mm,
            8625.0*SystemOfUnits.mm,
            9315.0*SystemOfUnits.mm
            ]
        myZStationXMax = 100.*SystemOfUnits.cm
        myZStationYMax = 100.*SystemOfUnits.cm

        # Upgrade
        #if self.getProp("DataType") == "Upgrade" :
        #    myZStations = [
        #        7672.0*SystemOfUnits.mm,
        #        8354.0*SystemOfUnits.mm,
        #        9039.0*SystemOfUnits.mm
        #        ]

        detMoniSeq.Members += [
            MCHitMonitor(
                "OTHitMonitor" + slot ,
                mcPathString = "MC/OT/Hits",
                zStations = myZStations,
                xMax = myZStationXMax,
                yMax = myZStationYMax
                )
            ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCOTHitPacker_
            upOT   = DataPacking__Unpack_LHCb__MCOTHitPacker_("UnpackOTHits"+slot,
                                                              OutputName = "MC/OT/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCOTHitPacker_
            cOT   = DataPacking__Check_LHCb__MCOTHitPacker_("CheckOTHits"+slot )
            packCheckSeq.Members += [upOT, cOT]



#"""
#   ><<       ><<
#   >< ><<   ><<<
#   ><< ><< > ><< ><<  ><<    ><<     ><< ><<
#   ><<  ><<  ><< ><<  ><<  ><<  ><<   ><<  ><<
#   ><<   ><  ><< ><<  ><< ><<    ><<  ><<  ><<
#   ><<       ><< ><<  ><<  ><<  ><<   ><<  ><<
#   ><<       ><<   ><<><<    ><<     ><<<  ><<
#
#"""

    def defineMuonGeo( self, detPieces ):
        #self.removeBeamPipeElements( "muon" )
        region = 'DownstreamRegion'
        detPieces[region]+=['Muon']


    def configureMuonSim ( self, slot, detHits ):
        det = "Muon"
        moni = GetTrackerHitsAlg( "Get"+det+"Hits"+slot,
                                  MCHitsLocation = 'MC/' + det + '/Hits',
                                  CollectionName = det + 'SDet/Hits',
                                  Detectors = ['/dd/Structure/LHCb/DownstreamRegion/'+det] )
        detHits.Members += [ moni ]
        self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')


    def configureMuonMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        det = "Muon"
        checkHits.MuonHits = 'MC/Muon/Hits'

        detMoniSeq.Members += [ MuonHitChecker( det + "HitChecker" + slot,
                                                FullDetail = True )]
## The following should only be done in expert checks
#        from Configurables import MuonMultipleScatteringChecker
#        detMoniSeq.Members += [
#            MuonMultipleScatteringChecker( "MuonMultipleScatteringChecker"+ slot )]

        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )

            from Configurables import DataPacking__Unpack_LHCb__MCMuonHitPacker_
            upMu   = DataPacking__Unpack_LHCb__MCMuonHitPacker_("UnpackMuonHits"+slot,
                                                                OutputName = "MC/Muon/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCMuonHitPacker_
            cMu   = DataPacking__Check_LHCb__MCMuonHitPacker_("CheckMuonHits"+slot )
            packCheckSeq.Members += [upMu, cMu]




#"""
#       ><<          ><        ><<           ><<<<
#    ><<   ><<      >< <<      ><<         ><<    ><<
#   ><<            ><  ><<     ><<       ><<        ><<
#   ><<           ><<   ><<    ><<       ><<        ><<
#   ><<          ><<<<<< ><<   ><<       ><<        ><<
#    ><<   ><<  ><<       ><<  ><<         ><<     ><<
#      ><<<<   ><<         ><< ><<<<<<<<     ><<<<
#
#"""

    def defineSpdGeo( self, detPieces ):
        self.removeBeamPipeElements("calo")
        region = 'DownstreamRegion'
        detPieces[region]+=['Spd']
        detPieces[region]+=['Converter']

    def definePrsGeo( self, detPieces ):
        region = 'DownstreamRegion'
        detPieces[region]+=['Prs']

    def defineEcalGeo( self, detPieces ):
        region = 'DownstreamRegion'
        detPieces[region]+=['Ecal']

    def defineHcalGeo( self, detPieces ):
        region = 'DownstreamRegion'
        detPieces[region]+=['Hcal']


    def configureSpdSim ( self, slot, detHits ):
        det = "Spd"
        moni = GetCaloHitsAlg(
            "Get"+det+"Hits"+slot,
            MCHitsLocation = 'MC/' + det + '/Hits',
            CollectionName = det + 'Hits'
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mccalohits(slot, 'MC/' + det + '/Hits')

    def configurePrsSim ( self, slot, detHits ):
        det = "Prs"
        moni = GetCaloHitsAlg(
            "Get"+det+"Hits"+slot,
            MCHitsLocation = 'MC/' + det + '/Hits',
            CollectionName = det + 'Hits'
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mccalohits(slot, 'MC/' + det + '/Hits')

    def configureEcalSim ( self, slot, detHits ):
        det = "Ecal"
        moni = GetCaloHitsAlg(
            "Get"+det+"Hits"+slot,
            MCHitsLocation = 'MC/' + det + '/Hits',
            CollectionName = det + 'Hits'
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mccalohits(slot, 'MC/' + det + '/Hits')

    def configureHcalSim ( self, slot, detHits ):
        det = "Hcal"
        moni = GetCaloHitsAlg(
            "Get"+det+"Hits"+slot,
            MCHitsLocation = 'MC/' + det + '/Hits',
            CollectionName = det + 'Hits'
            )
        detHits.Members += [ moni ]
        self.configure_redecay_mccalohits(slot, 'MC/' + det + '/Hits')




    def configureSpdMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.CaloHits.append('MC/Spd/Hits')

        det = "Spd"
        detMoniSeq.Members += [
            MCCaloMonitor(
                det + "Monitor" + slot,
                OutputLevel = 4,
                Detector = det,
                Regions = True,
                MaximumEnergy = 10.*SystemOfUnits.MeV,
                Threshold = 1.5*SystemOfUnits.MeV
                )
            ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCSpdHitPacker_
            upSpd  = DataPacking__Unpack_LHCb__MCSpdHitPacker_("UnpackSpdHits"+slot,
                                                               OutputName = "MC/Spd/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCSpdHitPacker_
            cSpd  = DataPacking__Check_LHCb__MCSpdHitPacker_("CheckSpdHits"+slot)
            packCheckSeq.Members += [upSpd,cSpd]



    def configurePrsMoni( self, slot, packCheckSeq, detMoniSeq, checkHits):
        # reinstate checkHits default value
        checkHits.CaloHits.append('MC/Prs/Hits')

        det = "Prs"
        detMoniSeq.Members += [
            MCCaloMonitor(
                det + "Monitor" + slot,
                OutputLevel = 4,
                Detector = 'Prs',
                Regions = True,
                MaximumEnergy = 10.*SystemOfUnits.MeV,
                Threshold = 1.5*SystemOfUnits.MeV
                )
            ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCPrsHitPacker_
            upPrs  = DataPacking__Unpack_LHCb__MCPrsHitPacker_("UnpackPrsHits"+slot,
                                                               OutputName = "MC/Prs/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCPrsHitPacker_
            cPrs  = DataPacking__Check_LHCb__MCPrsHitPacker_("CheckPrsHits"+slot)
            packCheckSeq.Members += [upPrs,cPrs]




    def configureEcalMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.CaloHits.append('MC/Ecal/Hits')

        det = "Ecal"
        detMoniSeq.Members += [
            MCCaloMonitor(
                det + "Monitor" + slot,
                OutputLevel = 4,
                Detector = det,
                Regions = True,
                MaximumEnergy = 1000.*SystemOfUnits.MeV,
                Threshold = 10.*SystemOfUnits.MeV
                )
            ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCEcalHitPacker_
            upEcal = DataPacking__Unpack_LHCb__MCEcalHitPacker_("UnpackEcalHits"+slot,
                                                                OutputName = "MC/Ecal/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCEcalHitPacker_
            cEcal = DataPacking__Check_LHCb__MCEcalHitPacker_("CheckEcalHits"+slot)
            packCheckSeq.Members += [upEcal,cEcal]




    def configureHcalMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):
        # reinstate checkHits default value
        checkHits.CaloHits.append('MC/Hcal/Hits')

        det = "Hcal"
        detMoniSeq.Members += [
            MCCaloMonitor(
                det + "Monitor" + slot,
                OutputLevel = 4,
                Detector = det,
                Regions = True,
                MaximumEnergy = 1000.*SystemOfUnits.MeV,
                Threshold = 5.*SystemOfUnits.MeV
                )
            ]
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

            packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
            from Configurables import DataPacking__Unpack_LHCb__MCHcalHitPacker_
            upHcal = DataPacking__Unpack_LHCb__MCHcalHitPacker_("UnpackHcalHits"+slot,
                                                                OutputName = "MC/Hcal/HitsTest" )
            from Configurables import DataPacking__Check_LHCb__MCHcalHitPacker_
            cHcal = DataPacking__Check_LHCb__MCHcalHitPacker_("CheckHcalHits"+slot)
            packCheckSeq.Members += [upHcal,cHcal]


########################################################################################################
#
# Herschel
#
########################################################################################################

    def defineHCGeo(self, detPieces):
      year = self.getProp("DataType")
      if year not in self.Run2DataTypes:
        log.warning( "Check your options: you have asked to simulate Herschel but not in %s."%year )
        log.warning( "Simulating only the LHC tunnel.")
      # Add the non-standard pieces of the BeforeMagnet region.
      region = 'BeforeMagnetRegion'
      if detPieces.has_key(region):
        pieces = ['PipeJunctionBeforeVelo', 'BeforeVelo']
        for piece in pieces:
          if piece in detPieces[region]: continue
          detPieces[region] += [piece]
      # Add the AfterMuon part of the Downstream region.
      region = 'DownstreamRegion'
      if detPieces.has_key(region):
        pieces = ['AfterMuon']
        for piece in pieces:
          if piece in detPieces[region]: continue
          detPieces[region] += [piece]
      # Add the entire Upstream, BeforeUpstream, and AfterDownstream regions.
      regions = ['UpstreamRegion', 'BeforeUpstreamRegion',
                 'AfterDownstreamRegion']
      for region in regions:
        if detPieces.has_key(region):
          detPieces[region] = []
        self._listOfGeoObjects_.append("/dd/Structure/LHCb/" + region)

      # Extend the world volume.
      if self.getProp('UseGaussGeo'):
        GaussGeo().ZsizeOfWorldVolume = 300.0 * SystemOfUnits.m
      else:
        GiGaGeo().ZsizeOfWorldVolume = 300.0 * SystemOfUnits.m

    def configureHCSim(self, slot, detHits):
      year = self.getProp("DataType")
      if year not in self.Run2DataTypes:
        raise RuntimeError( "Asking to simulate Herschel response but not present in %s" %year )

      det = "HC"
      getHits = GetTrackerHitsAlg("Get" + det + "Hits" + slot)
      getHits.MCHitsLocation = "MC/" + det + "/Hits"
      getHits.CollectionName = det + "SDet/Hits"
      getHits.Detectors = ["/dd/Structure/LHCb/UpstreamRegion/HCB0",
                           "/dd/Structure/LHCb/UpstreamRegion/HCB1",
                           "/dd/Structure/LHCb/BeforeUpstreamRegion/HCB2",
                           "/dd/Structure/LHCb/DownstreamRegion/AfterMuon/HCF1",
                           "/dd/Structure/LHCb/AfterDownstreamRegion/HCF2"]
      detHits.Members += [getHits]

    def configureHCMoni(self, slot, packCheckSeq, detMoniSeq, checkHits):
      from Configurables import HCHitChecker
      detMoniSeq.Members += [HCHitChecker("HCHitChecker" + slot)]

      if self.getProp("EnablePack") and self.getProp("DataPackingChecks"):
        packCheckSeq = GaudiSequencer("DataUnpackTest" + slot)
        from Configurables import DataPacking__Unpack_LHCb__MCHCHitPacker_
        upHC = DataPacking__Unpack_LHCb__MCHCHitPacker_("UnpackHCHits" + slot,
                                                        OutputName = "MC/HC/HitsTest")
        packCheckSeq.Members += [upHC]

        from Configurables import DataPacking__Check_LHCb__MCHCHitPacker_
        cHC = DataPacking__Check_LHCb__MCHCHitPacker_("CheckHCHits" + slot)
        packCheckSeq.Members += [cHC]


########################################################################################################
#
# Beam Condition Monitors
#
########################################################################################################

    def defineBcmGeo(self, detPieces):
      # Add the non-standard pieces of the BeforeMagnet region.
      region = 'BeforeMagnetRegion'
      if detPieces.has_key(region):
        pieces = ['PipeJunctionBeforeVelo', 'BeforeVelo']
        for piece in pieces:
          if piece in detPieces[region]: continue
          detPieces[region] += [piece]
      # Add the entire Upstream region.
      region = 'UpstreamRegion'
      if detPieces.has_key(region):
        detPieces[region] = []
      self._listOfGeoObjects_.append("/dd/Structure/LHCb/" + region)
      # Add the AfterMuon part of the Downstream region.
      region = 'DownstreamRegion'
      if detPieces.has_key(region):
        pieces = ['AfterMuon']
        for piece in pieces:
          if piece in detPieces[region]: continue
          detPieces[region] += [piece]

    def configureBcmSim(self, slot, detHits):
      det = "Bcm"
      getHits = GetTrackerHitsAlg("Get" + det + "Hits" + slot)
      getHits.MCHitsLocation = "MC/" + det + "/Hits"
      getHits.CollectionName = det + "SDet/Hits"
      getHits.Detectors = ["/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/BcmUp",
                           "/dd/Structure/LHCb/MagnetRegion/BcmDown"]
      detHits.Members += [getHits]

    def configureBcmMoni(self, slot, packCheckSeq, detMoniSeq, checkHits):
      from Configurables import BcmHitChecker
      checkerUp = BcmHitChecker("BcmHitCheckerUp" + slot)
      checkerUp.BcmDetLocation = "/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/BcmUp"
      checkerUp.MonitorInDetail = False
      checkerDn = BcmHitChecker("BcmHitCheckerDown" + slot)
      checkerDn.BcmDetLocation = "/dd/Structure/LHCb/MagnetRegion/BcmDown"
      checkerDn.MonitorInDetail = False
      detMoniSeq.Members += [checkerUp, checkerDn]

      if self.getProp("EnablePack") and self.getProp("DataPackingChecks"):
        packCheckSeq = GaudiSequencer("DataUnpackTest" + slot)
        from Configurables import DataPacking__Unpack_LHCb__MCBcmHitPacker_
        upBcm = DataPacking__Unpack_LHCb__MCBcmHitPacker_("UnpackBcmHits" + slot,
                                                          OutputName = "MC/Bcm/HitsTest")
        packCheckSeq.Members += [upBcm]

        from Configurables import DataPacking__Check_LHCb__MCBcmHitPacker_
        cBcm = DataPacking__Check_LHCb__MCBcmHitPacker_("CheckBcmHits" + slot)
        packCheckSeq.Members += [cBcm]


########################################################################################################
#
# Beam Loss Scintillators
#
########################################################################################################
#

    def defineBlsGeo(self, detPieces):
      # Add the non-standard pieces of the BeforeMagnet region.
      region = 'BeforeMagnetRegion'
      if detPieces.has_key(region):
        pieces = ['PipeJunctionBeforeVelo', 'BeforeVelo']
        for piece in pieces:
          if piece in detPieces[region]: continue
          detPieces[region] += [piece]
      # Add the non-standard pieces of the Upstream region,
      # unless the Upstream region has been added as a whole.
      region = 'UpstreamRegion'
      path = '/dd/Structure/LHCb/' + region
      if detPieces.has_key(region) and path not in self._listOfGeoObjects_:
        pieces = ['BlockWallUpstr']
        for piece in pieces:
          if piece in detPieces[region]: continue
          detPieces[region] += [piece]

    def configureBlsSim(self, slot, detHits):
      det = "Bls"
      getHits = GetTrackerHitsAlg("Get" + det + "Hits" + slot)
      getHits.MCHitsLocation = "MC/" + det + "/Hits"
      getHits.CollectionName = det + "SDet/Hits"
      # The geometry of the BLS changed in 2011
      idBLS = ["3", "4", "5", "6", "7", "8"]
      if self.getProp("DataType") in [ "2009", "2010" ]:
          idBLS = ["1", "2"]
      getHits.Detectors = []
      for iDet in idBLS:
         getHits.Detectors.append("/dd/Structure/LHCb/BeforeMagnetRegion/BeforeVelo/Bls"+iDet)
      detHits.Members += [getHits]


    def configureBlsMoni(self, slot, packCheckSeq, detMoniSeq, checkHits):
      from Configurables import BlsHitChecker
      checkerA = BlsHitChecker("BlsHitCheckerBlsA" + slot)
      checkerA.HistoDir = "BlsHitChecker/BlsHitCheckerBlsA"
      checkerA.BlsAOn = TRUE
      checkerA.HistogramTitlePrefix = "BlsA: "
      checkerC = BlsHitChecker("BlsHitCheckerBlsC" + slot)
      checkerC.HistoDir = "BlsHitChecker/BlsHitCheckerBlsC"
      checkerC.BlsCOn = TRUE
      checkerC.HistogramTitlePrefix = "BlsC: "
      detMoniSeq.Members += [checkerA, checkerC]

      if self.getProp("EnablePack") and self.getProp("DataPackingChecks"):
        packCheckSeq = GaudiSequencer("DataUnpackTest" + slot)
        from Configurables import DataPacking__Unpack_LHCb__MCBlsHitPacker_
        upBls = DataPacking__Unpack_LHCb__MCBlsHitPacker_("UnpackBlsHits" + slot,
                                                          OutputName = "MC/Bls/HitsTest")
        packCheckSeq.Members += [upBls]

        from Configurables import DataPacking__Check_LHCb__MCBlsHitPacker_
        cBls = DataPacking__Check_LHCb__MCBlsHitPacker_("CheckBlsHits" + slot)
        packCheckSeq.Members += [cBls]


#"""
#    ><<       ><<                                             ><<
#    >< ><<   ><<<                                             ><<
#    ><< ><< > ><<    ><<        ><<    ><< ><<      ><<     ><>< ><
#    ><<  ><<  ><<  ><<  ><<   ><<  ><<  ><<  ><<  ><   ><<    ><<
#    ><<   ><  ><< ><<   ><<  ><<   ><<  ><<  ><< ><<<<< ><<   ><<
#    ><<       ><< ><<   ><<   ><<  ><<  ><<  ><< ><           ><<
#    ><<       ><<   ><< ><<<      ><<  ><<<  ><<   ><<<<       ><<
#                           ><<
#"""

    def defineMagnetGeo( self , basePieces, detPieces ):
        # Turn off magnet if false
        path = "dd/Structure/LHCb/MagnetRegion/"
        detPieces["MagnetRegion"] = ['Magnet','BcmDown']
        # PSZ - check why this is here
        #if False:
        #    for element in detPieces['MagnetRegion']:
        #        myElement = path + element
        #        if myElement in self._listOfGeoObjects_:
        #            self._listOfGeoObjects_.remove([ path + element ])

        # PSZ - clean me up
        #if False:
        #    GiGaGeo().FieldManager           = "GiGaFieldMgr/FieldMgr"
        #    GiGaGeo().addTool( GiGaFieldMgr("FieldMgr"), name="FieldMgr" )
        #    GiGaGeo().FieldMgr.Stepper       = "ClassicalRK4"
        #    GiGaGeo().FieldMgr.Global        = True
        #    GiGaGeo().FieldMgr.MagneticField = "GiGaMagFieldGlobal/LHCbField"
        #    GiGaGeo().FieldMgr.addTool( GiGaMagFieldGlobal("LHCbField"), name="LHCbField" )
        #    GiGaGeo().FieldMgr.LHCbField.MagneticFieldService = "MagneticFieldSvc"


    def defineMagnetGeoField( self ):
        # Only bother with the FIELD Geometry if simulated.
        simDets = self.getProp('DetectorSim')['Detectors']
        if "Magnet" in simDets or "HC" in simDets:
            if self.getProp('UseGaussGeo'):
                GaussGeo().FieldManager           = "GiGaFieldMgr/FieldMgr"
                GaussGeo().addTool( GiGaFieldMgr("FieldMgr"), name="FieldMgr" )
                GaussGeo().FieldMgr.Stepper       = "ClassicalRK4"
                GaussGeo().FieldMgr.Global        = True
                GaussGeo().FieldMgr.MagneticField = "GiGaMagFieldGlobal/LHCbField"
                GaussGeo().FieldMgr.addTool( GiGaMagFieldGlobal("LHCbField"), name="LHCbField" )
                GaussGeo().FieldMgr.LHCbField.MagneticFieldService = "MagneticFieldSvc"
            else:
                GiGaGeo().FieldManager           = "GiGaFieldMgr/FieldMgr"
                GiGaGeo().addTool( GiGaFieldMgr("FieldMgr"), name="FieldMgr" )
                GiGaGeo().FieldMgr.Stepper       = "ClassicalRK4"
                GiGaGeo().FieldMgr.Global        = True
                GiGaGeo().FieldMgr.MagneticField = "GiGaMagFieldGlobal/LHCbField"
                GiGaGeo().FieldMgr.addTool( GiGaMagFieldGlobal("LHCbField"), name="LHCbField" )
                GiGaGeo().FieldMgr.LHCbField.MagneticFieldService = "MagneticFieldSvc"

        if "HC" in simDets:
            from Configurables import MagneticFieldSvc, MultipleMagneticFieldSvc
            # Use MultipleMagneticFieldSvc instead of default MagneticFieldSvc.
            if self.getProp('UseGaussGeo'):
                GaussGeo().FieldMgr.LHCbField.MagneticFieldService = "MultipleMagneticFieldSvc"
            else:
                GiGaGeo().FieldMgr.LHCbField.MagneticFieldService = "MultipleMagneticFieldSvc"
            # Add LHCb dipole magnet and compensators.
            if "Magnet" in simDets:
              MultipleMagneticFieldSvc().MagneticFieldServices += ["MagneticFieldSvc"]
              importOptions("$MAGNETROOT/options/UseCompensators.py")
            # Import LSS fields.
            importOptions("$MAGNETROOT/options/UseTripletLeft.py")
            importOptions("$MAGNETROOT/options/UseTripletRight.py")
            # Scale dipoles and quadrupoles.
            scalableMagnets = ["Q1", "Q2", "Q3", "D1", "MCBX"]
            magnets = MultipleMagneticFieldSvc().getProp("MagneticFieldServices")
            scale =  self.getProp("BeamMomentum") / (3.5 * SystemOfUnits.TeV )
            for magnet in magnets:
              if any(m in magnet for m in scalableMagnets):
                 MagneticFieldSvc(magnet).ForcedSignedCurrentScaling = scale
                 print "Scaling", magnet, "by", scale




#"""
# ><<<<<<<            ><<         ><<              ><<
# ><<    ><<           ><<       ><<               ><<
# ><<    ><< ><<  ><<   ><<     ><<      ><<     ><>< ><    ><<
# ><<<<<<<   ><<  ><<    ><<   ><<     ><   ><<    ><<    ><<  ><<
# ><<        ><<  ><<     ><< ><<     ><<<<< ><<   ><<   ><<    ><<
# ><<        ><<  ><<      ><<<<      ><           ><<    ><<  ><<
# ><<          ><<><<       ><<         ><<<<       ><<     ><<
#
#"""

    def definePuVetoGeo( self ):
        pass

    def configurePuVetoSim( self, slot, detHits ):
        region = "BeforeMagnetRegion"
        det = "PuVeto"
        # This is still awful - PSZ
        detextra, detextra1 = 'VeloPu', 'Velo'
        if 'VP' not in self.getProp('DetectorSim')['Detectors']:
            moni = GetTrackerHitsAlg(
                'Get' + det + 'Hits' + slot,
                MCHitsLocation = 'MC/' + det  + '/Hits',
                CollectionName = 'VeloPuSDet/Hits',
                Detectors = [ '/dd/Structure/LHCb/' + region + '/Velo' ]
                )
            detHits.Members += [ moni ]
            self.configure_redecay_mchits(slot, 'MC/' + det  + '/Hits')

    def configurePuVetoMoni( self, slot, packCheckSeq, detMoniSeq, checkHits ):

        checkHits.PuVetoHits = 'MC/PuVeto/Hits'
        # Turn off the PuVeto hits if using modified detector
        if 'VP' not in self.getProp('DetectorSim')['Detectors']:
            checkHits.PuVetoHits = ''

        from Configurables import DataPacking__Unpack_LHCb__MCPuVetoHitPacker_
        upPuVe = DataPacking__Unpack_LHCb__MCPuVetoHitPacker_("UnpackPuVetoHits"+slot,
                                                              OutputName = "MC/PuVeto/HitsTest" )
        if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :
            packCheckSeq.Members += [upPuVe]

            from Configurables import DataPacking__Check_LHCb__MCPuVetoHitPacker_
            # if there's no VP do PuVeto stuff
            if 'VP' not in self.getProp('DetectorSim')['Detectors']:
                cPuVe = DataPacking__Check_LHCb__MCPuVetoHitPacker_("CheckPuVetoHits"+slot)
                packCheckSeq.Members += [cPuVe]




#"""
##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################
#"""



#"""
#    ><<                          ><<    ><<<<<<<                  ><<
# ><<   ><<                     ><       ><<    ><<                ><<
#><<          ><<    ><< ><<  ><>< ><    ><<    ><<  ><< ><<       ><<
#><<        ><<  ><<  ><<  ><<  ><<      >< ><<       ><<  ><< ><< ><<
#><<       ><<    ><< ><<  ><<  ><<      ><<  ><<     ><<  ><<><   ><<
# ><<   ><< ><<  ><<  ><<  ><<  ><<      ><<    ><<   ><<  ><<><   ><<
#   ><<<<     ><<    ><<<  ><<  ><<      ><<      ><<><<<  ><< ><< ><<
#"""


    ##
    ## Functions to configuration various services that are used
    ##
    def configureRndmEngine( self ):
        # Random number service
        rndmSvc = RndmGenSvc()
        rndmGenName = self.getProp("RandomGenerator")
        if rndmGenName == "Ranlux":
            from Configurables import HepRndm__Engine_CLHEP__RanluxEngine_
            engine = HepRndm__Engine_CLHEP__RanluxEngine_("RndmGenSvc.Engine")
            engine.SetSingleton = True
        elif rndmGenName == "MTwist" :
            rndmSvc.Engine = "HepRndm::Engine<CLHEP::MTwistEngine>"
            from Configurables import HepRndm__Engine_CLHEP__MTwistEngine_
            engine = HepRndm__Engine_CLHEP__MTwistEngine_("RndmGenSvc.Engine")
            engine.SetSingleton = True
        else :
            raise RuntimeError("ERROR: RandomNumber engine '%s' not recognised!" %rndmGenName)



#"""
#    ><<                          ><<    ><<                             ><<
# ><<   ><<                     ><       ><<                             ><<
#><<          ><<    ><< ><<  ><>< ><    ><< ><< ><<   >< ><<  ><<  ><<><>< ><
#><<        ><<  ><<  ><<  ><<  ><<      ><<  ><<  ><< ><  ><< ><<  ><<  ><<
#><<       ><<    ><< ><<  ><<  ><<      ><<  ><<  ><< ><   ><<><<  ><<  ><<
# ><<   ><< ><<  ><<  ><<  ><<  ><<      ><<  ><<  ><< ><< ><< ><<  ><<  ><<
#   ><<<<     ><<    ><<<  ><<  ><<      ><< ><<<  ><< ><<       ><<><<   ><<
#                                                      ><<
#"""

    def configureInput(self):
        # No events are read as input (this is not true if gen phase is
        # switched off
        ApplicationMgr().EvtSel = 'NONE'
        # Transient store setup
        EventDataSvc().ForceLeaves = True
        # May be needed by some options
        importOptions("$STDOPTS/PreloadUnits.opts")



#"""
#    ><<                                ><<         ><<<<<                   ><<
# ><<   ><< ><<                         ><<         ><<   ><<   ><           ><<
#><<        ><<         ><<        ><<< ><<  ><<    ><<    ><<        ><<< ><>< ><  ><<<<
#><<        >< ><     ><   ><<   ><<    ><< ><<     ><<    ><< ><<  ><<      ><<   ><<
#><<        ><<  ><< ><<<<< ><< ><<     ><><<       ><<    ><< ><< ><<       ><<     ><<<
# ><<   ><< ><   ><< ><          ><<    ><< ><<     ><<   ><<  ><<  ><<      ><<       ><<
#   ><<<<   ><<  ><<   ><<<<       ><<< ><<  ><<    ><<<<<     ><<    ><<<    ><<  ><< ><<
#
#"""

    ## Raise an error if DetectorGeo/DetectorSim/DetectorMoni are not compatible
    def fixGeoSimMoniDictionary ( self ) :
        pass

    def checkGeoSimMoniDictionary ( self ) :
        for subdet in self.TrackingSystem + self.PIDSystem:
            # Could do something smarter here
            for det in self.getProp('DetectorSim')['Detectors']:
                if self.getProp('DetectorGeo')['Detectors'].count(det) == 0 :
                    if (det not in ['FT', 'FT-NoShield']) or ( ('FT' not in self.getProp('DetectorGeo')['Detectors']) and ('FT-NoShield' not in self.getProp('DetectorGeo')['Detectors']) ):
                        raise RuntimeError("Simulation has been required for '%s' sub-detector but it has been removed from Geometry" %det)
            for det in self.getProp('DetectorMoni')['Detectors']:
                if self.getProp('DetectorSim')['Detectors'].count(det) == 0 :
                    if (det not in ['FT', 'FT-NoShield']) or ( ('FT' not in self.getProp('DetectorSim')['Detectors']) and ('FT-NoShield' not in self.getProp('DetectorSim')['Detectors']) ):
                        raise RuntimeError("Monitoring has been required for '%s' sub-detector but it has been removed from Simulation" %det)


    def checkIncompatibleDetectors ( self ) :
        for section in self._incompatibleDetectors.keys():
            incompatList = self._incompatibleDetectors[section]
            myList = [det for det in self.getProp("DetectorGeo")['Detectors'] if det in incompatList]
            if len(myList) > 1:
                raise RuntimeError ( "Incompatible detectors: %s in %s section." %(myList, section) )



    ##
    def outputName(self):
        """
        Build a name for the output file, based on input options.
        Combines DatasetName, EventType, Number of events and Date
        """
        import time
        outputName = self.getProp("DatasetName")
        if self.eventType() != "":
            if outputName != "": outputName += '-'
            outputName += self.eventType()
        if ( self.evtMax() > 0 ): outputName += '-' + str(self.evtMax()) + 'ev'
        if outputName == "": outputName = 'Gauss'
        idFile = str(time.localtime().tm_year)
        if time.localtime().tm_mon < 10:
            idFile += '0'
        idFile += str(time.localtime().tm_mon)
        if time.localtime().tm_mday < 10:
            idFile += '0'
        idFile += str(time.localtime().tm_mday)
        outputName += '-' + idFile
        return outputName


#"""
# ><<<<<                  ><<   ><<<<<<<                                        ><<
# ><<   ><<             ><      ><<    ><<                          ><          ><<
# ><<    ><<   ><<    ><>< ><   ><<    ><<   ><<    >< ><<< ><<<<       ><<<< ><>< ><
# ><<    ><< ><   ><<   ><<     ><<<<<<<   ><   ><<  ><<   ><<     ><< ><<      ><<
# ><<    ><<><<<<< ><<  ><<     ><<       ><<<<< ><< ><<     ><<<  ><<   ><<<   ><<
# ><<   ><< ><          ><<     ><<       ><         ><<       ><< ><<     ><<  ><<
# ><<<<<      ><<<<     ><<     ><<         ><<<<   ><<<   ><< ><< ><< ><< ><<   ><<
#"""


    ##

#"""
#
# ><<<<<<<                                   ><< <<                        ><<                            ><<
# ><<    ><<                               ><<    ><<  ><               ><<   ><<                       ><
# ><<    ><< >< ><<<   ><<     >< ><<       ><<           ><<< ><< ><< ><<          ><<     ><< ><<   ><>< ><
# ><<<<<<<    ><<    ><<  ><<  ><  ><<        ><<     ><<  ><<  ><  ><<><<        ><<  ><<   ><<  ><<   ><<
# ><<         ><<   ><<    ><< ><   ><<          ><<  ><<  ><<  ><  ><<><<       ><<    ><<  ><<  ><<   ><<
# ><<         ><<    ><<  ><<  ><< ><<     ><<    ><< ><<  ><<  ><  ><< ><<   ><< ><<  ><<   ><<  ><<   ><<
# ><<        ><<<      ><<     ><<           ><< <<   ><< ><<<  ><  ><<   ><<<<     ><<     ><<<  ><<   ><<
#                              ><<
#
#"""

    def propagateSimConf( self ):
        # Propagate properties to SimConf
        SimConf().setProp("Writer","GaussTape")
        self.setOtherProps( SimConf(), ["SpilloverPaths","EnablePack","Phases","DataType"] )
        # if we have post-sim filters, we only want to write if the filter is passed
        if self.getProp("PostSimFilters") :
            OutputStream("GaussTape").RequireAlgs.append( "PostSimFilterSeq" )


        # CRJ : Propagate detector list to SimConf. Probably could be simplified a bit
        #       by sychronising the options in Gauss() and SimConf()
        detlist = []
        if 'Velo'    in self.getProp('DetectorSim')['Detectors'] : detlist += ['Velo']
        if 'PuVeto'  in self.getProp('DetectorSim')['Detectors'] : detlist += ['PuVeto']
        if 'TT'      in self.getProp('DetectorSim')['Detectors'] : detlist += ['TT']
        if 'IT'      in self.getProp('DetectorSim')['Detectors'] : detlist += ['IT']
        if 'OT'      in self.getProp('DetectorSim')['Detectors'] : detlist += ['OT']
        if [det for det in ['Rich1', 'Rich2', 'Rich1Pmt', 'Rich2Pmt'] if det in self.getProp('DetectorSim')['Detectors']] :
            detlist += ['Rich']
        if 'Muon'    in self.getProp('DetectorSim')['Detectors'] : detlist += ['Muon']
        if 'Spd'     in self.getProp('DetectorSim')['Detectors'] : detlist += ['Spd']
        if 'Prs'     in self.getProp('DetectorSim')['Detectors'] : detlist += ['Prs']
        if 'Ecal'    in self.getProp('DetectorSim')['Detectors'] : detlist += ['Ecal']
        if 'Hcal'    in self.getProp('DetectorSim')['Detectors'] : detlist += ['Hcal']
        if 'HC'      in self.getProp('DetectorSim')['Detectors'] : detlist += ['HC']
        # GC - 20160323 Bmc and Bls Off for now
        ## if 'Bcm'     in self.getProp('DetectorSim')['Detectors'] : detlist += ['Bcm']
        ## if 'Bls'     in self.getProp('DetectorSim')['Detectors'] : detlist += ['Bls']
        # PSZ - add upgrade detectors here
        if 'VP'      in self.getProp('DetectorSim')['Detectors'] : detlist += ['VP']
        if 'UT'      in self.getProp('DetectorSim')['Detectors'] : detlist += ['UT']
        if ('FT' in self.getProp('DetectorSim')['Detectors']) or ('FT-NoShield' in self.getProp('DetectorSim')['Detectors']) :
            detlist += ['FT']
        if 'SL'    in self.getProp('DetectorSim')['Detectors'] : detlist += ['SL']
        # if Skip4 then dont propagate the detector list
        if "GenToMCTree" in self.getProp("Phases"):
            detlist = []

        SimConf().setProp("Detectors",detlist)


        # Don't want SIM data unpacking enabled in DoD service
        SimConf().EnableUnpack = False


#"""
#      ><<                                                              ><<                     ><<
#   ><<   ><<                                  ><                       ><<        ><           ><<
#  ><<        >< ><<<   ><<     ><<<<  ><<<<      ><< ><<      ><<      ><<            ><<<<  ><>< ><
#  ><<         ><<    ><<  ><< ><<    ><<     ><<  ><<  ><<  ><<  ><<   ><<       ><< ><<       ><<
#  ><<         ><<   ><<    ><<  ><<<   ><<<  ><<  ><<  ><< ><<   ><<   ><<       ><<   ><<<    ><<
#   ><<   ><<  ><<    ><<  ><<     ><<    ><< ><<  ><<  ><<  ><<  ><<   ><<       ><<     ><<   ><<
#     ><<<<   ><<<      ><<    ><< ><<><< ><< ><< ><<<  ><<      ><<    ><<<<<<<< ><< ><< ><<    ><<
#                                                            ><<
#"""
    def defineCrossingList( self ):
        crossingList = [ '' ]
        spillOverList = self.getProp("SpilloverPaths")
        while '' in spillOverList :
            spillOverList.remove('')
        crossingList += spillOverList
        return crossingList


#"""
#
#  ><< ><<                                          ><<<<<<<
#  ><    ><<                                        ><<    ><<
#  ><     ><<    ><<        ><<     ><<< ><< ><<    ><<    ><<    ><<     >< ><<<    ><<     ><<< ><< ><<   ><<<<
#  ><<< ><     ><   ><<   ><<  ><<   ><<  ><  ><<   ><<<<<<<    ><<  ><<   ><<     ><<  ><<   ><<  ><  ><< ><<
#  ><     ><< ><<<<< ><< ><<   ><<   ><<  ><  ><<   ><<        ><<   ><<   ><<    ><<   ><<   ><<  ><  ><<   ><<<
#  ><      >< ><         ><<   ><<   ><<  ><  ><<   ><<        ><<   ><<   ><<    ><<   ><<   ><<  ><  ><<     ><<
#  ><<<< ><<    ><<<<      ><< ><<< ><<<  ><  ><<   ><<          ><< ><<< ><<<      ><< ><<< ><<<  ><  ><< ><< ><<
#"""
    #--Set the energy of the beam,
    #--the half effective crossing angle (in LHCb coordinate system),
    #--beta* and emittance
    #--and configure the colliding beam tool for all type of events in
    #--pp collisions.
    def setBeamParameters( self, CrossingSlots, genInit):

        from Configurables import ( MinimumBias , FixedNInteractions , HijingProduction )
        from Configurables import ( Special , Pythia8Production )
        from Configurables import ( Generation )

        #
        beamMom                        = self.getProp("BeamMomentum")
        xAngle                         = self.getProp("BeamHCrossingAngle")
        yAngle                         = self.getProp("BeamVCrossingAngle")
        xAngleBeamLine, yAngleBeamLine = self.getProp("BeamLineAngles")
        emittance                      = self.getProp("BeamEmittance")
        betaStar                       = self.getProp("BeamBetaStar")
        lumiPerBunch                   = self.getProp("Luminosity")
        totCrossSection                = self.getProp("TotalCrossSection")
        meanX, meanY, meanZ            = self.getProp("InteractionPosition")
        sigmaS                         = self.getProp("BunchRMS")
        b2Mom                          = self.getProp("B2Momentum")
        B1Particle                     = self.getProp("B1Particle")
        B2Particle                     = self.getProp("B2Particle")

        # Give beam parameters to GenInit algorithm
        genInit.CreateBeam              = True
        genInit.BeamEnergy              = beamMom
        genInit.HorizontalCrossingAngle = xAngle
        genInit.VerticalCrossingAngle   = yAngle
        genInit.NormalizedEmittance     = emittance
        genInit.BetaStar                = betaStar
        genInit.HorizontalBeamlineAngle = xAngleBeamLine
        genInit.VerticalBeamlineAngle   = yAngleBeamLine
        genInit.Luminosity              = lumiPerBunch
        genInit.TotalCrossSection       = totCrossSection
        genInit.XLuminousRegion         = meanX
        genInit.YLuminousRegion         = meanY
        genInit.ZLuminousRegion         = meanZ
        genInit.BunchLengthRMS          = sigmaS

        gen_t0 = Generation("Generation")

        # the following is for beam gas events, the values are just to give the
        # nominal beam conditions in the data but 1 single interaction is
        # forced selecting the appropriate pileup tool in the eventtype
        gen_t0.addTool(FixedNInteractions,name="FixedNInteractions")
        gen_t0.FixedNInteractions.NInteractions = 1

        # or with Hijing
        pInGeV   = beamMom*SystemOfUnits.GeV/SystemOfUnits.TeV
        txtP = "hijinginit efrm "+str(pInGeV)
        gen_t0.addTool(MinimumBias,name="MinimumBias")
        gen_t0.MinimumBias.addTool(HijingProduction,name="HijingProduction")
        gen_t0.MinimumBias.HijingProduction.Commands += [ txtP ]


        ## handle the information for HI generation (EPOS or HIJING)
        ## Is it a fixed target generation ?
        isFixedTarget = ( ( beamMom == 0. ) or (b2Mom == 0. ) )

        ## Setup EPOS particle type
        gen_t0.MinimumBias.addTool(CRMCProduction,name="CRMCProduction")
        if ( B1Particle not in self.__ion_pdg_id__.keys() ):
            raise RuntimeError( "Unknown particle type: %s"  % B1Particle )
        if ( B2Particle not in self.__ion_pdg_id__.keys() ):
            raise RuntimeError( "Unknown particle type: %s"  % B2Particle )
        gen_t0.MinimumBias.CRMCProduction.ProjectileID = self.__ion_pdg_id__[ B1Particle ]
        gen_t0.MinimumBias.CRMCProduction.TargetID = self.__ion_pdg_id__[ B2Particle ]
        gen_t0.MinimumBias.CRMCProduction.ProjectileMomentum = beamMom / SystemOfUnits.GeV
        gen_t0.MinimumBias.CRMCProduction.TargetMomentum = b2Mom / SystemOfUnits.GeV

        ## Setup HIJING particle type
        if ( B1Particle != 'p' ):
            Zproj = str( self.__ion_pdg_id__[ B1Particle ] )[ 3:5 ]
            Aproj = str( self.__ion_pdg_id__[ B1Particle ] )[ 6:8 ]
            textOptionHijing = "hijinginit izp %s," % Zproj
            textOptionHijing+= "hijinginit iap %s," % Aproj
            textOptionHijing+= "hijinginit proj A,"
        else:
            textOptionHijing = "hijinginit izp 1,"
            textOptionHijing+= "hijinginit iap 1,"
            textOptionHijing+= "hijinginit proj P,"
        if ( B2Particle != 'p' ):
            Ztarg = str( self.__ion_pdg_id__[ B2Particle ] )[ 3:5 ]
            Atarg = str( self.__ion_pdg_id__[ B2Particle ] )[ 6:8 ]
            textOptionHijing+= "hijinginit izt %s," % Ztarg
            textOptionHijing+= "hijinginit iat %s," % Atarg
            textOptionHijing+= "hijinginit targ A,"
        else:
            textOptionHijing+= "hijinginit izt 1,"
            textOptionHijing+= "hijinginit iat 1,"
            textOptionHijing+= "hijinginit targ P,"

        if isFixedTarget:
            textOptionHijing+= "hijinginit frame LAB,"
            if ( beamMom == 0 ):
                textOptionHijing+= "hijinginit beam2"
            else:
                textOptionHijing+= "hijinginit beam1"
        else:
            textOptionHijing+= "hijinginit frame CMS"
        gen_t0.MinimumBias.HijingProduction.Commands += [ textOptionHijing ]

    #--For beam gas events (with hijing) only the energy of the beams is set

    #--Set location for histogram particle guns based on beam energy
        from Configurables import ParticleGun, MomentumSpectrum
        pgun = ParticleGun("ParticleGun")
        pgun.addTool( MomentumSpectrum , name = "MomentumSpectrum" )
        txtPInGeV = str(pInGeV).split(".")[0]
        hFileName = pgun.MomentumSpectrum.getProp("InputFile")
        hFileName = hFileName.replace("Ebeam4000GeV","Ebeam"+txtPInGeV+"GeV")
        pgun.MomentumSpectrum.InputFile = hFileName
        print hFileName

    ## end of functions to set beam paramters and propagate them
    ##########################################################################



#"""
#      ><<                             ><<       ><<<<
#   ><<   ><<                        ><        ><    ><<
#  ><<           ><<     ><< ><<   ><>< ><    ><<            ><<     ><< ><<
#  ><<         ><<  ><<   ><<  ><<   ><<      ><<          ><   ><<   ><<  ><<
#  ><<        ><<    ><<  ><<  ><<   ><<      ><<   ><<<< ><<<<< ><<  ><<  ><<
#   ><<   ><<  ><<  ><<   ><<  ><<   ><<       ><<    ><  ><          ><<  ><<
#     ><<<<      ><<     ><<<  ><<   ><<        ><<<<<      ><<<<    ><<<  ><<
#
#"""
    def configureGen( self, SpillOverSlots ):
        """
        Set up the generator execution sequence and its sub-phases
        """

##         if "Gen" not in self.getProp("MainSequence") :
##             log.warning("No generator phase. Need input file")
##             return

        do_redecay = self.Redecay['active']
        if self.evtMax() <= 0:
            raise RuntimeError( "Generating events but selected '%s' events. Use LHCbApp().EvtMax " %self.evtMax() )

        gaussGeneratorSeq = GaudiSequencer( "Generator", IgnoreFilterPassed = True )
        gaussSeq = GaudiSequencer("GaussSequencer")
        gaussSeq.Members += [ gaussGeneratorSeq ]

        from Configurables import ( EvtGenDecay )
        EvtGenDecay().DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"


        from Configurables import ( GenInit, Generation, MaskParticles )
        for slot in SpillOverSlots:
            genSequence = GaudiSequencer("GeneratorSlot"+self.slotName(slot)+"Seq" )
            gaussGeneratorSeq.Members += [ genSequence ]

            TESNode = "/Event/"+self.slot_(slot)
            genInit = GenInit("GaussGen"+slot,
                              MCHeader = TESNode+"Gen/Header")

            FSRNode = "/FileRecords/"+self.slot_(slot)

            if slot != '':
                genInitT0 = GenInit("GaussGen")
                if genInitT0.isPropertySet("RunNumber"):
                    genInit.RunNumber = genInitT0.RunNumber
                if genInitT0.isPropertySet("FirstEventNumber"):
                    genInit.FirstEventNumber = genInitT0.FirstEventNumber


            genProc = 0
            genType = self.getProp("Production").upper()
            from Configurables import ParticleGun, MIBackground
            KnownGenTypes = ['PHYS','PGUN','MIB']
            if genType not in KnownGenTypes:
                raise RuntimeError("Unknown Generation type '%s'"%genType)
            if genType == 'PHYS':
                genProc = Generation("Generation"+slot)
            elif genType == 'PGUN':
                genProc = ParticleGun("ParticleGun"+slot)
            else:
                genProc = MIBackground("MIBackground"+slot)

            genProc.GenHeaderLocation = TESNode+"Gen/Header"
            genProc.HepMCEventLocation = TESNode+"Gen/HepMCEvents"
            genProc.GenCollisionLocation = TESNode+"Gen/Collisions"

            if "GenFSRLocation" in genProc.properties():
                if not genProc.isPropertySet("GenFSRLocation"):
                    genProc.GenFSRLocation = FSRNode+"GenFSR"

            if slot != '':
                genProc.PileUpTool = 'FixedLuminosityForSpillOver'

            if do_redecay:
                # This filter checks if a new event needs to be generated,
                # if not rest of the sequencer is not run.
                gaussrdfilter = GaussRedecayCtrFilter('RegisterNewEvent{}'.format(slot))
                gaussrdfilter.RegisterNewEvent = True
                genSequence.Members += [ genInit, gaussrdfilter, genProc ]
            else:
                genSequence.Members += [ genInit , genProc ]
            # When HC simulation is switched on the very forward protons must be
            # removed from the HepMC record since they cause showers in it
            if 'HC' in self.getProp('DetectorSim')['Detectors']:
                genMask = MaskParticles("MaskDiffractiveProton"+slot,
                                        HepMCEventLocation = TESNode+"Gen/HepMCEvents")
                genSequence.Members += [genMask]

            if slot == '' and do_redecay:
                genSequence.Members += [ GaussRedecaySorter() ]


    ## end of Gen configuration
    ##########################################################################


#"""
#     ><<                             ><<    ><<<<<<<
#  ><<   ><<                        ><       ><<    ><< ><<
# ><<           ><<     ><< ><<   ><>< ><    ><<    ><< ><<         ><<      ><<<<     ><<      ><<<<
# ><<         ><<  ><<   ><<  ><<   ><<      ><<<<<<<   >< ><     ><<  ><<  ><<      ><   ><<  ><<
# ><<        ><<    ><<  ><<  ><<   ><<      ><<        ><<  ><< ><<   ><<    ><<<  ><<<<< ><<   ><<<
#  ><<   ><<  ><<  ><<   ><<  ><<   ><<      ><<        ><   ><< ><<   ><<      ><< ><             ><<
#    ><<<<      ><<     ><<<  ><<   ><<      ><<        ><<  ><<   ><< ><<< ><< ><<   ><<<<    ><< ><<
#"""
    def configurePhases( self, SpillOverSlots  ):
        """
        Set up the top level sequence and its phases
        """

        gaussSeq = GaudiSequencer("GaussSequencer")
        ApplicationMgr().TopAlg = [ gaussSeq ]
##         mainSeq = self.getProp("MainSequence")
##         if len( mainSeq ) == 0:
##             mainSeq = self.DefaultSequence

##         mainSeq = map(lambda ph: ph.capitalize(), mainSeq)
##         self.setProp("MainSequence",mainSeq)
##         for phase in mainSeq:
##             raise RuntimeError("Unknown phase '%s'"%phase)

        # if the filter sequence is non-empty, add it here
        if self.getProp("PostSimFilters") :
            filterSeq = GaudiSequencer("PostSimFilterSeq")
            if not self.getProp("SplitSim"):
                ApplicationMgr().TopAlg += [ filterSeq ]
            self.configurePostSimFilters( filterSeq )


        ### Check for configuration consistency
        if ( ( "GenToMCTree" in self.getProp("Phases") ) and ( "Simulation" in self.getProp("Phases") ) ):
            raise RuntimeError("GenToMCTree and Simulation cannot be part of Phases simultaneously")

        self.configureGen( SpillOverSlots )
        if "GenToMCTree" in self.getProp("Phases"):
            self.configureSkipGeant4( SpillOverSlots )

        # Redecay and splitted simulation both hack the simulation sequence
        # and need a specialised configuration and so does their combination,
        # which are intertwined such that a combination is difficult
        if self.Redecay['active'] and self.getProp("SplitSim"):
            self.configureRedecaySplitSim( SpillOverSlots )
            self.configureRedecay(SpillOverSlots)
        elif self.Redecay['active'] and not self.getProp("SplitSim"):
            self.configureRedecaySim( SpillOverSlots )
            self.configureRedecay(SpillOverSlots)
        elif self.getProp("SplitSim"):
            self.configureSplitSim( SpillOverSlots )
        else:
            self.configureSim( SpillOverSlots )
        self.configureMoni( SpillOverSlots ) #(expert or default)
    ## end of phase configuration
    ##########################################################################

#"""
# ><<<<<<<<  ><  ><    ><
# ><<<<<<<<  ><  ><    ><
# ><<            ><  ><><><     ><<     >< ><<<   ><<<
# ><<<<<     ><  ><    ><     ><   ><<   ><<     ><<
# ><<        ><  ><    ><    ><<<<< ><<  ><<       ><<<
# ><<        ><  ><    ><    ><          ><<         ><<
# ><<        ><  ><<<  ><<     ><<<<    ><<<      <<><<
#
#"""

    def configurePostSimFilters(self, filterSeq):
        # essentially just a list of ifs for the different filters...
        for filter in self.PostSimFilters:
            if filter in self.KnownPostSimFilters:
                # check for the name of the know filters and add them one by one
                if filter == "ConversionFilter":
                    from Configurables import ConversionFilter
                    filterSeq.Members.append( ConversionFilter("ConversionFilter") )
            else:
                raise RuntimeError("Unknown Filter '%s'"%filter)


#"""
# ><<<<<                    ><<       ><<<<                 ><<                       ><<
# ><<   ><<               ><        ><<    ><<              ><<                       ><<
# ><<    ><<    ><<     ><>< ><   ><<        ><< ><<  ><< ><>< >< >< ><<   ><<  ><< ><>< ><
# ><<    ><<  ><   ><<    ><<     ><<        ><< ><<  ><<   ><<   ><  ><<  ><<  ><<   ><<
# ><<    ><< ><<<<< ><<   ><<     ><<        ><< ><<  ><<   ><<   ><   ><< ><<  ><<   ><<
# ><<   ><<  ><           ><<       ><<     ><<  ><<  ><<   ><<   ><< ><<  ><<  ><<   ><<
# ><<<<<       ><<<<      ><<         ><<<<        ><<><<    ><<  ><<        ><<><<    ><<
#                                                                 ><<
#"""

    def defineOutput( self, SpillOverSlots ):
        """
        Set up output stream according to phase processed, the spill-over slots and the type of output
        """

        #
        knownOptions = ['NONE','GEN','XGEN','RGEN','SIM','XSIM']
        output = self.getProp("OutputType").upper()
        if output == 'NONE':
            log.warning("No event data output produced")
            return

        simWriter = SimConf().writer()

        # define default file extensions depending on the phase that has been run
        fileDefaultExtension = ".gen"
        fileAllowedExtension = [fileDefaultExtension]
        if "GenToMCTree" in self.getProp("Phases"):
            fileDefaultExtension = ".xgen"
            fileAllowedExtension = [fileDefaultExtension, ".rgen"]
        elif "Simulation" in self.getProp("Phases"):
            fileDefaultExtension = ".sim"
            fileAllowedExtension = [fileDefaultExtension, ".xsim"]

        # choose the file extension from the one selected compatibly with the phase run
        if output not in knownOptions:
            print "WARNING: OutputType not supported. Use default for chosen phases : %s" %(fileDefaultExtension)
        fileExtension = "." + output.lower()
        if fileExtension not in fileAllowedExtension:
            fileExtension = fileDefaultExtension
            print "WARNING: OutputType not supported for this phase. Use default : %s" %(fileExtension)

        # set saving or not of HepMC depending on chosen file extension
        if SimConf().isPropertySet( "SaveHepMC" ):
            print "WARNING: SimConf().SaveHepMC will be ignored. Value set by Gauss()"
        saveHepMC = False
        if fileExtension in ['.gen','.xgen','.xsim']:
            saveHepMC = True
        SimConf().setProp("SaveHepMC", saveHepMC )

        outputFile=""
        from GaudiConf import IOHelper
        if simWriter.isPropertySet( "Output" ):
            outputFile=IOHelper().undressFile(simWriter.getProp("Output"))
        else:
            outputFile=self.outputName() + fileExtension

        # Merge genFSRs
        if self.getProp("WriteFSR"):
            seqGenFSR = GaudiSequencer("GenFSRSeq")
            ApplicationMgr().TopAlg += [ seqGenFSR ]

            if self.getProp("MergeGenFSR"):
                seqGenFSR.Members += [ "GenFSRMerge" ]

        IOHelper().outStream( outputFile, simWriter, self.getProp("WriteFSR") )

        simWriter.RequireAlgs.append( 'GaussSequencer' )
        if not FileCatalog().isPropertySet("Catalogs"):
            FileCatalog().Catalogs = [ "xmlcatalog_file:NewCatalog.xml" ]

#"""
#
# ><<<<<                    ><<   ><<       ><<                            ><<
# ><<   ><<               ><      >< ><<   ><<<                       ><   ><<
# ><<    ><<    ><<     ><>< ><   ><< ><< > ><<    ><<     ><< ><<       ><>< ><    ><<     >< ><<<  ><<<<
# ><<    ><<  ><   ><<    ><<     ><<  ><<  ><<  ><<  ><<   ><<  ><< ><<   ><<    ><<  ><<   ><<    ><<
# ><<    ><< ><<<<< ><<   ><<     ><<   ><  ><< ><<    ><<  ><<  ><< ><<   ><<   ><<    ><<  ><<      ><<<
# ><<   ><<  ><           ><<     ><<       ><<  ><<  ><<   ><<  ><< ><<   ><<    ><<  ><<   ><<        ><<
# ><<<<<       ><<<<      ><<     ><<       ><<    ><<     ><<<  ><< ><<    ><<     ><<     ><<<    ><< ><<
#
#"""

    def defineMonitors( self ):

        from Configurables import ApplicationMgr, AuditorSvc, SequencerTimerTool
        ApplicationMgr().ExtSvc += [ 'AuditorSvc' ]
        ApplicationMgr().AuditAlgorithms = True
        AuditorSvc().Auditors += [ 'TimingAuditor' ]
        #SequencerTimerTool().OutputLevel = WARNING

        # Set printout level and longer algorithm" identifier in printout
        MessageSvc().OutputLevel = INFO
        #ToolSvc.EvtGenTool.OutputLevel = 4 is it still necessary to reduce print?
        MessageSvc().setWarning.append( 'XmlGenericCnv' )
        if not MessageSvc().isPropertySet("Format"):
            MessageSvc().Format = '% F%24W%S%7W%R%T %0W%M'


#"""
#   ><< <<                                        ><<     ><<               ><<
# ><<    ><<                                      ><<     ><<  ><           ><<
#  ><<          ><<     ><<     ><<    ><<        ><<     ><<      ><<<<  ><>< ><    ><<      ><<<<
#    ><<      ><<  ><<   ><<   ><<   ><   ><<     ><<<<<< ><< ><< ><<       ><<    ><<  ><<  ><<
#       ><<  ><<   ><<    ><< ><<   ><<<<< ><<    ><<     ><< ><<   ><<<    ><<   ><<    ><<   ><<<
# ><<    ><< ><<   ><<     ><><<    ><            ><<     ><< ><<     ><<   ><<    ><<  ><<      ><<
#   ><< <<     ><< ><<<     ><<       ><<<<       ><<     ><< ><< ><< ><<    ><<     ><<     ><< ><<
#"""

    def saveHistos( self ):
        """
        Set up histogram service and file name unless done in job
        """

        # ROOT persistency for histograms
        importOptions('$STDOPTS/RootHist.opts')
        from Configurables import RootHistCnv__PersSvc
        RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True

        histOpt = self.getProp("Histograms").upper()
        if histOpt not in self.KnownHistOptions:
            raise RuntimeError("Unknown Histograms option '%s'"%histOpt)
            # HistogramPersistency needed to read in histogram for calorimeter
            # so do not set ApplicationMgr().HistogramPersistency = "NONE"
            return

        # If not saving histograms do not set the name of the file
        if ( histOpt == 'NONE') :
            log.warning("No histograms produced")
            return

        # Use a default histogram file name if not already set
        if not HistogramPersistencySvc().isPropertySet( "OutputFile" ):
            histosName = self.getProp("DatasetName")
            histosName = self.outputName() + '-histos.root'
            HistogramPersistencySvc().OutputFile = histosName








#"""
#GEO
#Geo
#
#     ><<<<
#   ><    ><<
#  ><<            ><<        ><<
#  ><<          ><   ><<   ><<  ><<
#  ><<   ><<<< ><<<<< ><< ><<    ><<
#   ><<    ><  ><          ><<  ><<
#    ><<<<<      ><<<<       ><<
#
#"""



    def defineGeoBasePieces( self, basePieces ):
        #basePieces['BeforeMagnetRegion']=['Velo2Rich1']
        basePieces['BeforeUpstreamRegion']  = []
        basePieces['UpstreamRegion']        = []
        basePieces['BeforeMagnetRegion']    = []
        basePieces['MagnetRegion']          = []
        basePieces['AfterMagnetRegion']     = []
        basePieces['DownstreamRegion']      = []
        basePieces['AfterDownstreamRegion'] = []

        #basePieces['UpstreamRegion']=[]
        #basePieces['BeforeMagnetRegion']=[]
        #basePieces['MagnetRegion']=['PipeInMagnet','PipeSupportsInMagnet']
        #basePieces['AfterMagnetRegion']=['PipeAfterT','PipeSupportsAfterMagnet']
        #basePieces['DownstreamRegion']=['PipeDownstream','PipeSupportsDownstream','PipeBakeoutDownstream']

    # This is where everything is parsed into geo items
    def defineStreamItemsGeo( self, basePieces, detPieces ):
        for region in basePieces.keys():
            path = "/dd/Structure/LHCb/"+region+"/"
            if len(detPieces[region])==0 : continue
            # This should preserve order
            for element in basePieces[region] + detPieces[region]:
                myStreamItem = path + element
                if myStreamItem not in self._listOfGeoObjects_:
                    self._listOfGeoObjects_.append(myStreamItem)
            #for element in detPieces[region]:
            #    myStreamItem = path + element
            #    if myStreamItem not in self._listOfGeoObjects_:
            #        self._listOfGeoObjects_.append(myStreamItem)



#"""
#  ><<<<<                    ><<       ><<<<
#  ><<   ><<               ><        ><    ><<
#  ><<    ><<    ><<     ><>< ><    ><<            ><<        ><<
#  ><<    ><<  ><   ><<    ><<      ><<          ><   ><<   ><<  ><<
#  ><<    ><< ><<<<< ><<   ><<      ><<   ><<<< ><<<<< ><< ><<    ><<
#  ><<   ><<  ><           ><<       ><<    ><  ><          ><<  ><<
#  ><<<<<       ><<<<      ><<        ><<<<<      ><<<<       ><<
#
#"""

    def defineDetectorGeo( self, basePieces, detPieces, det ):
        import string
        lDet = det.lower()
        if lDet not in self.__knownDetectors__:
            log.warning("Geo Detector not known : %s" %(det))

        if lDet == "magnet":
            self.defineMagnetGeo( basePieces, detPieces )
        elif lDet == "puveto":
            self.definePuVetoGeo( )
        elif lDet == "velo":
            self.defineVeloGeo( basePieces, detPieces )
        elif lDet == "tt":
            self.defineTTGeo( detPieces )
        elif lDet == "it":
            self.defineITGeo( detPieces )
        elif lDet == "ot":
            self.defineOTGeo( detPieces )
        elif lDet == "muon":
            self.defineMuonGeo( detPieces )
        elif lDet == "rich1":
            self.defineRich1GeoDet( detPieces )
        elif lDet == "rich2":
            self.defineRich2GeoDet( detPieces )
        elif lDet == "spd":
            self.defineSpdGeo( detPieces )
        elif lDet == "prs":
            self.definePrsGeo( detPieces )
        elif lDet == "ecal":
            self.defineEcalGeo( detPieces )
        elif lDet == "hcal":
            self.defineHcalGeo( detPieces )
        elif lDet == "hc":
            self.defineHCGeo(detPieces)
        # GC - 20160323 Bcm and Bls off for now
        ## elif lDet == "bcm":
        ##     self.defineBcmGeo(detPieces)
        ## elif lDet == "bls":
        ##     self.defineBlsGeo(detPieces)
        # Upgrade detectors below
        elif lDet == "vp":
            self.defineVPGeo( detPieces )
        elif lDet == "torch":
            self.defineTorchGeo()
        elif (lDet == "ft") or (lDet == "ft-noshield"):
            self.defineFTGeo( lDet, detPieces )
        elif lDet == "rich1pmt":
            self.defineRich1MaPmtGeoDet( detPieces )
        elif lDet == "rich2pmt":
            self.defineRich2MaPmtGeoDet( detPieces )
        elif lDet == "ut":
            self.defineUTGeo( detPieces )
        elif lDet == "sl":
            self.defineSLGeo( detPieces )
        else:
            log.warning("Geo Detector not known : %s" %(det))

    def defineDetectorGeoStream ( self, geoCnvSvc, det ):
        import string
        lDet = det.lower()
        if lDet not in self.__knownDetectors__:
            log.warning("Geo Stream Detector not known : %s" %(det))

        if lDet == "rich1":
            self.defineRich1GeoStream()
        elif lDet == "rich2":
            self.defineRich2GeoStream()
        elif lDet == "rich1pmt":
            self.defineRich1MaPmtGeoStream( geoCnvSvc )
        elif lDet == "rich2pmt":
            self.defineRich2MaPmtGeoStream( geoCnvSvc )
        elif lDet == "magnet":
            self.defineMagnetGeoField()



    def defineGDMLGeoStream ( self, geoCnvSvc ):
        if self.getProp("ReplaceWithGDML"):
            gdmlOpt = self.getProp("ReplaceWithGDML")
            if gdmlOpt[0]["volsToReplace"]:
                for gdmlDict in self.getProp("ReplaceWithGDML"):
                    self.defineGDMLGeo ( geoCnvSvc, gdmlDict )




    def defineGeo( self ):
        # Define the simulated geometry
        geo = GiGaInputStream( "Geo",
                               ExecuteOnce = True,
                               ConversionSvcName = "GiGaGeo",
                               DataProviderSvcName  = "DetectorDataSvc" )

        gaussSimulationSeq = GaudiSequencer("Simulation")

        if not self.getProp('UseGaussGeo'):
            gaussSimulationSeq.Members += [ geo ]

        # Detector geometry to simulate
        detPieces = {'BeforeUpstreamRegion':[], 'UpstreamRegion':[],
                     'BeforeMagnetRegion':[], 'MagnetRegion':[], 'AfterMagnetRegion':[],
                     'DownstreamRegion':[], 'AfterDownstreamRegion':[]}
        #detPieces = {'BeforeMagnetRegion':[],'AfterMagnetRegion':[],'DownstreamRegion':[],'MagnetRegion':[]}
        basePieces = {}

        # Must be first!
        # This used to list all the beampipe detector elements
        # which are not "inside" another detector.
        # i.e. top-level detector elements
        # DDDB structure may change in future
        self.defineGeoBasePieces( basePieces )

        # Define beampipe
        self.validateBeamPipeSwitch ( self.getProp("BeamPipe") )
        if ("BeamPipeOn" == self.getProp("BeamPipe")):
            # BeamPipe on - add BP elements
            self.defineBeamPipeGeo ( basePieces, detPieces )

        # Set geometry conversion service
        geoCnvSvc = None
        if self.getProp('UseGaussGeo'):
            geoCnvSvc = GaussGeo()
        else:
            geoCnvSvc = GiGaGeo()

        # Use information from SIMCOND and GeometryInfo
        # Allows to be set to False by RichXPmt
        geoCnvSvc.UseAlignment      = True
        geoCnvSvc.AlignAllDetectors = True

        # Define detectors
        for det in self.getProp('DetectorGeo')['Detectors']:
            det = "%s" %det
            self.defineDetectorGeo( basePieces, detPieces, det )

        # StreamItems definition needs to be after det definition
        self.defineStreamItemsGeo( basePieces, detPieces )

        # Define detector streams for RICHes
        for det in self.getProp('DetectorGeo')['Detectors']:
            det = "%s" %det
            self.defineDetectorGeoStream( geoCnvSvc, det )

        # Seperate Calo opts
        # Returns a list containing all the elments common to both lists
        if self.getProp('UseGaussGeo'):
            if [det for det in ['Spd', 'Prs', 'Ecal', 'Hcal'] if det in self.getProp('DetectorGeo')['Detectors']]:
                importOptions("$GAUSSCALOROOT/options/GaussGeo-Calo.py")
        else:
            if [det for det in ['Spd', 'Prs', 'Ecal', 'Hcal'] if det in self.getProp('DetectorGeo')['Detectors']]:
                importOptions("$GAUSSCALOROOT/options/Calo.opts")


        # Call GDML description
        self.defineGDMLGeoStream( geoCnvSvc )

        if self.getProp("Debug"):
            print "\nDEBUG Detector Geometry Elements:"
            print "\nkey : detPieces[key]"
            for key in detPieces.keys():
                print "%s : %s" %(key, detPieces[key])
            print "\nkey : detPieces[key]"

            for key in sorted(detPieces.keys()):
                print "%s : %s" %(key, detPieces[key])

            print "\nkey : basePieces[key]"
            for key in basePieces.keys():
                print "%s : %s" %(key, basePieces[key])

            print "\nkey : Sorted basePieces[key]"
            for key in sorted(basePieces.keys()):
                print "%s : %s" %(key, basePieces[key])

            print "\ngeo items:"
            for item in self._listOfGeoObjects_:
                print "%s" %(item)

            print "\ngeo items SORTED:"
            mySortedGeoStream = self._listOfGeoObjects_[:]
            mySortedGeoStream.sort()
            for item in mySortedGeoStream:
                print "%s" %(item)

        # No BP requested - therefore remove all elements from Geo.StreamItems
        if ("BeamPipeOff" == self.getProp("BeamPipe")):
            self.removeAllBeamPipeElements()

        # Populate the list of geometry elements in the requested conversion service
        for el in self._listOfGeoObjects_:
            if self.getProp('UseGaussGeo'):
                GaussGeo().GeoItemsNames.append(el)
            else:
                geo.StreamItems.append(el)


#"""
#     ><<                             ><<      ><< <<
#  ><<   ><<                        ><       ><<    ><<  ><
# ><<           ><<     ><< ><<   ><>< ><     ><<           ><<< ><< ><<
# ><<         ><<  ><<   ><<  ><<   ><<         ><<     ><<  ><<  ><  ><<
# ><<        ><<    ><<  ><<  ><<   ><<            ><<  ><<  ><<  ><  ><<
#  ><<   ><<  ><<  ><<   ><<  ><<   ><<      ><<    ><< ><<  ><<  ><  ><<
#    ><<<<      ><<     ><<<  ><<   ><<        ><< <<   ><< ><<<  ><  ><<
#"""

    def configureDetectorSim( self, slot, detHits, det, configuredRichSim ):
        import string
        det = det.lower()
        if det not in self.__knownDetectors__:
            log.warning("Sim Detector not known : %s" %(det))

        if det == "puveto":
            self.configurePuVetoSim( slot, detHits )
        elif det == "velo":
            self.configureVeloSim( slot, detHits )
        elif det == "tt":
            self.configureTTSim( slot, detHits )
        elif det == "it":
            self.configureITSim( slot, detHits )
        elif det == "ot":
            self.configureOTSim( slot, detHits )
        elif det == "muon":
            self.configureMuonSim( slot, detHits )
        elif det in ['rich1', 'rich2']:
            if not configuredRichSim[0]:
                self.configureRichSim( slot, detHits )
                configuredRichSim[0] = True
        elif det == "spd":
            self.configureSpdSim( slot, detHits )
        elif det == "prs":
            self.configurePrsSim( slot, detHits )
        elif det == "ecal":
            self.configureEcalSim( slot, detHits )
        elif det == "hcal":
            self.configureHcalSim( slot, detHits )
        elif det == "hc":
            self.configureHCSim(slot, detHits)
        # GC - 20160323 Bcm and Bls off for now
        ## elif det == "bcm":
        ##    self.configureBcmSim(slot, detHits)
        ## elif det == "bls":
        ##    self.configureBlsSim(slot, detHits)
        # Upgrade detectors below
        elif det == "vp":
            self.configureVPSim( slot, detHits )
        elif det == "torch":
            self.configureTorchSim( slot, detHits )
        elif (det == "ft") or (det == "ft-noshield"):
            self.configureFTSim( slot, detHits )
        elif det in ['rich1pmt', 'rich2pmt']:
            if not configuredRichSim[0]:
                self.configureRichMaPmtSim( slot, detHits )
                configuredRichSim[0] = True
        elif det == "ut":
            self.configureUTSim( slot, detHits )
        elif det == "sl":
            self.configureSLSim( slot, detHits )
        else:
            if det != "magnet":
            ## Magnetic field defined in defineMagnetGeoField called
            ## via defineDetectorGeoStream in defineGeo
                log.warning("Sim Detector not known : %s" %(det))

    ##
    ##
    def configureSim( self, SpillOverSlots ):

        """
        Set up the simulation sequence
        """

        if "Simulation" not in self.getProp("Phases"):
            log.warning("No simulation phase.")
            return

        ApplicationMgr().ExtSvc += [ "GiGa" ]
        EventPersistencySvc().CnvServices += [ "GiGaKine" ]

        gaussSimulationSeq = GaudiSequencer( "Simulation" )
        gaussSeq = GaudiSequencer("GaussSequencer")
        gaussSeq.Members += [ gaussSimulationSeq ]

        gigaStore = GiGaDataStoreAlgorithm( "GiGaStore" )
        gigaStore.ConversionServices = [ "GiGaKine" ]
        gaussSimulationSeq.Members += [ gigaStore ]

        self.defineGeo()

        self.configureGiGa()

        if self.getProp('UseGaussGeo'):
            GiGa().GeometrySource = "GaussGeo"
        else:
            GiGa().GeometrySource = "GiGaGeo"

        for slot in SpillOverSlots:
            TESNode = "/Event/"+self.slot_(slot)

            mainSimSequence = GaudiSequencer( self.slotName(slot)+"EventSeq" )

            gaussSimulationSeq.Members += [ mainSimSequence ]

            mainSimSequence.Members +=  [ SimInit( self.slotName(slot)+"EventGaussSim",
                                                   GenHeader = TESNode + "Gen/Header" ,
                                                   MCHeader = TESNode + "MC/Header" ) ]

            simSeq = GaudiSequencer( self.slotName(slot)+"Simulation",
                                     RequireObjects = [ TESNode + "Gen/HepMCEvents" ] )
            mainSimSequence.Members += [ simSeq ]

            simSlotSeq = GaudiSequencer( "Make"+self.slotName(slot)+"Sim" )
            simSeq.Members += [simSlotSeq]

            # CRJ : Set RootInTES - Everything down stream will then use the correct location
            #       (assuming they use GaudiAlg get and put) so no need to set data locations
            #       by hand any more ...
            if slot != '' : simSlotSeq.RootInTES = slot

            genToSim = GenerationToSimulation( "GenToSim" + slot,
                                               LookForUnknownParticles = True )
            simSlotSeq.Members += [ genToSim ]

            simSlotSeq.Members += [ GiGaFlushAlgorithm( "GiGaFlush"+slot ) ]
            simSlotSeq.Members += [ GiGaCheckEventStatus( "GiGaCheckEvent"+slot ) ]
            simToMC = SimulationToMCTruth( "SimToMCTruth"+slot )
            simSlotSeq.Members += [ simToMC ]

            ## Detectors hits
            TESNode = TESNode + "MC/"
            detHits = GaudiSequencer( "DetectorsHits" + slot )
            simSlotSeq.Members += [ detHits ]

            # Slight trick - configuredRichSim is a list and therefore MUTABLE!
            configuredRichSim = [ False ]
            for det in self.getProp('DetectorSim')['Detectors']:
                self.configureDetectorSim( slot, detHits, det, configuredRichSim )


            # Data packing ...
            if self.getProp("EnablePack") :
                packing = GaudiSequencer(self.slotName(slot)+"EventDataPacking")
                simSlotSeq.Members += [ packing ]
                SimConf().PackingSequencers[slot] = packing
        # End of Sim Configuration




#"""
#  ><<       ><<
#  >< ><<   ><<<                       ><
#  ><< ><< > ><<    ><<     ><< ><<
#  ><<  ><<  ><<  ><<  ><<   ><<  ><< ><<
#  ><<   ><  ><< ><<    ><<  ><<  ><< ><<
#  ><<       ><<  ><<  ><<   ><<  ><< ><<
#  ><<       ><<    ><<     ><<<  ><< ><<
#
#
#"""


    def configureGeneratorMoni ( self, SpillOverSlots ):

        # Monitors for the generator:
        for slot in SpillOverSlots:

            genSequence = GaudiSequencer("GeneratorSlot" + self.slotName(slot) + "Seq" )
            genMoniSeq = GaudiSequencer("GenMonitor" + slot )
            genSequence.Members += [ genMoniSeq ]

            TESLocation = "/Event/"+self.slot_(slot)+"Gen/HepMCEvents"
            genMoniSeq.Members += [
                GenMonitorAlg(
                "GenMonitorAlg"+slot,
                HistoProduce=True,
                Input = TESLocation
                )
                ]
            #            if moniOpt == 'Debug':
            #                genMoniSeq.Members += [ DumpHepMC( "DumpHepMC"+slot,
            #                                                   OutputLevel=1,
            #                                                   Addresses = [TESLocation] ) ]


    def configureDetectorMoni(
        self,
        slot,
        packCheckSeq,
        detMoniSeq,
        checkHits,
        det,
        configuredRichMoni
        ):
        import string
        det = det.lower()
        if det not in self.__knownDetectors__:
            log.warning("Moni Detector not known : %s" %(det))

        if det == "puveto":
            self.configurePuVetoMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "velo":
            self.configureVeloMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "tt":
            self.configureTTMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "it":
            self.configureITMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "ot":
            self.configureOTMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "muon":
            self.configureMuonMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        # Since I'm switching on the subcomponents I shouldn't configure this twice.
        elif (det == "rich1") or (det == "rich2"):
            if not configuredRichMoni[0]:
                self.configureRichMoni(
                    slot,
                    packCheckSeq,
                    detMoniSeq,
                    checkHits,
                    configuredRichMoni
                    )
                configuredRichMoni[0] = True
        elif det == "prs":
            self.configurePrsMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "spd":
            self.configureSpdMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "ecal":
            self.configureEcalMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "hcal":
            self.configureHcalMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "hc":
            self.configureHCMoni(slot, packCheckSeq, detMoniSeq, checkHits)
        # GC - 20160323 Bls and Bc off for now
        ## elif det == "bcm":
        ##     self.configureBcmMoni(slot, packCheckSeq, detMoniSeq, checkHits)
        ## elif det == "bls":
        ##     self.configureBlsMoni(slot, packCheckSeq, detMoniSeq, checkHits)
        # Upgrade detectors below
        elif det == "vp":
            self.configureVPMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "torch":
            self.configureTorchMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif (det == "ft") or (det =="ft-noshield"):
            self.configureFTMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif (det == "rich1pmt") or (det == "rich2pmt"):
            if not configuredRichMoni[0]:
                self.configureRichMaPmtMoni(
                    slot,
                    packCheckSeq,
                    detMoniSeq,
                    checkHits,
                    configuredRichMoni
                    )
                configuredRichMoni[0] = True
        elif det == "ut":
            self.configureUTMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        elif det == "sl":
            self.configureSLMoni( slot, packCheckSeq, detMoniSeq, checkHits )
        else:
            log.warning("Moni Detector not known : %s" %(det))

    def resetCheckHits( self, checkHits ):
        checkHits.TTHits     = ''
        checkHits.OTHits     = ''
        checkHits.ITHits     = ''
        checkHits.PuVetoHits = ''
        checkHits.VeloHits   = ''
        checkHits.MuonHits   = ''
        checkHits.RichHits   = ''
        checkHits.CaloHits   = []

        # checkHits has as default values:
        # |-TTHits                    = 'MC/TT/Hits'
        # |-OTHits                    = 'MC/OT/Hits'
        # |-ITHits                    = 'MC/IT/Hits'
        # |-PuVetoHits                = 'MC/PuVeto/Hits'
        # |-VeloHits                  = 'MC/Velo/Hits'
        # |-MuonHits                  = 'MC/Muon/Hits'
        # |-RichTracks                = ''
        # |-RichHits                  = 'MC/Rich/Hits'
        # |-CaloHits                  = ['MC/Spd/Hits', 'MC/Prs/Hits', 'MC/Ecal/Hits', 'MC/Hcal/Hits']



    def configureSimulationMoni( self, SpillOverSlots ):

        configuredRichMoni = [ False , False ]

        # Monitors for simulation
        for slot in SpillOverSlots:
            # Reset param to configure rich for slots.
            configuredRichMoni[0] = False
            TESNode = "/Event/"+self.slot_(slot)

            simSequence = GaudiSequencer( self.slotName(slot)+"Simulation" )
            simMoniSeq = GaudiSequencer( "SimMonitor" + slot )
            simSequence.Members += [ simMoniSeq ]

            # CRJ : Set RootInTES - Everything down stream will then use the correct location
            #       (assuming they use GaudiAlg get and put) so no need to set data locations
            #       by hand any more ...
            if slot != '' : simMoniSeq.RootInTES = slot

            # Basic monitors
            simMoniSeq.Members += [
                GiGaGetEventAlg("GiGaGet"+self.slotName(slot)+"Event"),
                MCTruthMonitor( self.slotName(slot)+"MCTruthMonitor" ) ]

            # can switch off detectors, or rather switch them on (see options
            # of algorithm)
            checkHits = GiGaGetHitsAlg( "GiGaGetHitsAlg" + slot )

            # Possibly dangerous - set all strings to ''
            # due to silly default values
            self.resetCheckHits( checkHits )



            simMoniSeq.Members += [ checkHits ]

            # checkHits had default values:
            # |-TTHits                    = 'MC/TT/Hits'
            # |-OTHits                    = 'MC/OT/Hits'
            # |-ITHits                    = 'MC/IT/Hits'
            # |-PuVetoHits                = 'MC/PuVeto/Hits'
            # |-VeloHits                  = 'MC/Velo/Hits'
            # |-MuonHits                  = 'MC/Muon/Hits'
            # |-RichTracks                = ''
            # |-RichHits                  = 'MC/Rich/Hits'
            # |-CaloHits                  = ['MC/Spd/Hits', 'MC/Prs/Hits', 'MC/Ecal/Hits', 'MC/Hcal/Hits']
            # | (default: ['MC/Spd/Hits', 'MC/Prs/Hits', 'MC/Ecal/Hits', 'MC/Hcal/Hits'])


            # Should add here the switching off of properties of
            # GiGaGetHitsAlg when a given detector is not monitored
            #self.configureTTMoni( checkHits )

            # OverWrite some things if using VP
            #self.configureVPMoni( checkHits )


            #if moniOpt == 'Debug':
            #    checkHits.OutputLevel = DEBUG

            detMoniSeq = GaudiSequencer( "DetectorsMonitor" + slot )
            simMoniSeq.Members += [ detMoniSeq ]

            packCheckSeq = None
            if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :

                packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
                simMoniSeq.Members += [packCheckSeq]

                upMCV = UnpackMCVertex("UnpackMCVertex"+slot,
                                       OutputName = "MC/VerticesTest" )
                upMCP = UnpackMCParticle( "UnpackMCParticle"+slot,
                                          OutputName = "MC/ParticlesTest" )
                packCheckSeq.Members += [ upMCV, upMCP ]

                compMCV = CompareMCVertex( "CompareMCVertex"+slot,
                                           TestName = "MC/VerticesTest" )
                compMCP = CompareMCParticle( "CompareMCParticle"+slot,
                                             TestName = "MC/ParticlesTest" )
                packCheckSeq.Members += [ compMCV, compMCP ]
            #=================================================


            # Define detectors
            for det in self.getProp('DetectorMoni')['Detectors']:
                self.configureDetectorMoni(slot, packCheckSeq, detMoniSeq, checkHits, det, configuredRichMoni)


#"""
#     ><<                             ><<    ><<<<<                  ><<              ><<<<<<<                      ><<
#  ><<   ><<                        ><       ><<   ><<               ><<              ><<    ><<                    ><<
# ><<           ><<     ><< ><<   ><>< ><    ><<    ><<    ><<     ><>< ><    ><<     ><<    ><<    ><<        ><<< ><<  ><<
# ><<         ><<  ><<   ><<  ><<   ><<      ><<    ><<  ><<  ><<    ><<    ><<  ><<  ><<<<<<<    ><<  ><<   ><<    ><< ><<
# ><<        ><<    ><<  ><<  ><<   ><<      ><<    ><< ><<   ><<    ><<   ><<   ><<  ><<        ><<   ><<  ><<     ><><<
#  ><<   ><<  ><<  ><<   ><<  ><<   ><<      ><<   ><<  ><<   ><<    ><<   ><<   ><<  ><<        ><<   ><<   ><<    ><< ><<
#    ><<<<      ><<     ><<<  ><<   ><<      ><<<<<       ><< ><<<    ><<    ><< ><<< ><<          ><< ><<<    ><<< ><<  ><<
#
#"""
#    def configureDataPackingMoni( self, SpillOverSlots, simMoniSeq ):
#        # Data packing checks
#        for slot in SpillOverSlots:
#            if self.getProp("EnablePack") and self.getProp("DataPackingChecks") :
#
#                packCheckSeq = GaudiSequencer( "DataUnpackTest"+slot )
#                simMoniSeq.Members += [packCheckSeq]
#
#                upMCV = UnpackMCVertex("UnpackMCVertex"+slot,
#                                       OutputName = "MC/VerticesTest" )
#                upMCP = UnpackMCParticle( "UnpackMCParticle"+slot,
#                                          OutputName = "MC/ParticlesTest" )
#                packCheckSeq.Members += [ upMCV, upMCP ]
#
#                compMCV = CompareMCVertex( "CompareMCVertex"+slot,
#                                           TestName = "MC/VerticesTest" )
#                compMCP = CompareMCParticle( "CompareMCParticle"+slot,
#                                             TestName = "MC/ParticlesTest" )
#                packCheckSeq.Members += [ compMCV, compMCP ]
#            #=================================================




#"""
#    ><<                             ><<    ><<       ><<
# ><<   ><<                        ><       >< ><<   ><<<                       ><
#><<           ><<     ><< ><<   ><>< ><    ><< ><< > ><<    ><<     ><< ><<
#><<         ><<  ><<   ><<  ><<   ><<      ><<  ><<  ><<  ><<  ><<   ><<  ><< ><<
#><<        ><<    ><<  ><<  ><<   ><<      ><<   ><  ><< ><<    ><<  ><<  ><< ><<
# ><<   ><<  ><<  ><<   ><<  ><<   ><<      ><<       ><<  ><<  ><<   ><<  ><< ><<
#   ><<<<      ><<     ><<<  ><<   ><<      ><<       ><<    ><<     ><<<  ><< ><<
#
#"""




    def configureMoni( self, SpillOverSlots ):
        self.configureGeneratorMoni( SpillOverSlots )
        # Per-detector configuration done here:
        self.configureSimulationMoni( SpillOverSlots )
        #self.configureDataPackingMoni( SpillOverSlots , simMoniSeq )

        #if histOpt == 'Expert':
        #    # For the moment do nothing
        #    log.Warning("Not yet implemented")

        # END OF MONI CONFIG







#"""
#     ><<                             ><<       ><<<<
#  ><<   ><<                        ><        ><    ><<   ><
# ><<           ><<     ><< ><<   ><>< ><    ><<                ><<       ><<
# ><<         ><<  ><<   ><<  ><<   ><<      ><<         ><<  ><<  ><<  ><<  ><<
# ><<        ><<    ><<  ><<  ><<   ><<      ><<   ><<<< ><< ><<   ><< ><<   ><<
#  ><<   ><<  ><<  ><<   ><<  ><<   ><<       ><<    ><  ><<  ><<  ><< ><<   ><<
#    ><<<<      ><<     ><<<  ><<   ><<        ><<<<<    ><<      ><<    ><< ><<<
#                                                              ><<
#"""
    ##
    ##
    def configureGiGa(self , skipG4 = False ):
         """
         Set up the configuration for the G4 settings: physics list, cuts and actions
         """
         richUpgradeConfig = False
         UpgradeRichPmtDetector=False
         Run1Run2RichDetector=False

         giga = GiGa()


         # PSZ - Use self.getProp('DataType') in future
         # This modification now being applied.  SE
         if self.getProp("DataType") == "Upgrade" :
             richUpgradeConfig=True

         # Do some sanity checks for RICH
         if [det for det in ['Rich1Pmt', 'Rich2Pmt'] if det in self.getProp('DetectorSim')['Detectors']]:
             UpgradeRichPmtDetector = True
             if richUpgradeConfig == False :
                 log.warning( "Incompatible Datatype and Detector configuration for RICH Upgrade. Please check your Configration" )
             else:
                 log.info( "Using RICH simulation  configuration for Upgrade Run ")


         if [det for det in ['Rich1', 'Rich2'] if det in self.getProp('DetectorSim')['Detectors']]:
             Run1Run2RichDetector = True
             if richUpgradeConfig == True :
                 log.warning( "Incompaticle Datatype and Detector configuration for RICH in RUN1 and RUN2. Please check your Configuration")
             else:
                 log.info( "Using RICH simulation configuration for RUN1 and RUN2 ")

         #end of sanity checks for RICH



         ## setup the Physics list and the productions cuts
         ## the following 2 lines commented out.
         #if skipG4:
         #    richPmt = False

         self.setPhysList(richUpgradeConfig)

         ## Mandatory G4 Run action
         giga.addTool( GiGaRunActionSequence("RunSeq") , name="RunSeq" )
         giga.RunAction = "GiGaRunActionSequence/RunSeq"
         if not skipG4:
             giga.RunSeq.addTool( TrCutsRunAction("TrCuts") , name = "TrCuts" )
             # To simulate Herschel or BCM, we need to change the default cuts.
             if 'HC' in self.getProp('DetectorSim')['Detectors']:
               giga.RunSeq.TrCuts.MinZ = -125.0 * SystemOfUnits.m
               giga.RunSeq.TrCuts.MaxZ =  125.0 * SystemOfUnits.m
             # GC - 20160323 Bcm off for now
             ## elif 'Bcm' in self.getProp('DetectorSim')['Detectors']:
             ##   giga.RunSeq.TrCuts.MinZ = -25.0 * SystemOfUnits.m
             giga.RunSeq.Members += [ "TrCutsRunAction/TrCuts" ]
             giga.RunSeq.addTool( GiGaRunActionCommand("RunCommand") , name = "RunCommand" )
             giga.RunSeq.Members += [ "GiGaRunActionCommand/RunCommand" ]
             giga.RunSeq.RunCommand.BeginOfRunCommands = [
                 "/tracking/verbose 0",
                 "/tracking/storeTrajectory  1",
                 "/process/eLoss/verbose -1" ]

         giga.EventAction = "GiGaEventActionSequence/EventSeq"
         giga.addTool( GiGaEventActionSequence("EventSeq") , name="EventSeq" )
         giga.EventSeq.Members += [ "GaussEventActionHepMC/HepMCEvent" ]

         giga.TrackingAction =   "GiGaTrackActionSequence/TrackSeq"
         giga.addTool( GiGaTrackActionSequence("TrackSeq") , name = "TrackSeq" )
         giga.TrackSeq.Members += [ "GaussPreTrackAction/PreTrack" ]

         giga.SteppingAction =   "GiGaStepActionSequence/StepSeq"
         giga.addTool( GiGaStepActionSequence("StepSeq") , name = "StepSeq" )

         #Now Configure the  RICH Simulation. The old setup using options is kept for backward
         #compatibility and may be removed in the future.

         # RICH simulation configuration
         if (richUpgradeConfig):
             if (self.getProp("UpgradeRICHSimRunOption") != "clunker"):
                mGaussCherenkovConf = GaussCherenkovConf()
                mGaussCherenkovConf.InitializeGaussCherenkovConfiguration()
                mGaussCherenkovConf.setUpgradeRichDetExistFlag(UpgradeRichPmtDetector)
                mGaussCherenkovConf.setSkipUpgradeGeant4Flag(skipG4)
                mGaussCherenkovConf.ApplyGaussCherenkovConfiguration(giga)

             else:
                 #keep the old options for backward compatibility for now. It may be removed in the future
                 #The method has the following in but it shoul not be tied to the RICH!!
                 #SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/Simulation.xml"

                 self.GaussCherenkovOldSetup(giga,UpgradeRichPmtDetector,skipG4 )
         else:

             if (self.getProp("CurrentRICHSimRunOption") != "clunker" ):
                mGaussRICHConf= GaussRICHConf()
                mGaussRICHConf.InitializeGaussRICHConfiguration()
                mGaussRICHConf.setRichDetectorExistFlag(Run1Run2RichDetector)
                mGaussRICHConf.setSkipGeant4RichFlag(skipG4 )
                mGaussRICHConf.ApplyGaussRICHConfiguration(giga)

             else:
                 #keep the old options for backward compatibility for now. It may be removed in the future.
                 self.GaussRICHOldSetup(giga,Run1Run2RichDetector ,skipG4 )

         # End of RICH simulation configuration

         giga.TrackSeq.Members += [ "GaussPostTrackAction/PostTrack" ]
         giga.TrackSeq.Members += [ "GaussTrackActionHepMC/HepMCTrack" ]
         giga.TrackSeq.addTool( GaussPostTrackAction("PostTrack") , name = "PostTrack" )

         giga.TrackSeq.PostTrack.StoreAll          = False
         giga.TrackSeq.PostTrack.StorePrimaries    = True
         giga.TrackSeq.PostTrack.StoreMarkedTracks = True
         giga.TrackSeq.PostTrack.StoreForcedDecays = True
         giga.TrackSeq.PostTrack.StoreByOwnEnergy   = True
         giga.TrackSeq.PostTrack.OwnEnergyThreshold = 100.0 * SystemOfUnits.MeV

         giga.TrackSeq.PostTrack.StoreByChildProcess  = True
         giga.TrackSeq.PostTrack.StoredChildProcesses = [ "RichG4Cerenkov", "Decay" ]
         giga.TrackSeq.PostTrack.StoreByOwnProcess  = True
         giga.TrackSeq.PostTrack.StoredOwnProcesses = [ "Decay" ]
         giga.StepSeq.Members += [ "GaussStepAction/GaussStep" ]


         giga.addTool( GiGaRunManager("GiGaMgr") , name="GiGaMgr" )
         giga.GiGaMgr.RunTools += [ "GiGaSetSimAttributes" ]
         giga.GiGaMgr.RunTools += [ "GiGaRegionsTool" ]
         giga.GiGaMgr.addTool( GiGaSetSimAttributes() , name = "GiGaSetSimAttributes" )
         giga.GiGaMgr.GiGaSetSimAttributes.OutputLevel = 4

         # Second part went here

         #return giga

    def GaussRICHOldSetup(self,giga,Run1Run2RichDetector=True, skipG4=False):
        #Old setup for GaussRICH which can be removed in the future.

        from Configurables import ( GiGaPhysConstructorOp,GiGaPhysConstructorHpd )
        if self.getProp("DataType") in self.Run2DataTypes :
            # Line to remove AEROGEL warnings
            SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/SimulationRICHesOff.xml"
        else:
            SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/Simulation.xml"

        giga.ModularPL.addTool( GiGaPhysConstructorOp,name = "GiGaPhysConstructorOp" )
        giga.ModularPL.addTool( GiGaPhysConstructorHpd,name = "GiGaPhysConstructorHpd" )
        if Run1Run2RichDetector:
            importOptions("$GAUSSRICHROOT/options/Rich.opts")
            if self.getProp("DataType") in self.Run2DataTypes :
                importOptions("$GAUSSRICHROOT/options/RichRemoveAerogel.opts")

            giga.ModularPL.GiGaPhysConstructorOp.RichActivateRichPhysicsProcVerboseTag = True
            giga.StepSeq.Members += [ "RichG4StepAnalysis4/RichStepAgelExit" ]
            giga.StepSeq.Members += [ "RichG4StepAnalysis5/RichStepMirrorRefl" ]
            if self.getProp("RICHRandomHits") == True :
                giga.ModularPL.GiGaPhysConstructorOp.Rich2BackgrHitsActivate = True
                giga.ModularPL.GiGaPhysConstructorOp.Rich2BackgrHitsProbabilityFactor = 0.5
        else:
            if not skipG4:
                giga.ModularPL.GiGaPhysConstructorOp.RichOpticalPhysicsProcessActivate = False
                giga.ModularPL.GiGaPhysConstructorHpd.RichHpdPhysicsProcessActivate = False


    def GaussCherenkovOldSetup(self, giga, UpgradeRichPmtDetector=True, skipG4=False):
        #Old set up for GaussCherenkov which can removed in the future
        from Configurables import ( GiGaPhysConstructorOpCkv, GiGaPhysConstructorPhotoDetector)
        # Line to remove AEROGEL warnings
        SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/SimulationRICHesOff.xml"
        if UpgradeRichPmtDetector :
            importOptions("$GAUSSCHERENKOVROOT/options/GaussCherenkov.opts")
            giga.ModularPL.addTool( GiGaPhysConstructorOpCkv, name="GiGaPhysConstructorOpCkv" )
            giga.ModularPL.addTool(GiGaPhysConstructorPhotoDetector, name="GiGaPhysConstructorPhotoDetector")
            giga.ModularPL.GiGaPhysConstructorOpCkv.RichActivateRichPhysicsProcVerboseTag = True
            giga.StepSeq.Members += [ "RichG4StepAnalysis4/RichStepAgelExit" ]
            giga.StepSeq.Members += [ "RichG4StepAnalysis5/RichStepMirrorRefl" ]
            if skipG4:
                giga.ModularPL.GiGaPhysConstructorOpCkv.RichOpticalPhysicsProcessActivate = False
                giga.ModularPL.GiGaPhysConstructorPhotoDetector.RichPmtPhysicsProcessActivate = False




#"""
#
#     ><<                             ><<       ><<<<
#  ><<   ><<                        ><        ><    ><<        ><<
# ><<           ><<     ><< ><<   ><>< ><    ><<             > ><<
# ><<         ><<  ><<   ><<  ><<   ><<      ><<            >< ><<
# ><<        ><<    ><<  ><<  ><<   ><<      ><<   ><<<<  ><<  ><<
#  ><<   ><<  ><<  ><<   ><<  ><<   ><<       ><<    ><  ><<<< >< ><<
#    ><<<<      ><<     ><<<  ><<   ><<        ><<<<<          ><<
#
#
#"""

    ##
    ## Configure the sequence to transform HepMC into MCParticles
    ## skipping Geant4
    def configureSkipGeant4( self, SpillOverSlots ):

        """
        Set up the sequence to transform HepMC into MCParticles
        """

        if "GenToMCTree" not in self.getProp("Phases"):
            log.warning("No GenToMCTree phase.")
            return

        # Do not do detector simulation in this case
        self.getProp('DetectorSim')['Detectors'] = []
        self.getProp('DetectorGeo')['Detectors'] = []
        self.getProp('DetectorMoni')['Detectors'] = []

        ApplicationMgr().ExtSvc += [ "GiGa" ]

        gaussSkipGeant4Seq = GaudiSequencer( "SkipGeant4" )
        gaussSeq = GaudiSequencer("GaussSequencer")
        gaussSeq.Members += [ gaussSkipGeant4Seq ]

        self.configureGiGa( True )

        for slot in SpillOverSlots:

            TESNode = "/Event/"+self.slot_(slot)

            mainSkipGeant4Sequence = GaudiSequencer( self.slotName(slot)+"EventSeq" )

            gaussSkipGeant4Seq.Members += [ mainSkipGeant4Sequence ]

            mainSkipGeant4Sequence.Members +=  [ SimInit( self.slotName(slot)+"EventGaussSkipGeant4",
                                                          GenHeader = TESNode + "Gen/Header" ,
                                                          MCHeader = TESNode + "MC/Header" ) ]

            skipGeant4Seq = GaudiSequencer( self.slotName(slot)+"SkipGeant4",
                                            RequireObjects = [ TESNode + "Gen/HepMCEvents" ] )
            mainSkipGeant4Sequence.Members += [ skipGeant4Seq ]

            skipGeant4SlotSeq = GaudiSequencer( "Make"+self.slotName(slot)+"SkipGeant4" )
            skipGeant4Seq.Members += [skipGeant4SlotSeq]

            # CRJ : Set RootInTES - Everything down stream will then use the correct location
            #       (assuming they use GaudiAlg get and put) so no need to set data locations
            #       by hand any more ...
            if slot != '' : skipGeant4SlotSeq.RootInTES = slot

            genToSim = GenerationToSimulation( "GenToSim" + slot,
                                               SkipGeant = True )
            skipGeant4SlotSeq.Members += [ genToSim ]

            # Data packing ...
            if self.getProp("EnablePack") :
                packing = GaudiSequencer(self.slotName(slot)+"EventDataPacking")
                skipGeant4SlotSeq.Members += [ packing ]
                SimConf().PackingSequencers[slot] = packing


#"""
#   ><< <<                ><<      ><<<<<<<
# ><<    ><<              ><<      ><<    ><< ><<
#  ><<          ><<     ><>< ><    ><<    ><< ><<      ><<   ><<  ><<<<
#    ><<      ><   ><<    ><<      ><<<<<<<   >< ><     ><< ><<  ><<
#       ><<  ><<<<< ><<   ><<      ><<        ><<  ><<    ><<<     ><<<
# ><<    ><< ><           ><<      ><<        ><   ><<     ><<       ><<
#   ><< <<     ><<<<       ><<     ><<        ><<  ><<    ><<    ><< ><<
#                                                       ><<
#"""

    def setPhysList( self, richUpgradeConfig ):

        giga = GiGa()
        giga.addTool( GiGaPhysListModular("ModularPL") , name="ModularPL" )
        giga.PhysicsList = "GiGaPhysListModular/ModularPL"
        gmpl = giga.ModularPL

        ## set production cuts
        ecut = 5.0 * SystemOfUnits.mm
        if not self.getProp("DeltaRays"):
            ecut = 10000.0 * SystemOfUnits.m
        print 'Ecut value =', ecut
        gmpl.CutForElectron = ecut
        gmpl.CutForPositron = 5.0 * SystemOfUnits.mm
        gmpl.CutForGamma    = 5.0 * SystemOfUnits.mm

        ## set up the physics list
        hadronPhys = self.getProp('PhysicsList')['Hadron']
        emPhys     = self.getProp('PhysicsList')['Em']
        lhcbPhys   = self.getProp('PhysicsList')['LHCbPhys']
        genPhys    = self.getProp('PhysicsList')['GeneralPhys']
        otherPhys  = ''
        if self.getProp('PhysicsList').has_key('Other'):
            otherPhys = self.getProp('PhysicsList')['Other']

        def gef(name):
            import Configurables
            return getattr(Configurables, "GiGaExtPhysics_%s_" % name)
        def addConstructor(template, name):
            gmpl.addTool(gef(template), name = name)
            gmpl.PhysicsConstructors.append(getattr(gmpl, name))

        ## --- EM physics:
        if  (emPhys == "Opt1"):
            addConstructor("G4EmStandardPhysics_option1", "EmOpt1Physics")
        elif(emPhys == "Opt2"):
            addConstructor("G4EmStandardPhysics_option2", "EmOpt2Physics")
        elif(emPhys == "Opt3"):
            addConstructor("G4EmStandardPhysics_option3", "EmOpt3Physics")
        elif(emPhys == "Std"):
            addConstructor("G4EmStandardPhysics", "EmPhysics")
        elif(emPhys == "NoCuts"):
            addConstructor("G4EmStandardPhysics_option1NoApplyCuts", "EmOpt1NoCutsPhysics")
        elif(emPhys.find("LHCb") != -1):
            if(emPhys.find("Test") != -1 ):
                addConstructor("G4EmStandardPhysics_LHCbTest", "EmOpt1LHCbPhysics")
            else:
                addConstructor("G4EmStandardPhysics_option1LHCb", "EmOpt1LHCbPhysics")
            # overwrite cuts depending on choice of list
            if(emPhys.find("NoCuts") != -1 ):
                gmpl.EmOpt1LHCbPhysics.ApplyCuts = False
            if(emPhys.find("OldForE") != -1 ):
                gmpl.EmOpt1LHCbPhysics.NewModelForE = False

            #gmpl.EmOpt1LHCbPhysics.OutputLevel = VERBOSE
        else:
            raise RuntimeError("Unknown Em PhysicsList chosen ('%s')"%emPhys)

        ## --- general  physics (common to all PL):
        if (genPhys == True):
        ## Decays
            addConstructor("G4DecayPhysics", "DecayPhysics" )
        ## EM physics: Synchroton Radiation & gamma,electron-nuclear Physics
            addConstructor("G4EmExtraPhysics", "EmExtraPhysics")
        ## Hadron physics: Hadron elastic scattering
        ##    addConstructor("G4HadronElasticPhysics", "ElasticPhysics")
        ## now specialised for different constructors
        ## Ions physics
            addConstructor("G4IonPhysics", "IonPhysics")
        elif (genPhys == False):
            log.warning("The general physics (Decays, hadron elastic, ion ...) is disabled")
        else:
            raise RuntimeError("Unknown setting for GeneralPhys PhysicsList chosen ('%s')"%genPhys)

        ## --- Hadron physics:
        if(hadronPhys == "QGSP_BERT"):
            addConstructor("G4HadronElasticPhysics", "ElasticPhysics")
            addConstructor("G4HadronPhysicsQGSP_BERT", "QGSP_BERTPhysics")
            addConstructor("G4StoppingPhysics", "StoppingPhysics")
            addConstructor("G4NeutronTrackingCut", "NeutronTrkCut")
        elif(hadronPhys == "QGSP_BERT_HP"):
            addConstructor("G4HadronElasticPhysicsHP", "ElasticPhysicsHP")
            addConstructor("G4HadronPhysicsQGSP_BERT_HP", "QGSP_BERT_HPPhysics")
            addConstructor("G4StoppingPhysics", "StoppingPhysics")
            # overwrite the defaut value of the HighPrecision property of the
            # G4HadronElasticPhysics constructor: no longer true, use dedicated
            # constructor
            #gmpl.ElasticPhysics.HighPrecision = True
            #gmpl.ElasticPhysics.OutputLevel = VERBOSE
        elif(hadronPhys == "QGSP_FTFP_BERT"):
            addConstructor("G4HadronElasticPhysics", "ElasticPhysics")
            addConstructor("G4HadronPhysicsQGSP_FTFP_BERT", "QGSP_FTFP_BERTPhysics")
            addConstructor("G4StoppingPhysics", "StoppingPhysics")
            addConstructor("G4NeutronTrackingCut", "NeutronTrkCut")
        elif(hadronPhys == "FTFP_BERT"):
            addConstructor("G4HadronElasticPhysics", "ElasticPhysics")
            addConstructor("G4HadronPhysicsFTFP_BERT", "FTFP_BERTPhysics")
            addConstructor("G4StoppingPhysics", "StoppingPhysics")
            addConstructor("G4NeutronTrackingCut", "NeutronTrkCut")
        elif(hadronPhys == "FTFP_BERT_HP"):
            addConstructor("G4HadronElasticPhysicsHP", "ElasticPhysicsHP")
            addConstructor("G4HadronPhysicsFTFP_BERT_HP", "FTFP_BERT_HPPhysics")
            addConstructor("G4StoppingPhysics", "StoppingPhysics")
        else:
            raise RuntimeError("Unknown Hadron PhysicsList chosen ('%s')"%hadronPhys)


        # Add Redecay tag particle physics list if necessary
        # This is also necessary for the pure SplitSim setting as the ReDecay
        # infrastructure is used.
        if self.Redecay['active'] or self.getProp("SplitSim"):
            from Configurables import GiGaPhysG4RDTag
            gmpl.addTool(GiGaPhysG4RDTag)
            gmpl.PhysicsConstructors.append(gmpl.GiGaPhysG4RDTag)


        ## --- LHCb specific physics:
        if  (lhcbPhys == True):
            if (richUpgradeConfig == True):
                self.defineRichMaPmtPhys(gmpl)
            else:
                self.defineRichPhys(gmpl)

        ## LHCb particles unknown to default Geant4
            gmpl.PhysicsConstructors.append("GiGaPhysUnknownParticles")
        elif (lhcbPhys == False):
            log.warning("The lhcb-related physics (RICH processed, UnknownParticles) is disabled")
        else:
            raise RuntimeError("Unknown setting for LHCbPhys PhysicsList chosen ('%s')"%lhcbPhys)

        ## and other exotic physics
        if (otherPhys == 'Higgs'):
            log.info("Enabling physics processe for Higgs particles")
            gmpl.PhysicsConstructors.append("GiGaHiggsParticles")
        else:
            if (otherPhys != '' ):
               raise RuntimeError("Unknown setting for OtherPhys PhysicsList chosen ('%s')"%otherPhys)



#"""
#      ><                           ><<                 ><<                             ><<
#     >< <<                         ><<              ><<   ><<                        ><
#    ><  ><<     >< ><<   >< ><<    ><< ><<   ><<   ><<           ><<     ><< ><<   ><>< ><
#   ><<   ><<    ><  ><<  ><  ><<   ><<  ><< ><<    ><<         ><<  ><<   ><<  ><<   ><<
#  ><<<<<< ><<   ><   ><< ><   ><<  ><<    ><<<     ><<        ><<    ><<  ><<  ><<   ><<
# ><<       ><<  ><< ><<  ><< ><<   ><<     ><<      ><<   ><<  ><<  ><<   ><<  ><<   ><<
#><<         ><< ><<      ><<      ><<<    ><<         ><<<<      ><<     ><<<  ><<   ><<
#                ><<      ><<            ><<
#"""

    ##
    ##
    ## Apply the configuration
    def __apply_configuration__(self):

        GaudiKernel.ProcessJobOptions.PrintOff()

        #defineDB() in Boole and
        self.configureRndmEngine()
        self.configureInput()  #defineEvents() in both Boole and Brunel
        LHCbApp( Simulation = True ) # in Boole? where?

        # raise an error if DetectorGeo/Sim/Moni dictionaries are incompatible
        self.checkGeoSimMoniDictionary()

        self.checkIncompatibleDetectors()

        self.setLHCbAppDetectors()

        #propagate info to SimConf
        self.propagateSimConf()

        #Construct Crossing List
        crossingList = self.defineCrossingList()

        # We want to pass this GenInit object to configure phases later
        from Configurables import ( Generation )
        genInitPrime = GenInit( "GaussGen" )

        self.setBeamParameters( crossingList, genInitPrime )
        # PSZ - everything happens here
        self.configurePhases( crossingList  )  # in Boole, defineOptions() in Brunel


        #--Configuration of output files and 'default' outputs files that can/should
        #--be overwritten in Gauss-Job.py
        self.defineOutput( crossingList )

        self.defineMonitors()
        self.saveHistos()

        GaudiKernel.ProcessJobOptions.PrintOn()
        log.info( self )
        GaudiKernel.ProcessJobOptions.PrintOff()

        # Print out TES contents at the end of each event
        #from Configurables import StoreExplorerAlg
        #GaudiSequencer("GaussSequencer").Members += [ StoreExplorerAlg() ]


# _____          _
#|  __ \        | |
#| |__) |___  __| | ___  ___ __ _ _   _
#|  _  // _ \/ _` |/ _ \/ __/ _` | | | |
#| | \ \  __/ (_| |  __/ (_| (_| | |_| |
#|_|  \_\___|\__,_|\___|\___\__,_|\__, |
#                                  __/ |
#                                 |___/

    def configure_redecay_mchits(self, slot, loc):
        if 'Signal' not in slot:
            GaussRedecayCopyToService('GaussRedecayCopyToService' + slot).MCHitsLocation += [loc]
            GaussRedecayRetrieveFromService('GaussRedecayRetrieveFromService' + slot).MCHitsLocation += [loc]


    def configure_redecay_mccalohits(self, slot, loc):
        if 'Signal' not in slot:
            GaussRedecayCopyToService('GaussRedecayCopyToService' + slot).MCCaloHitsLocation += [loc]
            GaussRedecayRetrieveFromService('GaussRedecayRetrieveFromService' + slot).MCCaloHitsLocation += [loc]


    def configureRedecay(self, SpillOverSlots ):
        """Apply final configuration to the redecay configurables, especially set
        the correct GaussRedecay Service instances for the different spillover
        slots"""
        n_redecays = self.Redecay['N']
        rd_mode = self.Redecay['rd_mode']

        genInit = GenInit('SignalGen')
        genInitT0 = GenInit("GaussGen")
        if genInitT0.isPropertySet("RunNumber"):
            genInit.RunNumber = genInitT0.RunNumber
        if genInitT0.isPropertySet("FirstEventNumber"):
            genInit.FirstEventNumber = genInitT0.FirstEventNumber

        for slot in SpillOverSlots:
            svcname = 'GaussRedecay' + slot
            ApplicationMgr().ExtSvc += ['GaussRedecay/GaussRedecay' + slot]
            GaussRedecay(svcname).Phase = 1
            GaussRedecay(svcname).nRedecay = n_redecays
            GaussRedecay(svcname).RedecayMode = rd_mode

            if slot == '':
                continue
            GaussRedecayCopyToService(
                'GaussRedecayCopyToService' + slot).GaussRedecay = svcname
            GaussRedecayRetrieveFromService(
                'GaussRedecayRetrieveFromService' + slot).GaussRedecay = svcname
            GaussRedecayCtrFilter(
                'RegisterNewEvent' + slot).GaussRedecay = svcname
            GaussRedecayCtrFilter(
                'CheckIfFullOrUESim' + slot).GaussRedecay = svcname
            GaussRedecayCtrFilter(
                'CheckIfSignalSim2' + slot).GaussRedecay = svcname

        from Configurables import RedecayProduction
        from Configurables import GaussRedecayFakePileUp

        sig_gen = Generation('GenerationSignal')
        org_gen = Generation('Generation')

        sig_gen.EventType = org_gen.EventType
        sig_gen.SampleGenerationTool = "SignalPlain"
        sig_gen.addTool(SignalPlain)

        sig_gen.PileUpTool = "GaussRedecayFakePileUp"
        sig_gen.addTool(GaussRedecayFakePileUp)
        sig_gen.VertexSmearingTool = ""

        # Signal SampleGenerationTool
        sig_sgt = sig_gen.SignalPlain
        sig_sgt.ProductionTool = "RedecayProduction"
        sig_sgt.addTool(RedecayProduction)
        sig_sgt.RevertWhenBackward = False

        # Turn of GenFSR for the Signal portion (not needed as it gives
        # biased efficiencies anyway and is a pain to set up)
        sig_gen.GenFSRLocation = ""
        sig_sgt.GenFSRLocation = ""

        # Original SampleGenerationTool to get the PIDList and CutTool
        org_sgt_name = org_gen.SampleGenerationTool.split('/')[-1]
        org_sgt = getattr(org_gen, org_sgt_name)
        sig_sgt.SignalPIDList = org_sgt.SignalPIDList

        # Set the correct names for the used decay tools
        if hasattr(org_gen, 'DecayTool'):
            sig_gen.DecayTool = org_gen.DecayTool
        if hasattr(org_sgt, 'DecayTool'):
            sig_sgt.DecayTool = org_sgt.DecayTool

        # Copy the CutTool if it exists
        if hasattr(org_sgt, 'CutTool'):
            org_ctl_name = org_sgt.CutTool.split('/')[-1]
            sig_sgt.CutTool = org_sgt.CutTool
            # Check if the cuttool is configured, might not be in case of
            # simple ones like DaughtersInLHCb
            if hasattr(org_sgt, org_ctl_name):
                org_ctl = getattr(org_sgt, org_ctl_name)
                sig_sgt.addTool(org_ctl, org_ctl_name)


    def configureRedecaySim( self, SpillOverSlots ):

        """
        Set up the simulation sequence
        """

        if "Simulation" not in self.getProp("Phases"):
            log.warning("No simulation phase.")
            return

        ApplicationMgr().ExtSvc += [ "GiGa" ]
        EventPersistencySvc().CnvServices += [ "GiGaKine" ]

        gaussSimulationSeq = GaudiSequencer( "Simulation" )
        gaussSeq = GaudiSequencer("GaussSequencer")
        gaussSeq.Members += [ gaussSimulationSeq ]

        gigaStore = GiGaDataStoreAlgorithm( "GiGaStore" )
        gigaStore.ConversionServices = [ "GiGaKine" ]
        gaussSimulationSeq.Members += [ gigaStore ]

        self.defineGeo()

        self.configureGiGa()

        if self.getProp('UseGaussGeo'):
            GiGa().GeometrySource = "GaussGeo"
        else:
            GiGa().GeometrySource = "GiGaGeo"

        for slot in SpillOverSlots:

            TESNode = "/Event/"+self.slot_(slot)

            mainSimSequence = GaudiSequencer( self.slotName(slot)+"EventSeq" )

            gaussSimulationSeq.Members += [ mainSimSequence ]

            mainSimSequence.Members +=  [ SimInit( self.slotName(slot)+"EventGaussSim",
                                                   GenHeader = TESNode + "Gen/Header" ,
                                                   MCHeader = TESNode + "MC/Header" ) ]

            # We might not have a HepMC events in the redecays, hence
            # the requirement is removed and explicitly checked using an
            # algorithm running at the beginning.
            simSeq = GaudiSequencer( self.slotName(slot)+"Simulation" )

            # simSeq used to have a requireObjects. Cannot do that anymore
            # as redecay events do not have a HepMC event. Hence make a filter
            # that decides based on the original event.
            hepmcfilterseq = GaudiSequencer('HepMCFilterSeq' + self.slotName(slot), ModeOR=True)
            hepmcfilter = GaussRedecayCtrFilter('HepMCFilter' + self.slotName(slot))
            hepmcfilter.CheckFor = TESNode + "Gen/HepMCEvents"
            hepmcfilterseq.Members += [hepmcfilter]

            mainSimSequence.Members += [ hepmcfilterseq]
            hepmcfilterseq.Members += [simSeq]

            simSlotSeq = GaudiSequencer( "Make"+self.slotName(slot)+"Sim",
                                         RequireObjects = [ TESNode + "Gen/HepMCEvents" ])
            simSlotFullSeq = GaudiSequencer( "Make"+self.slotName(slot)+"FullSim",
                                            ModeOR=True)

            simSlotSeq.Members += [simSlotFullSeq]
            simSeq.Members += [simSlotSeq]

            # CRJ : Set RootInTES - Everything down stream will then use the correct location
            #       (assuming they use GaudiAlg get and put) so no need to set data locations
            #       by hand any more ...
            if slot != '' : simSlotFullSeq.RootInTES = slot

            # Following is the main sim of the event, either normal event or
            # the underlying event component for redecay, filter out if this
            # event does not need this information.
            #
            # Make a filter to turn this part off for the signal redecay part.
            # Ask whether phase is 1 and set it to 2 later on. Only applies for
            # redecay, setting to 2 ignored otherwise!
            grdfilter = GaussRedecayCtrFilter(
                'CheckIfFullOrUESim{}'.format(slot))
            grdfilter.IsPhaseEqual = 2
            simSlotFullSeq.Members += [ grdfilter]

            # Migrate the actual work into a new sequence, only activated when
            # the previous filter return false due to the OR mode
            simSlotFullSeqImpl = GaudiSequencer(
                "Make"+self.slotName(slot)+"FullSimImpl")
            simSlotFullSeq.Members += [ simSlotFullSeqImpl]

            genToSim = GenerationToSimulation( "GenToSim" + slot,
                                               LookForUnknownParticles = True )
            simSlotFullSeqImpl.Members += [ genToSim ]

            simSlotFullSeqImpl.Members += [ GiGaFlushAlgorithm( "GiGaFlush"+slot ) ]
            simSlotFullSeqImpl.Members += [ GiGaCheckEventStatus( "GiGaCheckEvent"+slot ) ]
            simToMC = SimulationToMCTruth( "SimToMCTruth"+slot )
            simSlotFullSeqImpl.Members += [ simToMC ]

            ## Detectors hits
            TESNode = TESNode + "MC/"
            detHits = GaudiSequencer( "DetectorsHits" + slot )
            simSlotFullSeqImpl.Members += [ detHits ]
            simSlotFullSeqImpl.Members += [ GaussRedecayCopyToService(
                'GaussRedecayCopyToService{}'.format(slot)
            ) ]

            # Slight trick - configuredRichSim is a list and therefore MUTABLE!
            configuredRichSim = [ False ]
            for det in self.getProp('DetectorSim')['Detectors']:
                self.configureDetectorSim( slot, detHits, det, configuredRichSim )

            # ################################################
            # Signal part here
            # ################################################
            if slot == '':
                TESNode = "/Event/"+self.slot_(slot)+"Signal/"
                simSlotSignal = GaudiSequencer( "SignalSimulation")

                gdh = GenInit('SignalGen')
                self.setBeamParameters(self.defineCrossingList(), gdh)

                gdh.MCHeader = TESNode+"Gen/Header"
                gdh.CreateBeam = False
                sdh = SimInit('SignalSim')

                sdh.MCHeader = TESNode+"MC/Header"

                simSlotSignal.Members += [ gdh]
                simSlotSignal.Members += [ sdh]

                simSlotSignalSeq = GaudiSequencer( "Make"+self.slotName(slot)+"SignalSim", ModeOR=True)
                simSlotSignalSeqImpl = GaudiSequencer( "Make"+self.slotName(slot)+"SignalSimImpl")
                simSeq.Members += [simSlotSignal]
                simSlotSignal.Members += [simSlotSignalSeq]

                grdfilter = GaussRedecayCtrFilter('CheckIfSignalSim')
                grdfilter.IsPhaseEqual = 0
                simSlotSignalSeq.Members = [grdfilter, simSlotSignalSeqImpl]

                simSlotSignalSeqImpl.Members += [Generation("GenerationSignal")]
                simSlotSignalSeqImpl.RootInTES = '{}Signal'.format(slot)

                genToSim = GenerationToSimulation( "GenToSim" + slot + 'Signal',
                                                LookForUnknownParticles = True )
                # genToSim.SelectiveSimulationStep = 2
                simSlotSignalSeqImpl.Members += [ genToSim ]

                simSlotSignalSeqImpl.Members += [ GiGaFlushAlgorithm( "GiGaFlush"+slot + 'Signal' ) ]
                simSlotSignalSeqImpl.Members += [ GiGaCheckEventStatus( "GiGaCheckEvent"+slot + 'Signal' ) ]
                simToMC = SimulationToMCTruth( "SimToMCTruth"+slot + 'Signal' )
                simSlotSignalSeqImpl.Members += [ simToMC ]

                detHits = GaudiSequencer( "DetectorsHits" + slot + 'Signal' )
                simSlotSignalSeqImpl.Members += [ detHits ]
                simSlotSignalSeqImpl.Members += [ GaussRedecayPrintMCParticles('SignalPrint') ]

                configuredRichSim = [ False ]
                for det in self.getProp('DetectorSim')['Detectors']:
                    self.configureDetectorSim( slot+'Signal', detHits, det, configuredRichSim )

            # ##############################################
            # End signal part
            # ##############################################
            loadSlotSeq = GaudiSequencer( "Load"+self.slotName(slot)+"Sim", ModeOR=True )
            grdfilter = GaussRedecayCtrFilter('CheckIfSignalSim2{}'.format(slot))
            grdfilter.IsPhaseNotEqual = 2
            loadSlotSeq.RootInTES = slot
            loadSlotSeq.Members += [
                grdfilter,
                GaussRedecayRetrieveFromService('GaussRedecayRetrieveFromService{}'.format(slot))]
            simSeq.Members += [loadSlotSeq]
            if slot == '':
                grdfilter = GaussRedecayCtrFilter('CheckIfMerge')
                grdfilter.IsPhaseEqual = 0
                mergeSlotSeq = GaudiSequencer( "Merge"+self.slotName(slot)+"Sim", ModeOR=True )
                GaussRedecayMergeAndClean().MCHitsLocation = GaussRedecayCopyToService().MCHitsLocation
                GaussRedecayMergeAndClean().MCCaloHitsLocation = GaussRedecayCopyToService().MCCaloHitsLocation
                mergeSlotSeq.Members += [
                    #StoreExplorerAlg('BeforeMerge'),
                    grdfilter,
                    GaussRedecayMergeAndClean()]
                    #GaussRedecayPrintMCParticles('FullPrint'),
                    #StoreExplorerAlg('AfterMerge')]
                simSeq.Members += [mergeSlotSeq]
                GaudiSequencer('RichHitsSignal').Members = GaudiSequencer('RichHitsSignal').Members[:4]
            richpaddingSlotSeq = GaudiSequencer( "RichPadding"+self.slotName(slot) )
            richpaddingSlotSeq.RootInTES = slot
            richpaddingSlotSeq.Members = GaudiSequencer('RichHits' + slot).Members[4:]
            GaudiSequencer('RichHits' + slot).Members = GaudiSequencer('RichHits' + slot).Members[:4]
            simSeq.Members += [richpaddingSlotSeq]


            # Data packing ...
            if self.getProp("EnablePack") :
                packing = GaudiSequencer(self.slotName(slot)+"EventDataPacking")
                simSeq.Members += [ packing ]
                SimConf().PackingSequencers[slot] = packing


    def configureSplitSim( self, SpillOverSlots ):

        """
        Set up the simulation sequence
        """

        if "Simulation" not in self.getProp("Phases"):
            log.warning("No simulation phase.")
            return

        ApplicationMgr().ExtSvc += [ "GiGa" ]
        EventPersistencySvc().CnvServices += [ "GiGaKine" ]

        gaussSimulationSeq = GaudiSequencer( "Simulation" )
        gaussSeq = GaudiSequencer("GaussSequencer")
        gaussSeq.Members += [ gaussSimulationSeq ]

        gigaStore = GiGaDataStoreAlgorithm( "GiGaStore" )
        gigaStore.ConversionServices = [ "GiGaKine" ]
        gaussSimulationSeq.Members += [ gigaStore ]

        self.defineGeo()

        self.configureGiGa()

        if self.getProp('UseGaussGeo'):
            GiGa().GeometrySource = "GaussGeo"
        else:
            GiGa().GeometrySource = "GiGaGeo"

        for slot in SpillOverSlots:

            if slot == '':
                ApplicationMgr().ExtSvc += ['GaussRedecay/GaussRedecay']
                GaussRedecay().Phase = 1
                GaussRedecay().nRedecay = 1

            TESNode = "/Event/"+self.slot_(slot)

            mainSimSequence = GaudiSequencer( self.slotName(slot)+"EventSeq" )

            gaussSimulationSeq.Members += [ mainSimSequence ]

            mainSimSequence.Members +=  [ SimInit( self.slotName(slot)+"EventGaussSim",
                                                   GenHeader = TESNode + "Gen/Header" ,
                                                   MCHeader = TESNode + "MC/Header" ) ]

            simSeq = GaudiSequencer( self.slotName(slot)+"Simulation",
                                     RequireObjects = [ TESNode + "Gen/HepMCEvents" ] )
            mainSimSequence.Members += [ simSeq ]

            simSlotSeq = GaudiSequencer( "Make"+self.slotName(slot)+"Sim" )
            simSeq.Members += [simSlotSeq]

            # CRJ : Set RootInTES - Everything down stream will then use the correct location
            #       (assuming they use GaudiAlg get and put) so no need to set data locations
            #       by hand any more ...
            if slot != '' : simSlotSeq.RootInTES = slot

            if slot == '':
                gaussrdfilter = GaussRedecayCtrFilter('RegisterNewEvent')
                gaussrdfilter.RegisterNewEvent = True
                from Configurables import RedecayProduction
                splitter = GaussHepMCSplitter()
                splitter.ProductionTool = "RedecayProduction"
                splitter.addTool(RedecayProduction)
                simSlotSeq.Members += [ gaussrdfilter, splitter ]

                # ################################################
                # Split part here
                # ################################################
                if slot == '':
                    TESNode = "/Event/Split/"
                    simSlotSplit = GaudiSequencer( "SplitSimulation")
                    simSlotSeq.Members += [ simSlotSplit ]

                    gdh = GenInit('SignalGen')
                    self.setBeamParameters(self.defineCrossingList(), gdh)

                    gdh.MCHeader = TESNode+"Gen/Header"
                    gdh.CreateBeam = False
                    # Dummy SimInit to make GenToSim happy
                    # This will reset seeds but no simulation has yet been
                    # done so everyone is happy
                    sdh = SimInit('SplitSim')
                    sdh.GenHeader = "/Event/Gen/Header"  # Use the main gen header to get the event and run number
                    sdh.MCHeader = TESNode+"MC/Header"

                    simSlotSplit.Members += [ sdh]

                    simSlotSplitSeq = GaudiSequencer( "Make"+self.slotName(slot)+"SplitSim")
                    simSlotSplit.Members += [simSlotSplitSeq]

                    simSlotSplitSeq.RootInTES = '{}Split'.format(slot)

                    genToSim = GenerationToSimulation( "GenToSim" + slot + 'Split',
                                                    LookForUnknownParticles = True )
                    # genToSim.SelectiveSimulationStep = 2
                    simSlotSplitSeq.Members += [ genToSim ]

                    simSlotSplitSeq.Members += [ GiGaFlushAlgorithm( "GiGaFlush"+slot + 'Split' ) ]
                    simSlotSplitSeq.Members += [ GiGaCheckEventStatus( "GiGaCheckEvent"+slot + 'Split' ) ]
                    simToMC = SimulationToMCTruth( "SimToMCTruth"+slot + 'Split' )
                    simSlotSplitSeq.Members += [ simToMC ]

                    detHits = GaudiSequencer( "DetectorsHits" + slot + 'Split' )
                    simSlotSplitSeq.Members += [ detHits ]
                    simSlotSplitSeq.Members += [ GaussRedecayPrintMCParticles('SplitPrint') ]

                    configuredRichSim = [ False ]
                    for det in self.getProp('DetectorSim')['Detectors']:
                        self.configureDetectorSim( slot+'Split', detHits, det, configuredRichSim )

                    if self.getProp("PostSimFilters") :
                        filterSeq = GaudiSequencer("PostSimFilterSeq")
                        filterSeq.RootInTES = '{}Split'.format(slot)
                        simSlotSplitSeq.Members += [ filterSeq ]
                # ################################################
                # Split part end
                # ################################################

            genToSim = GenerationToSimulation( "GenToSim" + slot,
                                               LookForUnknownParticles = True )
            simSlotSeq.Members += [ genToSim ]

            simSlotSeq.Members += [ GiGaFlushAlgorithm( "GiGaFlush"+slot ) ]
            simSlotSeq.Members += [ GiGaCheckEventStatus( "GiGaCheckEvent"+slot ) ]
            simToMC = SimulationToMCTruth( "SimToMCTruth"+slot )
            simSlotSeq.Members += [ simToMC ]

            ## Detectors hits
            TESNode = TESNode + "MC/"
            detHits = GaudiSequencer( "DetectorsHits" + slot )
            simSlotSeq.Members += [ detHits ]

            # Slight trick - configuredRichSim is a list and therefore MUTABLE!
            configuredRichSim = [ False ]
            for det in self.getProp('DetectorSim')['Detectors']:
                self.configureDetectorSim( slot, detHits, det, configuredRichSim )

            # Merge the split
            if slot == '':
                mergeSlotSeq = GaudiSequencer( "Merge"+self.slotName(slot)+"Sim")
                GaussRedecayMergeAndClean().SignalTESROOT = 'Split/'
                GaussRedecayMergeAndClean().MCHitsLocation = GaussRedecayCopyToService().MCHitsLocation
                GaussRedecayMergeAndClean().MCCaloHitsLocation = GaussRedecayCopyToService().MCCaloHitsLocation
                mergeSlotSeq.Members += [
                    GaussRedecayMergeAndClean()]
                simSlotSeq.Members += [mergeSlotSeq]
                GaudiSequencer('RichHitsSplit').Members = GaudiSequencer('RichHitsSplit').Members[:4]
                richpaddingSlotSeq = GaudiSequencer( "RichPadding"+self.slotName(slot) )
                richpaddingSlotSeq.RootInTES = slot
                richpaddingSlotSeq.Members = GaudiSequencer('RichHits' + slot).Members[4:]
                GaudiSequencer('RichHits' + slot).Members = GaudiSequencer('RichHits' + slot).Members[:4]
                mergeSlotSeq.Members += [richpaddingSlotSeq]

            # Data packing ...
            if self.getProp("EnablePack") :
                packing = GaudiSequencer(self.slotName(slot)+"EventDataPacking")
                simSlotSeq.Members += [ packing ]
                SimConf().PackingSequencers[slot] = packing
        # End of Sim Configuration


    def configureRedecaySplitSim( self, SpillOverSlots ):

        """
        Set up the simulation sequence for the combination of redecay with a
        split simulation phase for the signal portion
        AKA the Franken-Config

        The plan:
            1. Have the normal GaussRedecay setup in place with its services
            2. Hook in another Redecay service for the split simulation
            3. Hope for the best
        """

        if "Simulation" not in self.getProp("Phases"):
            log.warning("No simulation phase.")
            return

        ApplicationMgr().ExtSvc += [ "GiGa" ]
        EventPersistencySvc().CnvServices += [ "GiGaKine" ]

        gaussSimulationSeq = GaudiSequencer( "Simulation" )
        gaussSeq = GaudiSequencer("GaussSequencer")
        gaussSeq.Members += [ gaussSimulationSeq ]

        gigaStore = GiGaDataStoreAlgorithm( "GiGaStore" )
        gigaStore.ConversionServices = [ "GiGaKine" ]
        gaussSimulationSeq.Members += [ gigaStore ]

        self.defineGeo()

        self.configureGiGa()

        if self.getProp('UseGaussGeo'):
            GiGa().GeometrySource = "GaussGeo"
        else:
            GiGa().GeometrySource = "GiGaGeo"

        # Configure the new service
        svcname = 'GaussSplitRedecay'
        ApplicationMgr().ExtSvc += ['GaussRedecay/'+svcname]
        GaussRedecay(svcname).Phase = 1
        GaussRedecay(svcname).nRedecay = 1
        GaussRedecay(svcname).Offset = 50  # offset pdg placeholder by 50 to not collide with the actual redecay service

        for slot in SpillOverSlots:

            TESNode = "/Event/"+self.slot_(slot)

            mainSimSequence = GaudiSequencer( self.slotName(slot)+"EventSeq" )

            gaussSimulationSeq.Members += [ mainSimSequence ]

            mainSimSequence.Members +=  [ SimInit( self.slotName(slot)+"EventGaussSim",
                                                   GenHeader = TESNode + "Gen/Header" ,
                                                   MCHeader = TESNode + "MC/Header" ) ]

            # We might not have a HepMC events in the redecays, hence
            # the requirement is removed and explicitly checked using an
            # algorithm running at the beginning.
            simSeq = GaudiSequencer( self.slotName(slot)+"Simulation" )

            # simSeq used to have a requireObjects. Cannot do that anymore
            # as redecay events do not have a HepMC event. Hence make a filter
            # that decides based on the original event.
            hepmcfilterseq = GaudiSequencer('HepMCFilterSeq' + self.slotName(slot), ModeOR=True)
            hepmcfilter = GaussRedecayCtrFilter('HepMCFilter' + self.slotName(slot))
            hepmcfilter.CheckFor = TESNode + "Gen/HepMCEvents"
            hepmcfilterseq.Members += [hepmcfilter]

            mainSimSequence.Members += [ hepmcfilterseq]
            hepmcfilterseq.Members += [simSeq]

            simSlotSeq = GaudiSequencer( "Make"+self.slotName(slot)+"Sim",
                                         RequireObjects = [ TESNode + "Gen/HepMCEvents" ])
            simSlotFullSeq = GaudiSequencer( "Make"+self.slotName(slot)+"FullSim",
                                            ModeOR=True)

            simSlotSeq.Members += [simSlotFullSeq]
            simSeq.Members += [simSlotSeq]

            # CRJ : Set RootInTES - Everything down stream will then use the correct location
            #       (assuming they use GaudiAlg get and put) so no need to set data locations
            #       by hand any more ...
            if slot != '' : simSlotFullSeq.RootInTES = slot

            # Following is the main sim of the event, either normal event or
            # the underlying event component for redecay, filter out if this
            # event does not need this information.
            #
            # Make a filter to turn this part off for the signal redecay part.
            # Ask whether phase is 1 and set it to 2 later on. Only applies for
            # redecay, setting to 2 ignored otherwise!
            grdfilter = GaussRedecayCtrFilter(
                'CheckIfFullOrUESim{}'.format(slot))
            grdfilter.IsPhaseEqual = 2
            simSlotFullSeq.Members += [ grdfilter]

            # Migrate the actual work into a new sequence, only activated when
            # the previous filter return false due to the OR mode
            simSlotFullSeqImpl = GaudiSequencer(
                "Make"+self.slotName(slot)+"FullSimImpl")
            simSlotFullSeq.Members += [ simSlotFullSeqImpl]

            genToSim = GenerationToSimulation( "GenToSim" + slot,
                                               LookForUnknownParticles = True )
            simSlotFullSeqImpl.Members += [ genToSim ]

            simSlotFullSeqImpl.Members += [ GiGaFlushAlgorithm( "GiGaFlush"+slot ) ]
            simSlotFullSeqImpl.Members += [ GiGaCheckEventStatus( "GiGaCheckEvent"+slot ) ]
            simToMC = SimulationToMCTruth( "SimToMCTruth"+slot )
            simSlotFullSeqImpl.Members += [ simToMC ]

            ## Detectors hits
            TESNode = TESNode + "MC/"
            detHits = GaudiSequencer( "DetectorsHits" + slot )
            simSlotFullSeqImpl.Members += [ detHits ]
            simSlotFullSeqImpl.Members += [ GaussRedecayCopyToService(
                'GaussRedecayCopyToService{}'.format(slot)
            ) ]

            # Slight trick - configuredRichSim is a list and therefore MUTABLE!
            configuredRichSim = [ False ]
            for det in self.getProp('DetectorSim')['Detectors']:
                self.configureDetectorSim( slot, detHits, det, configuredRichSim )

            # ################################################
            # Signal part here
            # ################################################
            if slot == '':
                TESNode = "/Event/"+self.slot_(slot)+"Signal/"
                simSlotSignal = GaudiSequencer( "SignalSimulation")

                gdh = GenInit('SignalGen')
                self.setBeamParameters(self.defineCrossingList(), gdh)

                gdh.MCHeader = TESNode+"Gen/Header"
                gdh.CreateBeam = False
                sdh = SimInit('SignalSim')

                sdh.MCHeader = TESNode+"MC/Header"

                simSlotSignal.Members += [ gdh]
                simSlotSignal.Members += [ sdh]

                simSlotSignalSeq = GaudiSequencer( "Make"+self.slotName(slot)+"SignalSim", ModeOR=True)
                simSlotSignalSeqImpl = GaudiSequencer( "Make"+self.slotName(slot)+"SignalSimImpl")
                simSeq.Members += [simSlotSignal]
                simSlotSignal.Members += [simSlotSignalSeq]

                grdfilter = GaussRedecayCtrFilter('CheckIfSignalSim')
                grdfilter.IsPhaseEqual = 0
                simSlotSignalSeq.Members = [grdfilter, simSlotSignalSeqImpl]

                simSlotSignalSeqImpl.Members += [Generation("GenerationSignal")]
                simSlotSignalSeqImpl.RootInTES = '{}Signal'.format(slot)

                splitter = GaussHepMCSplitter()
                splitter.ProductionTool = "RedecayProduction"
                splitter.GaussRedecay = svcname

                from Configurables import RedecayProduction
                splitter.addTool(RedecayProduction)
                splitter.RedecayProduction.GaussRedecay = svcname
                gaussrdfilter = GaussRedecayCtrFilter('RegisterNewSplitEvent')
                gaussrdfilter.RegisterNewEvent = True
                gaussrdfilter.GaussRedecay = svcname
                simSlotSignalSeqImpl.Members += [gaussrdfilter, splitter]

                # ################################################
                # Split part here
                # ################################################
                TESNode = "/Event/Signal/Split/"
                simSlotSplit = GaudiSequencer( "SplitSimulation")
                simSlotSignalSeqImpl.Members += [ simSlotSplit ]

                gdh = GenInit('SplitGen')
                self.setBeamParameters(self.defineCrossingList(), gdh)

                gdh.MCHeader = TESNode+"Gen/Header"
                gdh.CreateBeam = False
                # Dummy SimInit to make GenToSim happy
                # This will reset seeds but no simulation has yet been
                # done so everyone is happy
                sdh = SimInit('SplitSim')
                sdh.GenHeader = "/Event/Gen/Header"  # Use the main gen header to get the event and run number
                sdh.MCHeader = "Split/MC/Header"

                simSlotSplit.Members += [ sdh]

                simSlotSplitSeq = GaudiSequencer( "Make"+self.slotName(slot)+"SplitSim")
                simSlotSplit.Members += [simSlotSplitSeq]

                simSlotSplitSeq.RootInTES = 'Signal/Split'.format(slot)

                genToSim = GenerationToSimulation( "GenToSim" + slot + 'Split',
                                                LookForUnknownParticles = True )
                # genToSim.SelectiveSimulationStep = 2
                simSlotSplitSeq.Members += [ genToSim ]

                simSlotSplitSeq.Members += [ GiGaFlushAlgorithm( "GiGaFlush"+slot + 'Split' ) ]
                simSlotSplitSeq.Members += [ GiGaCheckEventStatus( "GiGaCheckEvent"+slot + 'Split' ) ]
                simToMC = SimulationToMCTruth( "SimToMCTruth"+slot + 'Split' )
                simSlotSplitSeq.Members += [ simToMC ]

                detHits = GaudiSequencer( "DetectorsHits" + slot + 'Split' )
                simSlotSplitSeq.Members += [ detHits ]
                simSlotSplitSeq.Members += [ GaussRedecayPrintMCParticles('SplitPrint') ]

                configuredRichSim = [ False ]
                for det in self.getProp('DetectorSim')['Detectors']:
                    self.configureDetectorSim( slot+'Split', detHits, det, configuredRichSim )

                if self.getProp("PostSimFilters") :
                    filterSeq = GaudiSequencer("PostSimFilterSeq")
                    simSlotSplitSeq.Members += [ filterSeq ]
                # ################################################
                # Split part end
                # ################################################

                genToSim = GenerationToSimulation( "GenToSim" + slot + 'Signal',
                                                LookForUnknownParticles = True )
                simSlotSignalSeqImpl.Members += [ genToSim ]

                simSlotSignalSeqImpl.Members += [ GiGaFlushAlgorithm( "GiGaFlush"+slot + 'Signal' ) ]
                simSlotSignalSeqImpl.Members += [ GiGaCheckEventStatus( "GiGaCheckEvent"+slot + 'Signal' ) ]
                simToMC = SimulationToMCTruth( "SimToMCTruth"+slot + 'Signal' )
                simSlotSignalSeqImpl.Members += [ simToMC ]

                detHits = GaudiSequencer( "DetectorsHits" + slot + 'Signal' )
                simSlotSignalSeqImpl.Members += [ detHits ]
                simSlotSignalSeqImpl.Members += [ GaussRedecayPrintMCParticles('SignalPrint') ]

                configuredRichSim = [ False ]
                for det in self.getProp('DetectorSim')['Detectors']:
                    self.configureDetectorSim( slot+'Signal', detHits, det, configuredRichSim )

                # Now merge the Split part back into the signal bit
                mergeSlotSeq = GaudiSequencer( "Merge"+self.slotName(slot)+"SignalSplitSim")
                GaussRedecayMergeAndClean('GaussRedecayMergeAndCleanSignal').SignalTESROOT = 'Split/'
                GaussRedecayMergeAndClean('GaussRedecayMergeAndCleanSignal').GaussRedecay = svcname
                mergeSlotSeq.Members += [
                    GaussRedecayMergeAndClean('GaussRedecayMergeAndCleanSignal')]
                simSlotSignalSeqImpl.Members += [mergeSlotSeq]

            # ##############################################
            # End signal part
            # ##############################################
            loadSlotSeq = GaudiSequencer( "Load"+self.slotName(slot)+"Sim", ModeOR=True )
            grdfilter = GaussRedecayCtrFilter('CheckIfSignalSim2{}'.format(slot))
            grdfilter.IsPhaseNotEqual = 2
            loadSlotSeq.RootInTES = slot
            loadSlotSeq.Members += [
                grdfilter,
                GaussRedecayRetrieveFromService('GaussRedecayRetrieveFromService{}'.format(slot))]
            simSeq.Members += [loadSlotSeq]
            if slot == '':
                grdfilter = GaussRedecayCtrFilter('CheckIfMerge')
                grdfilter.IsPhaseEqual = 0
                mergeSlotSeq = GaudiSequencer( "Merge"+self.slotName(slot)+"Sim", ModeOR=True )
                GaussRedecayMergeAndClean().MCHitsLocation = GaussRedecayCopyToService().MCHitsLocation
                GaussRedecayMergeAndClean().MCCaloHitsLocation = GaussRedecayCopyToService().MCCaloHitsLocation
                GaussRedecayMergeAndClean('GaussRedecayMergeAndCleanSignal').MCHitsLocation = GaussRedecayCopyToService().MCHitsLocation
                GaussRedecayMergeAndClean('GaussRedecayMergeAndCleanSignal').MCCaloHitsLocation = GaussRedecayCopyToService().MCCaloHitsLocation
                mergeSlotSeq.Members += [
                    #StoreExplorerAlg('BeforeMerge'),
                    grdfilter,
                    GaussRedecayMergeAndClean()]
                    #GaussRedecayPrintMCParticles('FullPrint'),
                    #StoreExplorerAlg('AfterMerge')]
                simSeq.Members += [mergeSlotSeq]
                GaudiSequencer('RichHitsSignal').Members = GaudiSequencer('RichHitsSignal').Members[:4]
            richpaddingSlotSeq = GaudiSequencer( "RichPadding"+self.slotName(slot) )
            richpaddingSlotSeq.RootInTES = slot
            richpaddingSlotSeq.Members = GaudiSequencer('RichHits' + slot).Members[4:]
            GaudiSequencer('RichHits' + slot).Members = GaudiSequencer('RichHits' + slot).Members[:4]
            simSeq.Members += [richpaddingSlotSeq]


            # Data packing ...
            if self.getProp("EnablePack") :
                packing = GaudiSequencer(self.slotName(slot)+"EventDataPacking")
                simSeq.Members += [ packing ]
                SimConf().PackingSequencers[slot] = packing
