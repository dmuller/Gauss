// Include files

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/MsgStream.h"

// from GaussRedecay
#include "GaussRedecay/IGaussRedecayCtr.h"
#include "GaussRedecay/IGaussRedecayStr.h"

// From Kernel. To access signal particle properties
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// from Event
#include "Event/GenCollision.h"
#include "GenEvent/HepMCUtils.h"
#include "HepMC/GenRanges.h"

// local
#include "GaussHepMCSplitter.h"
#include "Generators/IProductionTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GaussHepMCSplitter
//
//
// 2016-03-15 : Dominik Muller
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(GaussHepMCSplitter)

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
GaussHepMCSplitter::GaussHepMCSplitter(const std::string& Name,
                                       ISvcLocator* SvcLoc)
    : GaudiAlgorithm(Name, SvcLoc) {
  declareProperty("GaussRedecay", m_gaussRDSvcName = "GaussRedecay");
  declareProperty("HepMCEventLocation",
                  m_generationLocation = LHCb::HepMCEventLocation::Default,
                  "Location to read the HepMC event.");
  declareProperty("HepMCEventOutputLocation",
                  m_outputGenerationLocation = "Split/" + LHCb::HepMCEventLocation::Default,
                  "Location to write the fake HepMC event.");
  declareProperty("CollisionOutputLocation",
                  m_outputCollisionLocation = "Split/" + LHCb::GenCollisionLocation::Default,
                  "Location to write the GenCollisions for the fake HepMC event.");
  declareProperty("ProductionTool",
                  m_productionToolName = "RedecayProduction",
                  "Name of the production tool that construct the fake HepMC event.");
  declareProperty( "Mother", m_mother = "" );
  declareProperty( "FromSignal", m_from_signal = true, "Only use photons from signal decay." );
  declareProperty( "MaxSearchDepth", m_maxSearchDepth = -1, "Max mother search depth, negative: infinite depth");
  declareProperty( "MatchSearchDepth", m_matchSearchDepth = false, "Match mother search depth instead");
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode GaussHepMCSplitter::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if (sc.isFailure()) {
    return sc;
  }

  m_gaussRDCtrSvc = svc<IGaussRedecayCtr>(m_gaussRDSvcName, true);
  m_gaussRDStrSvc = svc<IGaussRedecayStr>(m_gaussRDSvcName, true);
  m_ppSvc = svc<LHCb::IParticlePropertySvc>("LHCb::ParticlePropertySvc", true);

  const LHCb::ParticleProperty* prop = m_ppSvc->find( m_mother );
  if( !prop ) {
    error() << "Could not retrieve ParticleProperty" << endmsg;
    m_motherID = -1;
  } else {
    // -- Get the ID of the mother (particle and antiparticle)
    m_motherID = prop->pdgID().abspid();
  }

  if ( "" != m_productionToolName ) 
    m_productionTool = tool< IProductionTool >( m_productionToolName , this ) ;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode GaussHepMCSplitter::execute() {
  m_theSignal = nullptr;
  if (nullptr == m_gaussRDCtrSvc) {
    return Error(" execute(): IGaussRedecayCtr* points to NULL");
  }

  if (nullptr == m_gaussRDStrSvc) {
    return Error(" execute(): IGaussRedecayStr* points to NULL");
  }

  if (nullptr == m_ppSvc) {
    return Error(" execute(): IParticlePropertySvc* points to NULL");
  }
  LHCb::HepMCEvents* generationEvents =
      get<LHCb::HepMCEvents>(m_generationLocation);
  if (!generationEvents) {
    return Error(
        " execute(): Could load HepMC event. That is ... eeehhh ... bad "
        "...");
  }

  /*Get the signal, going to need this in any case*/
  m_theSignal = find_signal(generationEvents);
  /*Redecay only the signal particle*/
  if (msgLevel(MSG::DEBUG)) {
    debug() << "Registering photons from the signal." << endmsg;
    auto tmp = m_theSignal->particles_in(HepMC::parents).begin();
    if (tmp != m_theSignal->particles_in(HepMC::parents).end()) {
      auto tmp2 = (*tmp)->particles_in(HepMC::parents).begin();
      if (tmp2 != (*tmp)->particles_in(HepMC::parents).end()) {
        printChildren(*tmp2);
      } else {
        printChildren(*tmp);
      }
    } else {
      printChildren(m_theSignal);
    }
  } // DEBUG

  if(m_from_signal){
    // If from signal is true, only photons from the signal are considered.
    // Optionally provided mother name is ignored!
    for(auto desc : m_theSignal->particles_out(HepMC::descendants)){
      if(abs(desc->pdg_id()) == 22 && !desc->end_vertex()){
        store_particle(desc);
      }
    }
  } else{
    // Loop over all particles and find all photons
    m_current_pileup = 0; // Assign individual pileup IDs to force all tag particles directly to MCParticles
    for(auto hepevt : *generationEvents){
      for(auto part : hepevt->pGenEvt()->particle_range()){
        if(!(abs(part->pdg_id()) == 22 && !part->end_vertex())){
          continue;
        }
        // If mother id was set, loop over all ancestors and check
        if(m_motherID>-1){
          bool found_mum = false;
          if(m_maxSearchDepth <= 0){
            // Search all ancestors of the signal
          for(auto ancestor : part->particles_in(HepMC::ancestors)){
            if(abs(ancestor->pdg_id()) == m_motherID){
              found_mum = true;
              break;
            }
          }} else {
            // only search a maximum of n_observations
            // Have to explicitly declare type of searcher to allow recursive calls
            std::function<bool(HepMC::GenParticle*, int)> searcher;
            searcher = [&] (HepMC::GenParticle* particle, int depth) -> bool {
              if((abs(particle->pdg_id()) == m_motherID) && (!m_matchSearchDepth ||(depth == 0))){
                return true;
              }
              if (depth == 0){
                return false;
              }
              bool ret = false;
              for(auto parent: particle->particles_in(HepMC::parents)){
                ret |= searcher(parent, depth - 1);
              }
              return ret;
            };
            found_mum = searcher(part, m_maxSearchDepth);
          }
          if(!found_mum){
            continue;
          }
        }
        // Register the particle for storage. Internally uses m_current_pileup.
        store_particle(part);
        // Increment the pileup counter here to have all particles end up
        // in their own fake hepmc events.
        m_current_pileup++;
      }
    }
  }
    if (msgLevel(MSG::DEBUG)) {
      debug() << "Stored all particles" << endmsg;
    }
  if (m_store_fail) {
    return Error(
        "Could not store all particles. Increase number of reserved tag "
        "particles.");
  }

  /*Now immediately use the production tool interface to build the fake events to be placed in the output location. A bit of an overkill ...*/
  LHCb::HepMCEvents* theEvents = new LHCb::HepMCEvents( );
  LHCb::GenCollisions* theCollisions = new LHCb::GenCollisions( );
  for(int iPileUp=0; iPileUp < m_gaussRDStrSvc->getNPileUp(); iPileUp++){
    LHCb::GenCollision* theCollision = nullptr;
    HepMC::GenEvent* theEvent = nullptr;
    prepareInteraction(theEvents, theCollisions, theEvent, theCollision);
    m_productionTool->generateEvent(theEvent, theCollision);

    // Usually this is meant to be decayed etc so we are modifying the status of all stable particles
    for (auto part : theEvent->particle_range()){
        if(!part->end_vertex()){
            part->set_status(LHCb::HepMCEvent::SignalInLabFrame);
        }
    }

    if (msgLevel(MSG::DEBUG)) {
      debug() << "Created dummy event, printed from beam particles" << endmsg;
      printChildren(theEvent->beam_particles().first);
    }
  }
  
  put(theEvents, m_outputGenerationLocation);
  put(theCollisions, m_outputCollisionLocation);

  return StatusCode::SUCCESS;
}

HepMC::GenParticle* GaussHepMCSplitter::find_signal(LHCb::HepMCEvents* evts) {
  HepMC::GenParticle* match = nullptr;
  /*Identify the signal particle as the SignalInLabFrame flagged particle
   * going
   * into the signal vertex.*/
  for (auto& ev : *evts) {
    auto sv = ev->pGenEvt()->signal_process_vertex();
    if (sv) {
      for (auto part : sv->particles(HepMC::parents)) {
        if (part->status() == LHCb::HepMCEvent::SignalInLabFrame) {
          match = part;
        }
      }
    }
  }

  return match;
}

void GaussHepMCSplitter::store_particle(HepMC::GenParticle* part) {
  IGaussRedecayStr::Particle temp_str_part;
  temp_str_part.momentum = Gaudi::LorentzVector(part->momentum());
  temp_str_part.point = Gaudi::XYZTPoint(part->production_vertex()->position());
  temp_str_part.pdg_id = part->pdg_id();

  /*Now store it, delete the daugthers and replace the pdg id with the
   * placeholder.*/
  auto new_id = m_gaussRDStrSvc->registerForRedecay(
      temp_str_part, m_current_pileup);
  if (new_id == -1) {
    m_store_fail = true;
  }
  if (msgLevel(MSG::DEBUG)) {
    auto mom = Gaudi::LorentzVector(part->momentum());
    debug() << "Stored particle for PDG ID " << part->pdg_id()
            << " with placeholder ID" << new_id<< "  #" << part->barcode() << endmsg;
    debug() << "#### Momentum (PT, Eta, Phi, E) = (" << mom.pt() << ", "
            << mom.eta() << ", " << mom.phi() << ", " << mom.E() << ")"
            << endmsg;
  }
  HepMCUtils::RemoveDaughters(part);
  part->set_pdg_id(new_id);
  part->set_status(LHCb::HepMCEvent::DecayedByDecayGenAndProducedByProdGen);
}

void GaussHepMCSplitter::printChildren(HepMC::GenParticle* part, int level) {
  std::map<int, std::string> id_to_name;
  id_to_name[0] = "Unknown";
  id_to_name[1] = "StableInProdGen";
  id_to_name[2] = "DecayedByProdGen";
  id_to_name[3] = "DocumentationParticle";
  id_to_name[777] = "DecayedByDecayGen";
  id_to_name[888] = "DecayedByDecayGenAndProducedByProdGen";
  id_to_name[889] = "SignalInLabFrame";
  id_to_name[998] = "SignalAtRest";
  id_to_name[999] = "StableInDecayGen";
  id_to_name[1042] = "Redecay";
  id_to_name[1043] = "ChildOfRedecay";
  std::string space = "";
  for (int i = 0; i < level; i++) {
    space += "|---> ";
  }
  std::string idname = "";
  if (id_to_name.find(part->status()) != id_to_name.end()) {
    idname = id_to_name[part->status()];
  }
  debug() << space << part->pdg_id() << " -> #" << part->barcode() << ", "
          << part->status() << ": " << idname << endmsg;
  if (part->end_vertex()) {
    for (auto p : part->particles_out(HepMC::children)) {
      printChildren(p, level + 1);
    }
  }
}

void GaussHepMCSplitter::prepareInteraction( LHCb::HepMCEvents * theEvents ,
    LHCb::GenCollisions * theCollisions , HepMC::GenEvent * & theGenEvent ,  
    LHCb::GenCollision * & theGenCollision ) const {
  LHCb::HepMCEvent * theHepMCEvent = new LHCb::HepMCEvent( ) ;
  theHepMCEvent -> setGeneratorName( "SplitHepMC" ) ;
  theGenEvent = theHepMCEvent -> pGenEvt() ;

  theGenCollision = new LHCb::GenCollision() ;  
  theGenCollision -> setEvent( theHepMCEvent ) ;
  theGenCollision -> setIsSignal( false ) ;

  theEvents -> insert( theHepMCEvent ) ;
  theCollisions -> insert( theGenCollision ) ;
}
