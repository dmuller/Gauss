#ifndef         GIGA_GIGASurfaceCnv_H
#define         GIGA_GIGASurfaceCnv_H  1 

/// from STL
#include <string>
#include <set>
/// base class from GiGa
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"
///
class Surface; 
class G4LogicalSurface; 

/** @class GiGaSurfaceCnv GiGaSurfaceCnv.h GiGa/GiGaSurfaceCnv.h
 *
 *  Converter of Surface class to Geant4
 *
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 */

class GiGaSurfaceCnv: public GiGaCnvBase
{
public:
  typedef std::set<std::string> Surfaces;

  /// Standard Constructor
  GiGaSurfaceCnv( ISvcLocator* );
  /// Standard (virtual) destructor
  virtual ~GiGaSurfaceCnv();
  ///
public:

  /// Create representation
  StatusCode createRep
  ( DataObject*      Object  ,
    IOpaqueAddress*& Address ) override;

  /// Update representation
  StatusCode updateRep
  ( IOpaqueAddress*  Address, DataObject* Object) override;

  /// Class ID for created object == class ID for this specific converter
  static const CLID&          classID();
  /// storage Type
  static unsigned char storageType() ;
  ///
protected:
  /// miscellaneous functions which performs the conversion itself
  StatusCode createSkinSurface   ( const Surface* , G4LogicalSurface *& );
  ///
  StatusCode createBorderSurface ( const Surface* , G4LogicalSurface *& );
  ///

private:
  ///
  Surfaces      m_surfaces ;
  GiGaLeaf      m_leaf     ;
  ///
};

// ============================================================================
// The End
// ============================================================================
#endif   //     GIGA_GIGASurfaceCnv_H
// ============================================================================















