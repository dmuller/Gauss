################################################################################
# Package: Generators
################################################################################
gaudi_subdir(Generators v16r0)

gaudi_depends_on_subdirs(Event/GenEvent
                         GaudiAlg
                         Gen/EvtGen
                         Gen/EvtGenExtras
                         Kernel/MCInterfaces
                         Kernel/LHCbKernel
                         Kernel/PartProp)

find_package(Boost COMPONENTS filesystem system)
find_package(ROOT)
find_package(HepMC)
find_package(Photos++)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${PHOTOS++_INCLUDE_DIRS})

string(REPLACE "-pedantic" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Wall" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Wextra" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Werror=return-type" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")

gaudi_add_library(GeneratorsLib
                  src/Lib/*.cpp src/Lib/*.F
                  PUBLIC_HEADERS Generators
                  INCLUDE_DIRS Kernel/MCInterfaces
                  LINK_LIBRARIES GenEvent GaudiAlgLib EvtGen EvtGenExtras PartPropLib)

gaudi_add_module(Generators
                 src/component/*.cpp
                 INCLUDE_DIRS Boost Kernel/MCInterfaces
                 LINK_LIBRARIES Boost GenEvent GaudiAlgLib LHCbKernel EvtGen EvtGenExtras GeneratorsLib)

