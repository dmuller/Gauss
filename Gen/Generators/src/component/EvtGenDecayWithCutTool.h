#pragma once
// from Generators
#include "EvtGenDecay.h"

class IGenCutTool;

/** @class EvtGenDecayWithCutTool EvtGenDecayWithCutTool.h "EvtGenDecayWithCutTool.h"
 *
 *  Does everything just like EvtGenDecay except it has a cut tool and overwrite the
 *  function used to generate the signal decay. It invokes EvtGenDecay::generateSignalDecay
 *  on the particle until the decay passes the cut.
 *
 *  ASSUMPTION:
 *  This obviously only makes sense if the cuttool only checks for things that depend only
 *  on the decay itself, i.e. some invariant mass of a number of decay products.
 *
 *  If the applied cut is independent of the signal particle frame, this should not bias the produced
 *  distributions in any way.
 *
 *  WARNING:
 *  While the cut tool needs to implement the usual cut tool interface, all but the
 *  first argument of applyCut(...) are passed a nullptr.
 *
 *  @author Dominik Muller
 *  @date   2018-3-12
 */
class EvtGenDecayWithCutTool : public EvtGenDecay
{
public:
  /// Standard constructor
  EvtGenDecayWithCutTool( const std::string& type, const std::string& name, const IInterface* parent );

  /** Initialize method.
   *  Runs the EvtGenDecay initialization and gets the cut tool.
   */
  StatusCode initialize() override;

  /// Implements IDecayTool::generateSignalDecay
  // Runs EvtGenDecay::generateSignalDecay until a decay passes the provided cut.
  StatusCode generateSignalDecay( HepMC::GenParticle* theMother, bool& flip ) const override;

private:
  std::string m_cutToolName = ""; ///< Name of the cut tool to be applied at each decay attempt.

  IGenCutTool* m_cutTool = nullptr;
};
