################################################################################# Package: LbSuperChic2
################################################################################
gaudi_subdir(LbSuperChic2 v1r0)

gaudi_depends_on_subdirs(Gen/SuperChic2
                         Gen/LbHard)

find_package(Pythia8)
find_package(Boost)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${PYTHIA8_INCLUDE_DIRS})

gaudi_add_module(LbSuperChic2
                 src/component/*.cpp
                 LINK_LIBRARIES SuperChic2 LbHardLib)

