// Include files

// local
#include "ExtraParticlesInAcceptance.h"

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/PhysicalConstants.h"

// from Kernel
#include "Kernel/ParticleID.h"

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "HepMC/GenRanges.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ExtraParticlesInAcceptance
//
// 2018-04-05 : Adam Morris
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( ExtraParticlesInAcceptance )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ExtraParticlesInAcceptance::ExtraParticlesInAcceptance( const std::string & type ,
                                                        const std::string & name ,
                                                        const IInterface * parent )
  : GaudiTool ( type, name , parent ) {
    declareInterface< IFullGenEventCutTool >( this ) ;
    declareProperty( "SignalEventOnly" , m_SignalEventOnly = true ,
                     "Only consider the signal event? Default true" ) ;
    declareProperty( "ExcludeSignalDaughters" , m_ExcludeSignalDaughters = true ,
                     "Only consider particles outside of the signal decay chain? Default true" ) ;
    declareProperty( "WantedIDs" , m_tmp_WantedIDs ,
                     "List of desired PIDs, subject to acceptance cuts" ) ;
    declareProperty( "NumWanted" , m_NumWanted = 0 ,
                     "Desired number of particles with a PID in WantedIDs and pass acceptance cuts. Default 0" ) ;
    declareProperty( "AtLeast" , m_AtLeast = true ,
                     "At least (opposed to exactly) NumWanted? Default true" ) ;
    declareProperty( "RequiredAncestors" , m_tmp_RequiredAncestors ,
                     "(Optional) List of PIDs. Each Wanted particle must have one of these in its ancestors" ) ;
    declareProperty( "ExtraIDs" , m_tmp_ExtraIDs ,
                     "(Optional) List of PIDs that must appear somewhere in the event without acceptance cuts. Default 0" ) ;
    declareProperty( "NumExtra" , m_NumExtra = 0 ,
                     "Number of extra particles required" ) ;
    declareProperty( "AtLeastExtra" , m_AtLeastExtra = true ,
                     "At least or exactly NumExtra?" ) ;
    declareProperty( "ChargedThetaMin" , m_chargedThetaMin = 10 * Gaudi::Units::mrad ,
                     "Minimum value of angle around z-axis for charged daughters" ) ;
    declareProperty( "ChargedThetaMax" , m_chargedThetaMax = 400 * Gaudi::Units::mrad ,
                     "Maximum value of angle around z-axis for charged daughters" ) ;
    declareProperty( "NeutralThetaMin" , m_neutralThetaMin = 5 * Gaudi::Units::mrad ,
                     "Minimum value of angle around z-axis for neutral daughters" ) ;
    declareProperty( "NeutralThetaMax" , m_neutralThetaMax = 400 * Gaudi::Units::mrad ,
                     "Maximum value of angle around z-axis for neutral daughters" ) ;
    declareProperty( "PtMin" , m_PtMin = 0 ,
                     "Minimum value of pT to pass acceptance cuts" ) ;
    declareProperty( "PtMax" , m_PtMax = 7*Gaudi::Units::TeV ,
                     "Maximum value of pT to pass acceptance cuts" ) ;
    declareProperty( "ZPosMin" , m_ZPosMin = -500.0*Gaudi::Units::mm ,
                     "Minimum value of production-vertex z to pass acceptance cuts" ) ;
    declareProperty( "ZPosMax" , m_ZPosMax = 1.0*Gaudi::Units::km ,
                     "Maximum value of production-vertex z to pass acceptance cuts" ) ;
    declareProperty( "AllFromSameB", m_AllFromSameB = false ,
                     "Only consider particles that come from the most-recent b-hadron ancestor of the signal? Default false" ) ;
}

//=============================================================================
// Initialize function
//=============================================================================
StatusCode ExtraParticlesInAcceptance::initialize( ) {
  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiTool
  if ( msgLevel( MSG::WARNING ) ) {
    bool noWanted = m_tmp_WantedIDs.empty() ;
    bool noAncestors = m_tmp_RequiredAncestors.empty() ;
    bool noExtra = m_tmp_ExtraIDs.empty() ;
    if ( noWanted && ! noAncestors ) {
      warning() << "RequiredAncestors has items, while WantedIDs is empty. The content of RequiredAncestors will be ignored. Please check your configuration." << endmsg ;
    }
    if ( noWanted && noAncestors && noExtra ) {
      warning() << "All PID lists are empty. Every event will pass." << endmsg ;
    }
  }
  // Copy the temporary std::vectors to std::sets for quicker lookups later
  m_WantedIDs = PIDs( m_tmp_WantedIDs.begin() , m_tmp_WantedIDs.end() ) ;
  m_RequiredAncestors = PIDs( m_tmp_RequiredAncestors.begin() , m_tmp_RequiredAncestors.end() ) ;
  m_ExtraIDs = PIDs( m_tmp_ExtraIDs.begin() , m_tmp_ExtraIDs.end() ) ;
  return StatusCode::SUCCESS;
}
//=============================================================================
// Accept function
//=============================================================================
bool ExtraParticlesInAcceptance::studyFullEvent( LHCb::HepMCEvents * theEvents ,
                                                 LHCb::GenCollisions * ) const {
  if ( theEvents == nullptr ) {
    if ( msgLevel( MSG::ERROR ) ) {
      error() << "Pointer to events is null" << endmsg ;
    }
    return false ;
  }
  // Count Wanted particles that pass cuts
  if ( ! m_WantedIDs.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Getting Wanted particles" << endmsg ;
    }
    Particles WantedParticles = getAllParticles( theEvents, m_WantedIDs ) ;
    int count = countInAcceptance( WantedParticles ) ;
    if ( !checkCount( count, m_NumWanted, m_AtLeast ) ) return false ;
  }
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Event passed first step" << endmsg ;
  }
  // Count Extra particles
  if ( ! m_ExtraIDs.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Getting Extra particles" << endmsg ;
    }
    Particles ExtraParticles = getAllParticles( theEvents, m_ExtraIDs ) ;
    int count = ExtraParticles.size() ;
    if ( !checkCount( count, m_NumExtra, m_AtLeastExtra ) ) return false ;
  }
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Event passed!" << endmsg ;
    for ( const auto event: *theEvents ) {
      const Event evt = event -> pGenEvt() ;
      if( ! m_SignalEventOnly || evt -> signal_process_vertex() != nullptr ) printChildren( evt -> beam_particles().first );
    }
  }
  return true ;
}
//=============================================================================
// Get the list of particles to study
//=============================================================================
ExtraParticlesInAcceptance::Particles ExtraParticlesInAcceptance::getAllParticles( const LHCb::HepMCEvents * theEvents, const PIDs & list ) const {
  Particles allParticles ;
  unsigned nEvtsUsed( 0 ) ;
  for ( const auto event: *theEvents ) {
    const Event evt = event -> pGenEvt() ;
    // Is this the signal event?
    HepMC::GenVertex * signal_vertex = evt -> signal_process_vertex() ;
    const bool isSignal = signal_vertex != nullptr ;
    if ( msgLevel( MSG::DEBUG ) && isSignal ) {
      debug() << "Found a signal event" << endmsg ;
    }
    if ( m_SignalEventOnly && ! isSignal ) continue ;
    HepMC::GenVertex * Bparent = nullptr;
    if ( m_AllFromSameB ) {
      Bparent = mostRecentBAncestor( signal_vertex ) ;
      if ( Bparent == nullptr ) {
        if ( msgLevel( MSG::WARNING ) ) {
          warning() << "AllFromSameB is set to true, but the signal does not come from a b-hadron." << endmsg ;
        }
      }
    }
    // Append particles in this event to allParticles
    std::copy_if( evt -> particles_begin() ,
                  evt -> particles_end() ,
                  std::back_inserter( allParticles ) ,
                  [ this, list, evt, signal_vertex, Bparent ] ( Particle p ) {
                    if ( ! hasPIDInList( p , list ) ) return false ;
                    if ( m_ExcludeSignalDaughters && hasVertexInAncestors( p , signal_vertex ) ) return false ;
                    if ( Bparent != nullptr && m_AllFromSameB && ! hasVertexInAncestors( p , Bparent ) ) return false ;
                    return true ;
                  }
                ) ;
    ++nEvtsUsed ;
  }
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Copied " << allParticles.size() << " particle pointers from " << nEvtsUsed << " event(s)." << endmsg ;
  }
  return allParticles ;
}
//=============================================================================
// Check if a particle's PID is in a given list
//=============================================================================
bool ExtraParticlesInAcceptance::hasPIDInList( const Particle p, const PIDs & list ) const {
  int pid = p -> pdg_id() ;
  bool wanted = list.find( pid ) != list.end() ; // std::set is good for this
  return wanted ;
}
//=============================================================================
// Check if a particle passes acceptance cuts
//=============================================================================
bool ExtraParticlesInAcceptance::passesCuts( const Particle p ) const {
  // Standard inLHCb acceptance cuts
  double abs_sin_theta = std::abs( sin( p -> momentum().theta() ) ) ;
  LHCb::ParticleID pid( p -> pdg_id() ) ;
  if ( 0 == pid.threeCharge() ) {
    if ( abs_sin_theta > std::abs( sin( m_neutralThetaMax ) )
      || abs_sin_theta < std::abs( sin( m_neutralThetaMin ) ) )
      return false ;
  } else {
    if ( abs_sin_theta > std::abs( sin( m_chargedThetaMax ) )
      || abs_sin_theta < std::abs( sin( m_chargedThetaMin ) ) )
      return false ;
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "|sin(theta)| = " << abs_sin_theta << " fails cut" << endmsg ;
    }
  }
  // Transverse momentum cuts
  double Pt = p -> momentum().perp();
  if ( Pt > m_PtMax || Pt < m_PtMin ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Pt = " << Pt * Gaudi::Units::MeV << " MeV fails cut" << endmsg ;
    }
    return false ;
  }
  // Production-vertex z-position cuts
  double ZPos = p -> production_vertex() -> position().z() ;
  if ( ZPos > m_ZPosMax || ZPos < m_ZPosMin ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "ZPos = " << ZPos * Gaudi::Units::mm << " mm fails cut" << endmsg ;
    }
    return false ;
  }
  return true ;
}
//=============================================================================
// Check for required ancestors
//=============================================================================
bool ExtraParticlesInAcceptance::hasAncestor( const Particle p ) const {
  if ( m_RequiredAncestors.empty() ) return true ; // No specific ancestors required
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Looking for ancestors in the set { " ;
    for ( auto && item : m_RequiredAncestors ) {
      debug() << item << ", " ;
    }
    debug() << "}" << endmsg ;
  }
  const auto vtx = p -> production_vertex() ;
  if ( vtx == nullptr ) return false ;
  bool pass = std::any_of( vtx -> particles_begin( HepMC::ancestors ) ,
                           vtx -> particles_end( HepMC::ancestors ) ,
                           [ this ]( const Particle anc ) {
                             return hasPIDInList( anc, m_RequiredAncestors ) ;
                           }
                         ) ;
  return pass ;
}
//=============================================================================
// Check if a particle has a specific vertex in its ancestry
//=============================================================================
bool ExtraParticlesInAcceptance::hasVertexInAncestors( const Particle p, const HepMC::GenVertex * signal_vertex ) const {
  // Use the particle's production vertex because it may not have an end vertex
  const auto vtx = p -> production_vertex() ;
  if ( vtx == nullptr || signal_vertex == nullptr ) return false ;
  // Check the the production vertex first (may not appear in its own ancestors)
  if ( vtx -> barcode() == signal_vertex -> barcode() ) return true;
  // Check the end vertices of the ancestors
  bool pass = std::any_of( vtx -> particles_begin( HepMC::ancestors ) ,
                           vtx -> particles_end( HepMC::ancestors ) ,
                           [ this , signal_vertex ]( const Particle anc ) {
                             return anc -> end_vertex() -> barcode() == signal_vertex -> barcode() ;
                           }
                         ) ;
  return pass ;
}
//=============================================================================
// Find the most recent b-hadron ancestor of a given vertex
//=============================================================================
HepMC::GenVertex * ExtraParticlesInAcceptance::mostRecentBAncestor( HepMC::GenVertex * vtx ) const {
  // Fill a vector with b-hadron ancestors
  Particles Bancestors ;
  std::copy_if( vtx -> particles_begin( HepMC::ancestors ) ,
                vtx -> particles_end( HepMC::ancestors ) ,
                std::back_inserter( Bancestors ) ,
                [ this ]( const Particle anc ) {
                  LHCb::ParticleID pid( anc -> pdg_id() ) ;
                  return pid.hasBottom() && pid.isHadron() ; // exclude b-quarks
                }
              ) ;
  // If none found, return a null pointer
  if ( Bancestors.empty() ) return nullptr ;
  if ( Bancestors.size() > 1 ) { // If 1, just skip to returning the first element
    // Sort by most recent
    std::sort( Bancestors.begin() , Bancestors.end() ,
      [ this ] ( Particle A, Particle B ) {
        // If A has B in its ancestors, then A is more recent than B
        return hasVertexInAncestors( A , B -> end_vertex() ) ;
      }
    ) ;
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << vtx -> barcode() ;
      for ( auto && part : Bancestors ) {
        debug() << "<-" << part -> barcode() ;
      }
      debug() << endmsg ;
    }
  }
  // The first element of the vector is the most recent
  return Bancestors[0] -> end_vertex() ;
}
//=============================================================================
// Count the particles that pass the cuts and have one of the desired ancestors
//=============================================================================
int ExtraParticlesInAcceptance::countInAcceptance( const Particles & parts ) const {
  int count = std::count_if( parts.begin() , parts.end() ,
                             [ this ]( const Particle p ) {
                               return passesCuts( p ) && hasAncestor( p ) ;
                             }
                           ) ;
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << count << " particles are wanted" << endmsg ;
  }
  return count ;
}
//=============================================================================
// Check the count against the target
//=============================================================================
bool ExtraParticlesInAcceptance::checkCount( int count , int target, bool atleast ) const {
  if ( atleast && count < target ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "There weren't at least " << target << endmsg ;
    }
    return false ;
  } else if ( ! atleast && count != target ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "There weren't exactly " << target << endmsg ;
    }
    return false ;
  }
  return true ;
}
//=============================================================================
// Debug function: print decay products of a given particle
//=============================================================================
void ExtraParticlesInAcceptance::printChildren( HepMC::GenParticle* part, int level, std::set<HepMC::GenVertex*> *done, std::map<int,int> *remaining_at_level ) const {
  bool delete_it = false;
  if(done == nullptr){
    done = new std::set<HepMC::GenVertex*>();
    remaining_at_level = new std::map<int,int>();
    delete_it = true;
  }
  const std::string space = "    ";
  const std::string downS = "│   ";
  const std::string downA = "├──▶";
  const std::string arrow = "└──▶";
  if ( part->status() != LHCb::HepMCEvent::DocumentationParticle ) {
    std::string line;
    if ( level > 0 ) {
      for ( int i = 1; i < level; i++ ) {
          line += (*remaining_at_level)[ i ] == 0 ? space : downS;
        }
      line += (*remaining_at_level)[ level ] == 0 ? arrow : downA;
    }
    std::string idname;
    debug() << line << part->pdg_id() << "(#" << part->barcode() << ")" ;
    if ( hasPIDInList( part, m_WantedIDs ) ) debug() << ", in wanted" ;
    if ( hasPIDInList( part, m_ExtraIDs ) ) debug() << ", in extra" ;
    if ( hasAncestor( part ) ) debug() << ", has ancestor" ;
    if ( passesCuts( part ) ) debug() << ", passes cuts" ;
    debug() << endmsg ;
  } else {
    level -= 1 ;
  }
  if ( part->end_vertex() != nullptr && done->count(part->end_vertex()) == 0 ) {
    auto children = part->particles_out( HepMC::children ) ;
    for ( auto && p : children ) { // children does not have .size()
      (void) p ;
      ++ (*remaining_at_level)[ level + 1 ] ;
    }
    for ( auto && p : children ) {
      -- (*remaining_at_level)[ level + 1 ] ;
      printChildren( p, level + 1, done, remaining_at_level );
    }
    done -> insert( part->end_vertex() );
  }
  if ( delete_it ) {
    delete done;
    delete remaining_at_level;
  }
}

