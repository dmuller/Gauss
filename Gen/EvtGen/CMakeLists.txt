################################################################################
# Package: EvtGen
################################################################################
gaudi_subdir(EvtGen v14r7)

gaudi_depends_on_subdirs(Gen/GENSER)

find_package(CLHEP)
find_package(HepMC)

find_package(Pythia8)
find_package(Photos++)
find_package(Tauola++)

if(EXISTS "${PYTHIA8_INCLUDE_DIR}/Pythia8/Pythia.h")
  # include directory changed from Pythia8 175 to 183
  include_directories(SYSTEM ${PYTHIA8_INCLUDE_DIR}/Pythia8)
endif()

include_directories(SYSTEM ${PHOTOS++_INCLUDE_DIRS} ${PYTHIA8_INCLUDE_DIRS} ${TAUOLA++_INCLUDE_DIRS})

gaudi_add_library(EvtGen
                  src/*.cpp
                  NO_PUBLIC_HEADERS
                  INCLUDE_DIRS HepMC CLHEP Tauola++ Pythia8 Photos++
                  LINK_LIBRARIES HepMC CLHEP Tauola++ Pythia8 Photos++ pythia6forgauss)
                  
gaudi_env(SET PYTHIA8DATA ${PYTHIA8_XML})
